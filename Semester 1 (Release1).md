# **Tl 2022 Semester 1 (Release 1)**


## Team Charter
   * [Team Charter](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdgZyKmCU0pNvThvyq-XnrYBzwgjYMSw-ZDKzfP4BB9SWA?e=cvoidh)
   * [Team Ability](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP)

## Project

   * [Statement of work](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EY8oC8sEo9FEqDEgjK1rZ64Bcy3LpD1J5dRT02sABm_jeA?e=fYkXaX)  
   * [Problematisation Analysis](https://anu365-my.sharepoint.com/:f:/g/personal/u5927645_anu_edu_au/EoB8hB7qMdVHpsWVTakbRYkBahJqs4Y0hepPeo_QtJXa2A?e=NHskhp)
   * [Timeline/Milestone](https://anu365-my.sharepoint.com/:x:/r/personal/u5927645_anu_edu_au/_layouts/15/Doc.aspx?sourcedoc=%7B33FA03FA-18D8-4D35-A445-41BA93AAAB3C%7D&file=Decision%20log%20and%20risk%20register.xlsx&action=default&mobileredirect=true)
   * [Showcase video](https://anu365-my.sharepoint.com/:v:/g/personal/u5927645_anu_edu_au/EXUYP1-LatlGulosmcGQC2IBT5FDRx_McjHAIUPFC58H-w?e=jlBIpQ)
   * [Goals status summary - Week 1  - Week 9 (Audit 3)](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EZ0aijs1a0NKivQtIINl1NYBLNamvhaJJTpwaadwkg_JrQ?e=Cpt0rb)



## Agile Management 
   * [ClickUp Link](https://sharing.clickup.com/18606598/l/h/hqug6-2342/c2d4a1d4fbe3d86) 

## Audits

   * [Audit 1 slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EYc6za03S4pClk9eMTHNOvcB2QC6Gx41s_bpIIfs6hjLUg?e=iXiw39)  
   * [Audit 1 feedback analysis and reflection](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP) 
   * [Audit 2 Planing](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP) 
   * [Audit 2 Slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/ETyX4sTRoZdMl8uilxV1Y7wBPpTUb_GefYqCvMiMbGFh2Q?e=0JlSTD)
   * [Audit 3 Slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EeCDvEr-DdZNufaKJF5_LqwBDn69vrsB0FriDTcv_Oy_PQ?e=NkzkI5)
     

## Evidence Summary 
   * [Guide of Finding Valuable Evidence](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EeIXFZpskWdOsFWilyOZW94Bia9NNFdUskutUB7Ax5THeA?e=BYdhe0) 
   * [Output Evidence Summary Break 2 ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ERnP1uTmBLRKgHlgjKI-WxMBIx4CSCvCtvDIMHfOAkng1Q?e=7Gbhzg)
   * [Output Evidence Summary Week 7 ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EbvSFGsYuCJBjxMyxqNytwIB9zVvB1y_fHwHLcHm4QYLSg?e=pmg8hZ)
   * [Output Evidence Summary Week 8 ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EW8r6ViI9K5LkUadTwWBnakBBuKeN4iUptnephmrJLaIJg?e=lqQHVl)
   * [Output Evidence Summary Week 9 ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EYDHvJI9cWxOruG8XQ7PhAkBGBX5XQ7SZkzqicDzb5xfCw?e=61FTDZ)




## Agendas 

   * [Saturday 05 Feb 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Efy-lei0UfNFhK5NmsQM8bwBkWAy0zKaa9iqbMLg40pGTg?e=Zv1XXm)
   * [Sunday 06 Mar 2022 (week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcwQ4rTNaVtLvVQRu-o7XgkBp-5bznB8HEjs5bi6S5sFkA?e=iU7Gbo)
   * [Tuesday 08 Mar 2022 (week 3) - First Audit](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ef5sUtccwYBMmWeIpyfQkjsBp4mwfoieev4Da5y4qyPAng?e=KUXrFW)
   * [Saturday 12 Mar 2022 (Week 3) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EU0E1K80SCtMunm6j079YqYBJz3f7ndWI3iEIXMP_lDLSw?e=OFheI0)
   * [Saturday 19 Mar 2022 (Week 4) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdEqGHEHno1CkELc1XAYeY8B78hojxFiH1WRSvJ5WoLi2g?e=jnxgbZ)
   * [Tuesday 22 Mar 2022 (Week 5) - Tutorial meeting ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EWnUqqVjm3xMtevZeUmph7cBx5vdvJAHTsbJwZxLGrP8Fg?e=6XEm2f)
   * [Friday 25 Mar 2022 (Week 5) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EaB2SQFc2G1Jv8x0gGRfhysBEEt1CCAbjkPxC1CUH4S5Yw?e=A26Ziv)
   * [Saturday 26 Mar 2022 (Week 5) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdEfeux4mCZMlhjC_sAkX4cBtvRfkOb5APiBex396_ITUg?e=8QfvKw)
   * [Monday 28 Mar 2022 - Client Meeting ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Eb_xJfRKv_hLqZrdesOjjkMBkLc6pIGmElbRzjjZrtloSw?e=CpGNGu)
   * [Tuesday 29 Mar 2022 (Week 6) - Audit 2 ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EZSyGXJSN-RFgohqqJmDdBcBh_6j-wlvf8yHTSJUW4eH7A?e=cUDkSs)
   * [Saturday 02 April 2022 (Week 6)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ESsGzq9UoGNFkB6dLY9nzQ0BXJoU6ImI6L4n6U3HzTNI1A?e=OJBZ5x)
   * [Saturday 09 April 2022 (Break 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUKwBpUWwChMoA2vvHM57fsBWMf4iD1pAkpehJv5aSdvKw?e=Q57Gyh)
   * [Saturday 16 April 2022 (Break 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQ8HWWN-8sdOna7i24YH8wMBhqE-7bxVILi7l27JbVzDWQ?e=SGBsJR)
   * [Thursday 21 April 2022 (Week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ETMpHHCDsAxHnzbjmzVcLfkBOD66UD5jYjzrmxoBn6AkTg?e=fcLQeK)
   * [Saturday 23 April 2022 (Week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EexAmKsoTaRKudzQlRyrc0gBwdobznWXM9dMXH81kPjQqg?e=PdPMbM)
   * [Wednesday 27 April 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EZo8JBqyWxxGuGjmos8ZbI0Bx--Yr-wgmkcdpbJ9LuYkNw?e=gzN7oX)
   * [Saturday 30 April 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EYSYvK1DYthIkYi26bTF_msBm62tHyYnEo3lNWM5EdNSYQ?e=AK0yc9)
   * [Saturday 07 May 2022 (Week 9)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EU97ZtK6iidFg5M_1PhLeGEBak47u4bV-C1UMEwOgkvZPw?e=1CJFxI)
   * [Saturday 21 May 2022 (Week 11)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ET2u1MqgwddOrqtKbtGI5b0BAZYYGXidZQULqSvUmQ7Iaw?e=Ec7fSQ)

   
   

## Meeting Minutes

   * [Thursday 24 Feb 2022  (Week 1) - Team Formation Meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EeehCqNYLRpJlByeDZyaVBkBDr_mIrI-szngngV9jIxn6Q?e=dSy6hd)      
   * [Friday 25 Feb 2022  (Week 1) - Initiation Meeting ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQeir2MdKMxHg2OuUP_WkxMB37awlRtFr-ayhO6ZfiItBA?e=Na8DEk)  
   * [Friday 04 Mar 2022 (Week 2) - Client Meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcwHUcNiWy1Gn1gDok9EDSgBolOY8NRC1eL4wxGUtWX1Gw?e=RWFLEh)
   * [Saturday 05 Mar 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQeir2MdKMxHg2OuUP_WkxMB37awlRtFr-ayhO6ZfiItBA?e=dPfWEw)
   * [Sunday 06 Mar 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Edoa7Sxgc-dCkwzKzZ2qEZwBlcmB5PtroIc8cqAVqL5aDQ?e=ZOFned)
   * [Saturday 12 Mar 2022 (Week 3)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUBuiOnl92tOlQ5OZAdXccsBWOEi3fAnpeiDwiWcr7V0Kg?e=t5UINM)
   * [Tuesday 15 Mar 2022 (Week 4) - Tutorial meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcCupDXCwDJNjbXfkrHeLbYB_-C0wJPoG00z7WJESHw-Yg?e=z7I4Wt)
   * [Saturday 19 Mar 2022 (Week 4)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ea3ihpFvOzxHjaEfk4xlqLcBTxrn3nfwxbRA-S2fROXoyw?e=WbwUHw)
   * [Tuesday 22 Mar 2022 (Week 5) - Tutorial meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ESlCXTbym_xBptooyMhq9DkBDR_upshVBbCSQKNM-Kd78w?e=xJ5D3N)
   * [Friday 25 Mar 2022 (Week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Eb13ntyMWyJJrlH1gCidgwABZjFY8QGHPGfR5M8C6NH8ag?e=0St5d3)
   * [Saturday 26 Mar 2022 (Week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQyVUo_ue69Ko3B0S15p9AgBnz54NNZh7ellYK_bPAB-0Q)
   * [Monday 28 Mar 2022 (Week 6) - Client Meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcsZHbGOQNVFpYRTcazIgvMBCqqar12vniLoG-pCe8ViTg?e=Oh6s0j)
   * [Saturday 2 Apr 2022 (Break)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ESfVRj2YflpDuAvZ4lt75o4BU6dIQ3JXf0Cmkyb1CY7KYA?e=hm1LjH)
   * [Saturday 9 Apr 2022 (Break)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EY0Vn91_2LBIrS2F5ph3tXYBFihgqAjlD_KNQYSZubb0wA?e=91Eryd)
   * [Thursday 14 April 2022 (Break 2) - Meeting with Karla and Charles](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ecm_tP489ZBDpEKqJUNNuO8B-5KzRR-MmVAYLXRp8n--sQ?e=EfFDgd)
   * [Saturday 16 Apr 2022 (Break 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ea8NPamQV4VNt-Rvkc3at1kBXxB6vFzfy0qfAjBjuXhUwg?e=QshyO8)
   * [Tuesday 19 Apr 2022 (Week 7) - Tutorial meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQq1b6afPixBgVXz26ikn7AByZBAsSRy-97cFyV92H6lFQ?e=nZNK6c)
   * [Thursday 21 Apr 2022 (Week 7) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ETMpHHCDsAxHnzbjmzVcLfkBOD66UD5jYjzrmxoBn6AkTg?e=t4h0xa) 
   * [Saturday 23 Apr 2022 (Week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdbvAwgmjJVHrqQAQ9r5SqEBNP6JXRNmjvbUpDa_UuFZTg?e=NxNfub) 
   * [Wednesday 27 Apr 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EXYlU5N-6hREu0ztenCV1eoBL5BeDcTKFaU_LeRGgaynlQ?e=Po09Us)
   * [Thursday 28 Apr 2022 (Week 8) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u7079320_anu_edu_au/EdyEA2eShltNifjGCjXqVCYBGvG4femaTIXoMMYoVTlgvQ?e=MIujeJ)
   * [Thursday 28 Apr 2022 (Week 8) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EfqTYcpL6h5BnNQtzENhbq0BlOL4ZSkaxaH4Fy6ftPXaWA?e=y32JOy)  
   * [Saturday 30 Apr 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ESMT0KJxH71GmAX2E8BD-50BjxfhwO8Xeq3zFYHqyET-yw?e=oreNY9) 
   * [Tuesday 03 May 2022 (Week 9) - Tutorial meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ETVFjUL5wChHgkIvm6SyeH4BZ4hDn-pj-xOcwa8MgOGwwA?e=ahVpWt)
   * [Wednesday 04 May 2022 (Week 9) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdCKGlwnlCRCideInNcGadkBlpN9sCkf1PTeqk-2fbqHCg?e=5jXcKZ)
   * [Friday 06 May 2022 (Week 9) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EVzWcGCArLBDqxfylC8lSroB4xddLGelQURsFSywLpY60Q?e=9NuvRT)
   * [Saturday 07 May 2022 (Week 9)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQfYCmMQcitNnLGWJyWQId4BXsdIdwXmMLpb_grs1bbMjQ?e=GbEii1) 
   * [Monday 09 May 2022 (Week 10) - Client meeting](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EXLIcGe6Vd9LmFmjsvNC2k8Bf2hh4YKOgCzsVIUgVTX3yA?e=TuNBWT) 


  

## Socail Media Management
   * [Facebook](https://www.facebook.com/groups/330402447351380/?ref=share)
   * [Blog](https://ccyy.xyz)

## Others
   * [IP Agreement](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EWaHBat4v_1Dgp16slLd51ABHFjHuFgjJmAa-h6R20-gdQ?e=XDiMzC)
  
