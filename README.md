#  **Tl 2022 Semester 2 (Release 2)**

## Handover Documentation 
   Hi, 2023 civilise.ai teams. If you don't know where to start to understand 2022's work, start from this Handover Documentation session. Hope that this session helps your team have a quick start. Good luck! 
   * [Project Guide for Next Team ](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EQEK6B9OKbBMnKzNuFwYaZMBBg4rI4SOR1UofrQhmzqDvA?e=NlrzAl)
   * [Userstory Map(please read this map to check what goals we have achieved in 2022)](https://sharing.clickup.com/18606598/wb/h/hqug6-8402/321a4224cd1f272)
   * [The List of Undeveloped Features](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/ERSfQk9MkKhAr96d1EfmfF0B1XK89vqWVnz8RlEU_NIk7Q?e=n00dUJ)
   * [Goal E Handover documentation](https://docs.google.com/document/d/1xPENFxv9wqBCrOkUPU_wf_5oxVNYwtmWL5lLzEF3NKo/edit?usp=sharing)
   * [Goal F Handover documentation](https://docs.google.com/document/d/1MeCffnenhB4AnWQGzr8Kya1yLyoINlSIte45wDWIjro/edit?usp=sharing)
   * [Group Reflection](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdtbsIpaRPtPuTrvOyQbMPgBkYa4jz0wWzI4uFU2UEkagQ?e=nrmrKJ)
   * [ClickUp Exports](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EV2ZhazNtEVAl_nCtF-wZzwBNbT5ELwUGiBHQl6KwQKT8w?e=9gV4Ad)


## Project

   * [Statement of work](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EY8oC8sEo9FEqDEgjK1rZ64Bcy3LpD1J5dRT02sABm_jeA?e=fYkXaX)  
   * [IP Agreement](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/Ee8C2XIdEm5GuGWnvKvchrABxsiuPh7bNMNVGU8lY0pD5A?e=fbiuI6)
   * [Showcase video](https://anu365-my.sharepoint.com/:v:/g/personal/u5927645_anu_edu_au/ES5poUVE0ZBDtYRSUvKQysoBpsIa1zc-1AD5Q-yVfzqysg)
   * [Showcase pdf](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/ER7SVa8XGOBPgS8inT2vEhYBGoe2rt_vB7kz5YExEY2inA?e=2h5EwS)
   * [Burndown Chart](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/ERuZAtlFTdpBlBLfpDhodWUBinqtZESWCWNwR2R88cEKJg?e=Jz4lBZ)
   * [Acceptance of Work for Release Two](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EUfITW2f_HpAkqvI1uU5kYEBohn-Pjoj2TFmqXVR7_2BSg?e=4x6ZxE)
  

## Team Management 
   * [Team Charter (Updated)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EZfCwa7SU1xLgBYqHvdaFm0B3H6mrQuwQFMb4HEZRl2_Ew?e=HUeNfT)
   * [Agile Management Tool - ClickUp Link](https://clickup.com/signup)
   * [Team Ability](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP)


## Audits 

   * [Audit 1 Slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EWpSwf2nx6tGm-_vzjRolcABG4M7zAd1DUO8h1Tq00ZPBg?e=qftJKY) 
   * [Audit 2 Slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EbMcqRcWSglDk3jD0NwjvfEBQZcGVUNu1PEEnkP8q8r-8w?e=Kydu9f) 
   * [Audit 3 Slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EVFs4VHjzAhJoujTLWBB2WIBCdOyN4LklN7P8TrtyjuaGA?e=WQHeiC) 

   


## Output - Evidence Summary 
 
   * [Goal E Weekly Evidence Summary ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EZaJ2gfWHfNBkMIf-yFl4uMBtnYQdwWU0haPPdGsu5IXIA?e=gfed0N)
   * [Goal F Weekly Evidence Summary ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ES2skKviFFdBlM0Tmbeh2bgBldyrzU8i3WzAKnPlAIbwPg?e=oKYjJe)
   * [Data Flow diagram Updates - Week 6 ](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EUVndMMaEeFDkhq-vs4MtSMB-cIP1UY5auJg8D1KwsVr5A?e=l8hSaD)
   * [Data Flow diagram Updates - Week 7 ](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/ESEE8n91Vf1JtdKsLfDpCOwBwlc0uuWshlNuPZannzjdLg?e=ZkEuqB)
   * [Data Flow diagram Updates - Week 9 ](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/ET88PUPRhQFGteUilgnNaMABC8XyROVAfS3hWDQbIdP-cg?e=J5Pu3s)

  



## Teamwork: Agendas & Meeting Minutes 

   * [Group Meeting: Saturday 23 July 2022 (Oweek)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EXOy9d33svNAneHTAyK8GUEBIn2rvwEgPAVhcV7Ha1iLEA?e=l8DhCm)
   * [Group Meeting: Saturday 30 July 2022 (Week 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUnzZ-wSJSpKvJWS-9DlBqYBi2hcZbF4pmPRmnZZ3zZV-w?e=4H1pK2)
   * [Tutorial: Friday 05 August 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdxzcRG3L2tMvQ6ZSN0606cBQO-wQDxS6n7o3pYlbR3ntQ?e=tgEEPd)
   * [Group Meeting: Saturday 6 August 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EfveTW7uDmBDnkYkpt9fKu4BF3dsKa6gQPfHIpjq9qcYag?e=4TAHir)
   * [Group Meeting: Saturday 13 August 2022 (Week 3)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EY9K7PdIY7BBt_H6aPHhflQBHQWV78pJV5aTJVYueU-Scw?e=K0rLWK)
   * [Tutorial: Friday 19 August 2022 (Week 4)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdcaKWaKw0BJt8fV2Ph7RjkBgCWvHdGWCn84tVBNqMZJ_Q?e=bY9Mej)
   * [Group Meeting: Saturday 20 August 2022 (Week 4)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcdcJNwAEQxIu6jTi6OHxAYBZH8zlrMPJJksrTqwBaMc6w?e=ZFVqq3)
   * [Tutorial: Friday 26 August 2022 (Week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EVxOIagYpqVGiQcjRu2fbwgBgCizuNBfLg-WBwWoGfHYoQ?e=GmTAhp)
   * [Group Meeting: Saturday 25 August 2022 (Week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EXqzTq59a_xGj9islGHlxfgBI6AoodlIqn_ES1t1a6Xfvw?e=9beElb)
   * [Audit 2: Friday 02 Sep 2022 (Week 6)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EY70ir5g4cNBoZkz1UkY4_IBCsnQIDfNHL8kLd0cNg6fEA?e=3UUwIR)
   * [Group Meeting: Friday 02 Sep 2022 (Week 6)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ESb8IzlsvRFAhRtyuthX8R4BHTr1WAY1zuG2D24Sm3Egfg?e=4CRBYT)
   * [Group Meeting: Saturday 10 Sep 2022 (Break 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EXQ1KgkkMlBHs3QsFkkfZ6sBKoH2gku_-UQpHXzNQunnkQ?e=RsruI3)
   * [Group Meeting: Saturday 17 Sep 2022 (Break 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EWieFEM2QMtCkqREupUiUAgBRgxptsnA_-jPgqQdotvehg?e=enJMmJ)
   * [Tutorial: Friday 23 Sep 2022 (Week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUjxY961CjxIjfrh9mc2xm4BJ62NWbMg49PpWmsZS-5-yg?e=PSjsBM)
   * [Group Meeting: Saturday 24 Sep 2022 (Week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Efwda1K0VgJIs902RPocbLcBT2Nj-WvNhMrjVw2qyuOkow?e=VrvyTG)
   * [Group Meeting: Saturday 31 Sep 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ERGJIMEuG0tNmA_eJJnB3hEB7aRdQBK9ZfUEZFf0SlurIQ?e=VURr1t)
   * [Tutorial: Friday 07 Sunday 2022 (Week 9)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EbUom14BRzhCjY3fwDC0SgYBrUVfgwmc2HZiqqsYZ6SCMA?e=XfYrDQ)
   * [Group Meeting: Saturday 8 oct 2022 (Week 9)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUYpsXr8S99DsxK4dBDl5DYB2r6wSn_A5lVuKx5dLL7fJg?e=gsSWSI)
   * [Audit 3: Friday 14 Oct 2022 (Week 10)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EaPm3pz1veNCgqa2ivd2LOwBmEMtlBMAkRUscbWrQ_iNRw?e=DDD8iK)
   * [Tutorial: Friday 21 Friday 2022 (Week 11)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EShfo8LdtcpPmrLszVhxmKEB0bgJJwFbKKo4Z4hxmlkXsQ?e=80MRdg)
   * [Tutorial: Friday 28 Friday 2022 (Week 12)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUfwzE32wA1Jj6vHG9FDuKQBoPouMiuER8cwBGqASCntiQ?e=O74tQo)

  

## Stakeholder Engagement 

   * [Client Meeting (John):  25 July 2022 (week 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUa1J__gkbpKkuR4rAJyqT4BjHcY7dSR-AgIdbYB2uaJLA?e=heN0Aq)
   * [Client Meeting (Justin & Kirsty & John):  26 July 2022 (week 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EbDZ6s_YDlhPghy859yr2y0BzfDmArTX4oouIRM29n5bwQ?e=9ImG93)
   * [Client Meeting (John):  26 July 2022 (week 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u7323660_anu_edu_au/EV-oJU1Y_RhIj6eq9-5_qZABo5Oo_uDXGewxtYYLN0afRw)
   * [Client Meeting (John):  3 August 2022 (week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u7323660_anu_edu_au/ERZUVWHvQVxKoiNJfsxAV6EBie50YIltoSEWWMYbHhKV4g?e=lJpLx7)
   * [Client Meeting (Justin & Kirsty & John):  4 August 2022 (week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EX8AsiHfas1Pmjgn9BsupxoBcZ62fHc4k0TMbCocS9areg?e=bTkd4d)
   * [Presentation Slides in the above client meeting](https://anu365-my.sharepoint.com/:f:/g/personal/u5927645_anu_edu_au/Ejia6_CRLkxNocSjTH9OlPMBKRI3eq7vQEvQNnrCG2fMlQ?e=ZZfum4)
   * [Client Meeting (Firouzeh):  17 August 2022 (week 4)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQNOhU1VOIxNlDDOe8MAoR0BnCKMm-n4_eBU3thoPHptOA?e=yzqOhi)
   * [Client Meeting (Firouzeh):  23 August 2022 (week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EbbAQkIfaGFBiH6eqi8Df-kBsJm2-EQTeaYgfBqoGC2F5A?e=gI1NDh)
   * [Client Meeting - Iteration One Review (Justin & Kirsty & John):  26 August 2022 (week 5)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQdc9Oq2509InJbDctB49ygBAcqVCNFzwkAP1Yxx0NSRhA?e=dSpqYm)
   * [Client Meeting - Iteration One Retrospective Meeting (John):  1 Sep 2022 (week 6)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EeWusEeir6RCu2-RHQC_f2QB4hgx2Ikir3oRiNAOjYKSLA?e=ECWqyw)
   * [Client Meeting (John): 13 Sep 2022  (Break 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EWh7GcYPOKRCo-Ym3EkN7w8BNXsLXD1WFLVMRmP0jkRwxg?e=DvlaeY)
   * [Client Meeting - Iteration Two Review (John): 23 Sep 2022 (week 7)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ed4mUkv1BH5GpWIrjBoc0BEBt6y3X2wELA1P5aOW9fBsvg?e=D5Nd4I)
   * [Client Meeting (Kirsty): 26 Sep 2022 (Week 8)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EZjjhQ0RGfpJpsZpYXDYwTMBSlKMedcc5r1wfISKpwQ4tg?e=5E3qJx)
   * [Client Meeting (Firouzeh): 4 Oct 2022 (Week 9)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EfSYt_BXRz5FghOt1oXeKocB5lfu5xwYrASj-M_tbSjCNA?e=Caz62O)
   * [Client Meeting - Iteration Three Review (Kirsty & John): 7 Oct 2022 (Week 10)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EVdbpJs_qeNLsK5CwLg-KWEBplcahlU8susDbwFyD76iAA?e=2553Ag)
   * [Client Meeting - Goal E Iteration Three Review (John): 9 Oct 2022 (Week 10)](https://anu365-my.sharepoint.com/:w:/g/personal/u7079320_anu_edu_au/EdQJPSDlw1hEraQzJzJ8xa8BDkSaYwo4GLub6OvaxgurVA?e=M6q8xn)
   * [Client Meeting(Fiouzeh): 21 Oct 2022 (Week 11)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EYiYEb8R9mpFlC5-bZJfHBkBq97-lG68SKQqOWGnO5cDaA?e=uXriQu)
  

   

   

