import os
import matplotlib.pyplot as plt
import numpy as np
from Backend.util.file_manager import find_data
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges
from Demos.util.visualisation import show_contour_overlays

if __name__ == "__main__":
    # dem = np.loadtxt(os.path.join(find_data(), "black_mountain.txt"))
    dem = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))
    # dem = np.loadtxt(os.path.join(find_data(), "araluen.txt"))

    gullies, ridges = gullies_and_ridges(dem, 99, 98, healing=False)

    title = "Araluen Unhealed"
    show_contour_overlays(dem, [gullies, ridges], ["blue", "red"], title)