## Paddock Plan / Management units
Before you ask, the 'paddock plan' and 'management units' are the same thing. The words 'paddock plan' are easier to understand, but the words 'management unit' better reflect what it actually is.

### What is a management unit?
A management unit is a section of land that is used for a specific purpose. Management units should be 1-10 hectares in size and span from a ridge to gully. Like a [terroir](https://en.wikipedia.org/wiki/Terroir).

### Why management units are important in our project?
Management units are useful for helping farming decide what to do with each bit of land. Each management unit can be given a specific purpose such as:
- agriculture 
    - general paddocks & lambing paddocks
- crop farming - units can help with deciding how to do crop rotation
- tree growth
- unused

### How are we going to implement a method?
* Split the land into sections based on the gullies, ridges, and contours chosen for trenches
* Calculate the size of each section
* Split sections that are too large into smaller sections using the smallest split possible (to minimise fencing requirements)

### History
Initially we implemented the paddock plan by finding large flat areas. But through customer meetings we discovered that what they wanted out of the paddock plan was more complex, as we have outlined above. For the sake of documentation, here is an example result from our initial algorithm.
![Paddocks](../../Archive/Sem1/backend/images/naive_paddocks.png)
