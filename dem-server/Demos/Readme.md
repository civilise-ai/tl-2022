# Demos
This directory is used for testing and demonstrating the Backend.

## Installation Instructions:
### python packages.
  * We recommend creating an anaconda environment using python 3.7 or 3.8. 
  * Dependencies: scipy numpy sklearn tensorflow matplotlib pysheds
  * install via 'conda' or 'pip'
  ##### mayavi:
  * mayavi is a nice visualisation library, but not required for most scripts.
  * mayavi requires python 3.7, vtk 8.1.2, and is not compatible with opencv
  * [instructions to install](https://docs.enthought.com/mayavi/mayavi/installation.html)


## Sub-directories
We split the backend into these subdirectories to help manage the code. See their respective Readme's for more info.
- [ridge_and_gully](Demos/ridge_and_gully)
- [trench_plan](Demos/trench_plan)
- [paddock_plan](Demos/paddock_plan)
- [queries](Demos/queries)

- [erosion_model](Demos/erosion_model)
- [Data](Demos/Data)





  
   