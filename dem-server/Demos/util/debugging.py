import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

class Progress_estimator():
    """Print estimates for how long the loop will take"""
    def __init__(self, total_iterations, loop_name=""):
        self.current_iteration = 0
        self.total_iterations = total_iterations
        self.start_time = time.time()
        self.expected_completion = self.start_time
        self.loop_name = loop_name
        print(f"{self.loop_name} starting loop: {time.ctime(time.time())}")

    def __call__(self):
        if self.current_iteration > 0:
            iteration_time = (time.time() - self.start_time)/self.current_iteration
            self.expected_completion = self.start_time + (self.total_iterations * iteration_time)
            duration = relativedelta(datetime.fromtimestamp(self.expected_completion),
                                     datetime.fromtimestamp(self.start_time)) - \
                       relativedelta(datetime.fromtimestamp(time.time()),
                                      datetime.fromtimestamp(self.start_time))
            print(f"{self.loop_name} {self.current_iteration}/{self.total_iterations}; "
                  f"Expected time left: {self.readable(duration)}; "
                  f"Expected completion time: {time.ctime(self.expected_completion)}")
        self.current_iteration += 1
        if self.current_iteration == self.total_iterations:
            print(f"Finished at {time.time()}")

    def readable(self, duration):
        """Makes a relativedelta duration human readable"""
        params = {"year":duration.years,
                  "month":duration.months,
                  "day":duration.days,
                  "hour":duration.hours,
                  "minute":duration.minutes,
                  "second":duration.seconds}
        return ", ".join(f"{params[p]} {p}{'s' if params[p] != 1 else ''}" for p in params if params[p])
