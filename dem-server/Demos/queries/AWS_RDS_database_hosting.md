AWS RDS Database setup process

1. Create an AWS account (need to pay $1)
https://aws.amazon.com/free

2. Create an AWS RDS db instance
https://aws.amazon.com/rds/postgresql/

3. Adjust the security settings so you can access the database
https://aws.amazon.com/rds/postgresql/

4. Connect to the db instance 
    ``` 
    psql \
       --host=civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com \
       --port=5432 \
       --username=postgres \
       --password
    ```
    password=`br8Ihgtwa8Rehouse`

5. Create a database and a user
    ```
    CREATE user me with encrypted password 'me';
    create database nsw;
    exit
    ```

6. Connect to that database 
    ```
      psql \
       --host=civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com \
       --port=5432 \
       --username=me \
       -d nsw
       --password
    ```
    password=`me`

7. Enable postgis
    ```
    create extension postgis
    exit
    ```

8. Load the backup into the database
    ```
    psql -h civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com -p 5432 -d nsw -U me < three.sql
    ```
    (Took about 30 mins)