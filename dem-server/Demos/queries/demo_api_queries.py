from Backend.queries.api_querying_interface import *
from Backend.backendManager import parse_coord
import matplotlib.pyplot as plt

if __name__ == '__main__':

    araluen = "-35.584439, 149.817381"
    mulligans_flat = "-35.132832, 149.165401; -35.143402, 149.179046"
    spring_valley = "-35.280716, 149.021031; -35.296579, 149.003169"
    black_mountain = "-35.270872, 149.099910"
    middlingbank = "-36.313496, 148.883731"
    cootlaranta = "-36.317520, 148.841032"
    kangaroo_valley = "-34.742719, 150.518267;-34.732672, 150.537371"
    bombala = "-36.5397651, 149.3071905"
    swamp_creek = "-35.2629, 148.9205"
    caloola = "-35.6716, 149.0727"

    coord = parse_coord(araluen)
    coords = parse_coord(mulligans_flat)

    cell_size = 10

    # DEM = generate_dem_one_point_gp(coord[0], coord[1])
    dem, bottom_left = generate_dem_one_point_gp(coord[0], coord[1], cell_size=10)
    plt.imshow(dem)
    plt.show()

    dem, bottom_left = generate_dem_two_points_gp(coords[0], coords[1], cell_size=10)
    plt.imshow(dem)
    plt.show()

    cadastre = generate_cadastre_one_point(coord[0], coord[1], None)
    plt.imshow(cadastre)
    plt.show()

    cadastre = generate_cadastre_two_points(coords[0], coords[1], None, cell_size=10)
    plt.imshow(cadastre)
    plt.show()

    img = RGB_aerial_one_point_gp(coord[0], coord[1], [1024,1024])
    plt.imshow(img)
    plt.show()

    img = RGB_aerial_two_points_gp(coords[0], coords[1], [1024,1024])
    plt.imshow(img)
    plt.show()

