# Database Statistics

Actual
- The raw 2m contours for NSW are 90.72GB broken up into 343 regions
- A sample region (berridale) is 0.444GB raw, and expands to 1.43GB when interpolated to 10mx10m regions
    - This interpolation took ~30 mins on my machine
    
Theoretical
- Therefore, it should take about about 7 days and 292GB to interpolate all of NSW
- But realistically, we probably only need a small section, maybe 12 regions from Canberra down to Jacob's river (e.g. Canberra, Brindabella, Tumut, Michelago, Tantangara, Yarrangobilly, Cooma, Berridale, Kosciuszko, Bombala, Numbla, Jacobs Creek)
    - Need to verify this with stakeholders
    - This should only take ~6 hours and 17GB
    - You can see how the sections are broken up in the [Australia Topos Index](Australia_Topos_index.pdf)
