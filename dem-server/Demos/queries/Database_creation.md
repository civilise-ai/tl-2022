# Database Creation

These instructions are for preprocessing the DEM from the contours. For just loading a premade database, see 'Database_setup'.

## How nsw_dems.sql was created
1.1. brew install postgresql and postgis.
1.2 Start postgresql locally
    - `pg_ctl -D /usr/local/var/postgres start`
1.3. create a superuser & database
    - `sudo -u postgres psql`
    - `CREATE user me with encrypted password 'me';`
    - `ALTER USER me WITH SUPERUSER;`
    - `CREATE database act;`
    - `exit`
1.4. Enable PostGIS
    - `psql -U me -d act`
    - `CREATE EXTENSION postgis;`
    - `exit`

2. shp2pgsql -I -s 7855 Z55/Berridale-CONT-AHD_55_2m.shp new | psql nsw me
    - I have no idea how we obtained these 2m shape files. Ask John.
3. create table contours(wkb_geometry geometry, elevation bigint);
4. INSERT INTO contours(wkb_geometry, elevation) select ST_transform(geom,28355) as wkb_geometry, elevation from new;
5. create table uninterpolated(point geometry,z int);
6. INSERT INTO uninterpolated(point, z)
SELECT ST_SetSRID(ST_POINT(ST_X((dp).geom), ST_Y((dp).geom)), 28355) as point, z FROM 
(SELECT ST_DumpPoints(wkb_geometry) AS dp, elevation AS z 
   FROM contours) as points;
7. CREATE INDEX uninterpolated_idx
  ON uninterpolated
  USING GIST(point);
8. create table interpolated(point geometry primary key,z float);
9. CREATE INDEX interpolated_idx
  ON interpolated
  USING GIST(point);


shp2pgsql -I -s 7855 Z55/Canberra-CONT-AHD_55_2m.shp new | psql nsw me

----------------------
# ACT database

NOTE: This was the old database using 1m contours which has since been superseded by the NSW 2m contours (which also contains the ACT)

1. Download the ACT contours (3GB) from [here](https://actmapi-actgov.opendata.arcgis.com/datasets/2015-1m-contours)
    - Click the 'Download as geodatabase in MGA94 zone55'
2. Download the ACT blocks from [here](https://actmapi-actgov.opendata.arcgis.com/datasets/act-blocks)
    - Click 'Download' -> 'Shapefile'
3. Download QGIS from [here](https://www.qgis.org/en/site/forusers/download.html)
4. Use QGIS to convert the ACT contours to a .sql_backup (15GB)
    - Navigate to the file using the browser & double click it (static_contours2015.gbs/contours1m)
    - Right click on the layer that shows up, select Export -> Save Feature As...
    - In Format, choose Postgres SQL dump
    - Choose a file name & location
5. Repeat step 4 for ACT blocks (ACT_Blocks.shp)
6. brew install postgresql and postgis
7. create a superuser & database
    - `sudo -u postgres psql`
    - `CREATE user me with encrypted password 'me';`
    - `ALTER USER me WITH SUPERUSER;`
    - `CREATE database act;`
    - `exit`
8. Enable PostGIS
    - `psql -U me -d act`
    - `CREATE EXTENSION postgis;`
    - `exit`
9. Load the datasets
    - psql -U me -d act < (filepath/act_blocks.sql)
    - psql -U me -d act < (filepath/act_contours.sql)
10. Run the preprocessing.py script
11. Export just the dems table with 
    - `pg_dump  --username me --file "filepath/dems.sql" --table dems act`
