import os
import matplotlib.pyplot as plt
import psycopg2
import numpy as np

from Backend.backendManager import parse_coord
from Backend.paddock_plan.paddocks import paddocks
from Backend.queries.api_querying_interface import generate_dem_two_points_gp, generate_dem_one_point_gp
from Backend.queries.database_querying_interface import query_interpolated_one_point, query_interpolated_two_points
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges
from Backend.trench_plan.equal_area_trenches import equal_area_trench_plan
from Backend.util.file_manager import find_data
from Backend.util.exports import raster_export, vector_export

from Demos.util.visualisation import show_contour_overlays

if __name__ == '__main__':

    # Need to change this to the Vultr database address, see note in backendManager
    connection = psycopg2.connect(user="me",
                                  password="me",
                                  host="civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com",
                                  port=5432,
                                  database="nsw")

    # There are four different ways of requesting the DEM. 1 coord, 2 coords, inside preprocessed region, outside preprocessed region
    # Had to change the output type of each request to include the bottom left coord. Will need to update backendManager accordingly too
    # I'm outputting the bottom left coordinate in EPSG:28355

    spring_valley_two_points = "-35.280716, 149.021031; -35.296579, 149.003169"
    coords = parse_coord(spring_valley_two_points)
    DEM, bottom_left = query_interpolated_two_points(coords[0], coords[1], connection)
    DEM, bottom_left = generate_dem_two_points_gp(coords[0], coords[1], cell_size=10)

    spring_valley_one_point = "-35.28878, 149.01080"
    coords = parse_coord(spring_valley_one_point)
    DEM, bottom_left = query_interpolated_one_point(coords[0], coords[1], connection)
    DEM, bottom_left = generate_dem_one_point_gp(coords[0], coords[1], cell_size=10)
    print(f"DEM shape: {DEM.shape}")

    gullies, ridges = gullies_and_ridges(DEM, gully_threshold=98, healing=False)
    trenches = equal_area_trench_plan(DEM, 5)
    show_contour_overlays(DEM, [gullies, ridges, trenches], ['blue', 'red', 'green'])
    print("created ridges, gullies, and trenches")

    testData = {"ridges": ridges,
                "gullies": gullies,
                "trenches": trenches}
    testPaddocksData = paddocks(testData)
    paddock_overlay = np.array(testPaddocksData['overlay'])
    plt.imshow(paddock_overlay % 9,cmap='Set1')
    plt.show()
    print("created the paddocks")

    raster_export(bottom_left, ridges, os.path.join(find_data(), "Exports", "ridges.tif"))
    raster_export(bottom_left, trenches, os.path.join(find_data(), "Exports", "trenches.tif"))
    raster_export(bottom_left, gullies, os.path.join(find_data(), "Exports", "gullies.tif"))
    raster_export(bottom_left, paddock_overlay, os.path.join(find_data(), "Exports", "paddocks.tif"))

    vector_export(bottom_left, ridges, os.path.join(find_data(), "Exports", "ridges.geojson"))
    vector_export(bottom_left, trenches, os.path.join(find_data(), "Exports", "trenches.geojson"))
    vector_export(bottom_left, gullies, os.path.join(find_data(), "Exports", "gullies.geojson"))
    vector_export(bottom_left, paddock_overlay, os.path.join(find_data(), "Exports", "paddocks.geojson"), polygons=True)