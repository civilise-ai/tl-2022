from Backend.queries.database_querying_interface import *
from Backend.backendManager import parse_coord
import matplotlib.pyplot as plt
import psycopg2
from os import environ

establish_database_connection = lambda: psycopg2.connect(
  user="me",
  password="me",
  host=environ.get('DB_HOST'),
  port="5432",
  database="nsw"
)

if __name__ == '__main__':
  connection = establish_database_connection()

  araluen = "-35.584439, 149.817381"
  mulligans_flat = "-35.132832, 149.165401; -35.143402, 149.179046"
  spring_valley = "-35.280716, 149.021031; -35.296579, 149.003169"
  spring_valley_single = "-35.28878, 149.01080"
  black_mountain = "-35.270872, 149.099910"
  middlingbank = "-36.313496, 148.883731"
  cootlaranta = "-36.317520, 148.841032"
  bombala = "-36.5397651, 149.3071905"
  swamp_creek = "-35.2629, 148.9205"
  caloola = "-35.6716, 149.0727"

  overlapping = ""

  coord = parse_coord(spring_valley_single)
  coords = parse_coord(spring_valley)
  #
  # DEM = query_interpolated_one_point(coord[0], coord[1], connection)
  # plt.imshow(DEM)
  # plt.show()

  DEM, bottom_left = query_interpolated_two_points(coords[0], coords[1], connection)
  plt.imshow(DEM)
  plt.show()

  # np.savetxt("../../Demos/Data/mulligans_flat.txt", DEM)
  # fig = go.Figure(data=[go.Surface(z=dem, colorscale='Viridis', showscale=True)])
  # fig.show()
