#!/bin/bash
# Need to use the postgres account to run this on bodhi
# And need to activate the enjin environment before starting this

# This script assumes we already have a few tables:
# contours: create table contours(wkb_geometry geometry, elevation bigint);
# uninterpolated: create table uninterpolated(point geometry,z int);
# interpolated: create table interpolated(point geometry primary key,z float);
# spatial index: CREATE INDEX interpolated_idx ON interpolated USING GIST(point);

# files=(Lindsay Mildura Taltingan Mulurulu CobhamLake Monolon Nowingi Hatfield Nyah Menindee Murtee FowlersGap Popiltah Robinvale Scotia Paika BerawinniaDowns Urisino FortGrey Pooncarie Redan Bunnerungee MiddleCamp Topar Wilcannia Tibooburra Milparinka Bancannia Nuchea Yancannia Weimby SwanHill Bunda Thackaringa Kayrunnera Yantara Arumpo Urella MenaMurtee Grasmere BrokenHill Kerang LakeTandou Teilta Tongowoko Turlee Para Manara Smithville Darnick Wonnaminta MountArrowsmith Callindra OliveDowns Corona CliftonBore Nartooka WildDog Balranald Teryawynia Cuthero Boolaboolka Wentworth Bidura LakeVictoria HawkerGate Buckalow Tongo ThurlooDowns Yantabangee Manfred WhiteCliffs)
# files=(Bono)
files=(ThurlooDowns Yantabangee Manfred WhiteCliffs)


# Local filepaths
# path_python=/Users/christopherbradley/repos/tl-2021/Demos/queries/preprocessing.py
# data_path=/Users/christopherbradley/Documents/Uni/Civilise/Data/NSW_State_Contours/Z55/

# Bodhi filepaths
path_python=/home/chris/tl-2021/Demos/queries/preprocessing.py
data_path=/media/hilltop/gis_data/state_contours/Z54

for region in ${files[@]}
do
  echo working on $region $(date)
  full_path=$data_path$region-CONT-AHD_54_2m.shp

  psql nsw -c "truncate table contours;"
  psql nsw -c "truncate table uninterpolated;"
  psql nsw -c "drop table if exists new;"

  shp2pgsql -I -s 7854 $data_path/$region-CONT-AHD_54_2m.shp new | psql nsw
  psql nsw -c "INSERT INTO contours(wkb_geometry, elevation) select ST_transform(geom,28355) as wkb_geometry, elevation from new;"
  psql nsw -c "INSERT INTO uninterpolated(point, z) SELECT ST_SetSRID(ST_POINT(ST_X((dp).geom), ST_Y((dp).geom)), 28355) as point, z FROM (SELECT ST_DumpPoints(wkb_geometry) AS dp, elevation AS z FROM contours) as points;"
  echo interpolating $region $(date)
  python $path_python

  echo completed $region $(date)
done
