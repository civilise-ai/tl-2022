import sys, os

from Demos.util.visualisation import show_contour_overlays

sys.path.insert(1, os.getcwd())
from Backend.trench_plan.evenly_spaced_trenches import trench_plan, trench_costs
# from Demos.ridge_and_gully.ridge_detection.ridge_detection import plot_result
from Backend.trench_plan.evenly_spaced_trenches import trench_plan
import matplotlib.pyplot as plt
import numpy as np
# from mayavi import mlab     # Mayavi requires python 3.7, vtk 8.1.2, and is incompatible with opencv-python
from Backend.util.file_manager import find_data


if __name__ == "__main__":
    Z = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    t = trench_plan(Z, num_trenches=10)
    show_contour_overlays(Z, [t], ["green"], "Equal area trenches")

    # mlab is a really cool visualisation tool that we should have used more! Rotten dependencies :(
    # mlab.surf(Z, colormap = 'gist_earth')
    # mlab.view(20)
    # mlab.show()