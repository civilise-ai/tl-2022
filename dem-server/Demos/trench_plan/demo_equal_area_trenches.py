import sys, os

from Demos.util.visualisation import show_contour_overlays

sys.path.insert(1, os.getcwd())
from Backend.trench_plan.equal_area_trenches import equal_area_trench_plan, trench_costs
# from Demos.ridge_and_gully.ridge_detection.ridge_detection import plot_result
from Backend.trench_plan.equal_area_trenches import equal_area_trench_plan
import numpy as np
import matplotlib.pyplot as plt
from Backend.util.file_manager import find_data


if __name__ == "__main__":
    Z = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    t = equal_area_trench_plan(Z, num_trenches=10)
    show_contour_overlays(Z, [t], ["green"], "Equal area trenches")

    length, costs = trench_costs(t)
    print(length)
    print(costs)


