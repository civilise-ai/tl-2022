## Audit 1 outputs

- [Extra google maps layers](727d82eb): Both satellite imagery and terrain view. Labels can also be toggled on and off.
![](Assets/satellite_imagery.png)

- [Terrain exaggeration slider](47ec93d1): So you can adjust how steep the terrain looks.
![](Assets/terrain_exaggeration.png)

## Audit 2 outputs

- [Docker](c018bc5b)
    - Can now install dependencies and run backend and frontend with just "docker compose up"
    - Good for handover, and allows for backend development on windows
![](Assets/docker_example.png)


- [Paddock Highlighting](855905ac)
    - Hovering over a paddock now makes it highlighted
    - This can be used later to connect the map with the paddock table
![](Assets/paddock_highlighting.png)


- [Ridge and gully improvements](6c687be2)
    - Fixed major bug from last semester and improved on ridge and gully "healing"
    - Explained approach and previous attempts (including hand-drawn examples) in the [Ridge and Gully Healing Document](https://docs.google.com/document/d/1rO3GITV9pvFCUKKH8yV5xrYt0o__MWo5r-y5Gej_690/edit?usp=sharing)
![](Assets/ridge_and_gully_healing.png)


- [Geolocated exports](dca3a193)
    - Ridges, gullies, and trenches as rasters
    - Paddocks as vectors
![](Assets/geolocated_exports.png)
