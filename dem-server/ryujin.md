# Notes from time with James

> 🤓 This is all to install docker, using apt install will likely put you on the wrong version and things will break.

From https://docs.docker.com/engine/install/ubuntu/
```bash
sudo apt-get update
```
```bash
sudo apt-get install ca-certificates curl gnupg lsb-release
```
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
```bash
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

```bash
sudo apt-get update
```
```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
```bash
sudo docker run hello-world
```

> 🤓 This installs docker-compose which doesn't come pre-installed with docker.  We install from github releases install of apt.

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

* https://docs.docker.com/engine/install/linux-postinstall/

```bash
sudo groupadd docker # Create the docker group
```

```bash
sudo usermod -aG docker $USER # Add your user to the docker group.
```

```bash
reboot #Log out and log back in so that your group membership is re-evaluated.
```

```bash
docker run hello-world # Verify that you can run docker commands without sudo.
```

```bash
sudo chown root:docker /usr/local/bin/docker-compose # Set group for docker-compose to docker
```

```bash
sudo chmod 770 /usr/local/bin/docker-compose # Set docker-compose permissions
```

```bash
docker-compose up -d # -d tells it to persist
```

```bash
export DB_HOST=138.130.218.141 # Set environment variable: DB_HOST
```


## Resetting Image State

```bash
docker-compose down
```

```bash
docker container prune
```

```bash
docker image rm tl-2021_frontend
```

```bash
docker image rm tl-2021_backend
```

```bash
docker-compose up
```

## Generating Schema.sql file from existing nsw database
```bash
psql postgresql://me:me_pass@127.0.0.1/nsw
```

```bash
DB_URL=postgresql://me:me_pass@127.0.0.1/nsw
```

```bash
pg_dump $DB_URL --schema-only
```

