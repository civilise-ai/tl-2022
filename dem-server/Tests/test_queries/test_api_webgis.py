import numpy as np
import unittest
import os

from Backend.util.file_manager import find_data
from Backend.queries.api_querying import single_interpolation


class Test_Webgis(unittest.TestCase):

    sv_extent = [682185.5923, 6091990.3866, 683655.1468, 6094688.2768]
    cell_size = 10

    def test_single_interpolation(self):
        """Check Spring valley gets interpolated to 269x147 10mx10m cells"""
        # ~1 sec
        uninterpolated = os.path.join(find_data(), "spring_valley_uninterpolated")
        points = np.loadtxt(uninterpolated)
        single_interpolated = single_interpolation(points, self.sv_extent, self.cell_size)
        assert single_interpolated.shape == (269, 147)

    def test_extract_points(self):
        pass

    def test_extract_perimeters(self):
        pass

if __name__ == '__main__':
    unittest.main()