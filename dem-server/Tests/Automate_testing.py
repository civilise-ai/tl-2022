
import subprocess
import os
from Backend.util.file_manager import find_root

original_Directory=os.path.join(find_root(), 'Tests')

def change_directory(directory):
    """Navigate to one of the testing directories"""
    os.chdir(os.path.join(original_Directory, directory))

def call_subprocess(filename):
    """Run all the tests in a given file"""
    subprocess.call(f'python -m unittest {filename}', shell=True)

change_directory('test_queries')

call_subprocess("test_griddata.py")
call_subprocess("test_api_interface.py")
call_subprocess("test_api_webgis.py")
call_subprocess("test_database_querying.py")
call_subprocess("test_Querying_database.py")

change_directory('test_ridges')

call_subprocess("test_endpoints.py")
call_subprocess("test_ridges.py")

change_directory('test_trenches')

call_subprocess('test_equal_trenches.py')
call_subprocess('test_evenly_spaced.py')
call_subprocess('test_Extract_Trench.py')

change_directory('test_util')
call_subprocess('test_backend_manager.py')

"""
 nodemon --exec python3 start.py --auto-commit

"""