
from Backend.trench_plan.evenly_spaced_trenches import *
import os
from Backend.util.file_manager import find_data
import numpy as np
from Backend.trench_plan.Extract_Contours import extract_contours
import matplotlib.pyplot as plt
import unittest


class Test_Trenches(unittest.TestCase):
    dem = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    def test_evenly_spaced(self):
        """Check the shape matches up"""
        example = np.loadtxt(os.path.join(find_data(), "trenches"))
        calculated = trench_plan(self.dem, num_trenches=10)
        assert example.shape == calculated.shape

    def test_dictToGrid(self):
        """
        maxZ , minZ , spacing values are taken from spring valley
        """
        maxZ = 676
        minZ = 489
        spacing = 18
        dict_Contours = extract_contours(self.dem)
        contours = range(minZ + spacing // 2, maxZ, spacing)
        grid = dict_to_grid(self.dem, dict_Contours, contours)
        assert grid.shape == (269, 147)

    def test_extractContours(self):
        extractContours = extract_contours(self.dem)
        assert len(extractContours) == 187

    def test_trenchcosts(self):
        cost1 = trench_costs(10, 40, 1)
        cost2 = trench_costs(10, 40, 2)
        self.assertEqual(cost1, (1, 40))
        self.assertEqual(cost2, (2, 80))


if __name__ == '__main__':
    unittest.main()
