# Frontend

[Visit our production site here :)](https://enjinwebsite.herokuapp.com)

## Flask
The frontend is a website and runs on the Flask framework. This allows for communication between the backend and frontend. Things like webpages being accessed, buttons being clicked etc can trigger python functions on the backend, which can boot the entire backend system. Data is returned simply by the return statement in the backend funciton.

## Running
To run the website locally, make sure you have python, flask and all dependencies installed
```bash
pip3 install flask
```

Start the local server with
```bash
python3 app.py
```
and open the website at the local hosted address.

## Deploying to Production
The production site is hosted on Heroku, and new code can be pushed and automatically deployed with git. This requires a different file structure than the one in gitlab, and so must cloned to a seperate directory.

### Setting up the production repo on local
To be able to interact with Heroku you will need to install the Heroku CLI.
```bash
brew tap heroku/brew && brew install heroku
```
If you don't already have an account with Heroku [sign up here](https://signup.heroku.com/login), then login to Heroku using
```bash
heroku login
```
Then let me know to add your account to the Civilise.ai project
Finally, you can clone the project to a new directory useing
```bash
heroku git:clone -a enjinwebsite
```

### Configuring the project for production and deploying
It's easiest to copy and replace files you have changed your local gitlab repo into the prouction repo. When doing this, make sure you change any import statements to the ones that work in production:
```python
sys.path.append(path)
```
Also file paths may have changed since all the backend code is in one folder called backend (feel free to organise this folder if it gets too much and if you're brave).
If this new code introduces any new dependencies, be sure to list those in requirements.txt to let Heroku know to install them
Finally, push the new code
```bash
git commit -am \"blaaahhhh\"
git push
```
Heroku will build and deploy the new site.

### Debugging
After deploying you should make sure this new code is working. Head to enjinwebsite.herokuapp.com and click through everything you added. If you come accross an internal server error, you can examine a real-time error log with
```bash
heroku logs --tail
```
