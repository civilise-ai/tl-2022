# Frontend Development Log

## Rewriting
The frontend was completely rewirtten in semester 2 to take advantage of the Flask framework's server and backend/frontend linking capabilities. This will increase development time somewhat, however in the long run development in Flask should be faster as it is the simplest framework available for this application.

## Django
**Django** is a framework that allows us to link the frontend with python scripts on the backend, and can be used locally in development as well as server side in production. A test website was built using this framework, however it proved to be too cumbersome for this application because:
- It is designed mostly for database-driven websites, and ours does not link directly to a database.
- It provides many functionalities natively, such as users, admin functions etc that are not needed for this project and would end up bulking it
- It is complicated to use, resulting in long development cycles
- It dictates the file structure of our project, which isn't fair on the backend development team

## Flask

**Flask** is a similar framework. It is much more lightweight and simple to use, as it does not natively provide a user system, admin functions, database linking and other bloat. This is perfect for us as our application uses none of this. The advantages of Flask are:
- It is lightweight and simple to use, making development easier
- It does not dictate a file structue, allowing the backend team to completely disregard the frontend
