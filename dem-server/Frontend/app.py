from flask import Flask, render_template, request, jsonify

from flask_cors import CORS, cross_origin
import numpy as np

# Use these import statements for gitlab project structure
import sys
import os
import time
# sys.path.insert(1, os.getcwd()+'/Backend')
sys.path.insert(1, os.getcwd())  # noqa
from Backend.backendManager import test, backend_paddocks, backend_ridges, backend_trenches_step, backend_trenches_even, backend_dem, backend_aerial, backend_gullies, backend_cadastre, getbottomleft # noqa
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges
from Backend.trench_plan.evenly_spaced_trenches import trench_plan as trenches_even
from Backend.util.exports import layers_to_linestrings, vector_export


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route("/")
def home():
    return render_template("index.html")


@app.route("/index")
def index():
    return render_template("index.html")


@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/demos", methods=["POST", "GET"])
def demosapp():
    if request.method == "POST":
        returnedStatement = (test(request.form['address']))
        print("Demo name entered: " + request.form['address'])
    else:
        returnedStatement = ""
    return render_template("demos.html", returnedStatement=returnedStatement)


# @app.route("/api")
# def api_home():
#     return jsonify({"message": "hello from our API!"})


@app.route("/api", methods=["POST"])
def api_dem():
    # 1. Extract parameters from the url. This includes coordinates, toggles and slider settings
    print ("Started api_dem with URL: " + str(request.url))
    parameters = request.url[request.url.find("?")+1:].split("&")

    # The "=" is just in case the parameters are empty (which should never happen)
    if ("=" in parameters[0]):
        paramDict = {p.split("=")[0]:p.split("=")[1] for p in parameters}
    else:
        paramDict = dict()

    # 2. Extract the data from the form. This includes the DEM, layers data etc
    # The paramDict contains the request, the jsonData contains anything that's been previously calculated.
    jsonData = request.get_json()
    print ("paramDict: ", paramDict)
    print("prevParams:", jsonData["prevParams"])
    print("layers:", jsonData["layers"].keys())

    # 3. Send these to backendManager.py. Allows for requesting a single layer, or multiple layers at once.
    dataDict = dict()

    if "coords" in paramDict:
      # Required params: coords
      coord_dict = backend_dem(paramDict['coords'])
      dataDict = {**dataDict, **coord_dict}

      # Record any dem data that might be useful for other layers
      jsonData['prevParams']['coords'] = paramDict["coords"]
      jsonData['layers']['dem'] = coord_dict["DEM"]
      jsonData['prevParams']['shape'] = coord_dict["shape"]

    if "aerial" in paramDict:
      # Required params: coords, shape
      aerial_dict = backend_aerial(jsonData['prevParams']['coords'],
                                   jsonData['prevParams']['shape'])
      dataDict = {**dataDict, **aerial_dict}

    if "cadastre" in paramDict:
      # Required params: coords, shape
      cadastre_dict = backend_cadastre(jsonData['prevParams']['coords'],
                                   jsonData['prevParams']['shape'])
      dataDict = {**dataDict, **cadastre_dict}

    if "trenchesEven" in paramDict:
      # Required params: dem
      trenches_even_dict = backend_trenches_even(jsonData['layers']['dem'],
                                                 paramDict['trenchesEven'])
      dataDict = {**dataDict, **trenches_even_dict}

    if "trenchesStep" in paramDict:
      # Required params: dem
      trenches_step_dict = backend_trenches_step(jsonData['layers']['dem'],
                                                 paramDict['trenchesStep'])
      dataDict = {**dataDict, **trenches_step_dict}

    if "gullies" in paramDict:
      # Required params: dem
      # Optional params: ridges (the threshold)
      if "ridges" not in jsonData['prevParams']:
        jsonData['prevParams']['ridges'] = 99
      gullies_dict = backend_gullies(jsonData['layers']['dem'],
                                    paramDict['gullies'],
                                    jsonData['prevParams']['ridges'])
      dataDict = {**dataDict, **gullies_dict}

    if "ridges" in paramDict:
      # Required params: dem
      # Optional params: gullies (the threshold)
      if "gullies" not in jsonData['prevParams']:
        jsonData['prevParams']['gullies'] = 99
      ridges_dict = backend_ridges(jsonData['layers']['dem'],
                                    jsonData['prevParams']['gullies'],
                                    paramDict['ridges'])
      dataDict = {**dataDict, **ridges_dict}

    if "paddocks" in paramDict:
      # Required params: dem
      # Optional params: ridges, gullies, trenches_even
      if "ridges" not in jsonData['prevParams']:
        jsonData['prevParams']['ridges'] = 99
      if "gullies" not in jsonData['prevParams']:
        jsonData['prevParams']['gullies'] = 99
      if "gullies" not in jsonData['prevParams']:
        jsonData['prevParams']['gullies'] = 99
      if "trenchesEven" not in jsonData['prevParams']:
        jsonData['prevParams']['trenchesEven'] = 4

      # Note: we are not using the previously generated ridges and gullies anymore.
      # This is because they are a different version (healed vs unhealed).
      # So the paddocks will take a while to load.
      dem = jsonData['layers']['dem']
      gullies, ridges = gullies_and_ridges(dem,
                                           float(jsonData['prevParams']['gullies']),
                                           float(jsonData['prevParams']['ridges']),
                                           healing=True)
      trenches = trenches_even(dem, int(jsonData['prevParams']['trenchesEven']))

      paddocks_dict = backend_paddocks(gullies, ridges, trenches)
      dataDict = {**dataDict, **paddocks_dict}

    if not dataDict:
      raise Exception("Failed to generate any layers")
    print("Returning dataDict:", dataDict.keys())
    return jsonify(dataDict)


# @app.route("/api/gully", methods=["POST"])
# def api_gully():
#     print ("Started Gully interface: " + str(request.url))
#     jsonData = request.get_json()
#     print(jsonData)
#
#     data = backend_gullies  # do backend stuff using form data and store here
#     return jsonify({
#         "data": data
#     })


@app.route("/api/export", methods=["POST", "GET"])
def export():
    print ("EXPORT: export request")

    #return jsonify({"exportFile" : "static/loading.gif"})
    #return send_file('static/loading.gif', as_attachment=False)
    #return jsonify({"test":"http://127.0.0.1:5000/static/loading.gif"})

    parameters = request.url[request.url.find("?")+1:].split("&")
    if ("=" in parameters[0]):
      paramDict = {p.split("=")[0]:p.split("=")[1] for p in parameters}
    else:
      paramDict = dict()

    if "coords" in paramDict:

      jsonData = request.get_json()

      bottom_left = getbottomleft(paramDict['coords'])
      print ("EXPORT: Bottom left coord: " + str(bottom_left))
      

      # Add all our layer exports to this dict
      exportDict = {}

      if ('trenches_even' in jsonData['layers']):
        exportDict.update({'trenches_even' : str(vector_export(bottom_left, np.array(jsonData['layers']['trenches_even'])))})
      
      if ('trenches_step' in jsonData['layers']):
        exportDict.update({'trenches_step' : str(vector_export(bottom_left, np.array(jsonData['layers']['trenches_step'])))})

      if ('gullies_pysheds' in jsonData['layers']):
        exportDict.update({'gullies_pysheds' : str(vector_export(bottom_left, np.array(jsonData['layers']['gullies_pysheds'])))})

      if ('ridges_pysheds' in jsonData['layers']):
        exportDict.update({'ridges_pysheds' : str(vector_export(bottom_left, np.array(jsonData['layers']['ridges_pysheds'])))})

      if ('paddocks' in jsonData['layers']):
        #print (jsonData['layers']['paddocks']['overlay'])
        B = [[0 for x in range(len(jsonData['layers']['paddocks']['overlay'][0]))] for y in range(len(jsonData['layers']['paddocks']['overlay']))]
        for x in range(0, len(jsonData['layers']['paddocks']['overlay'])):
          for y in range(0, len(jsonData['layers']['paddocks']['overlay'][x])):
            if (jsonData['layers']['paddocks']['overlay'][x][y] != -1):
              B[x][y] = 1
            else:
              B[x][y] = 0

        exportDict.update({'paddocks' : str(vector_export(bottom_left, np.array(B)))})


      # cadastre is choking up vector converter for some reason :/
      '''if ('cadastre' in jsonData['layers']):
        # Cadastre comes as a boolean array rather than 1s and 0s so convert this
        C = [[0 for x in range(len(jsonData['layers']['cadastre'][0]))] for y in range(len(jsonData['layers']['cadastre']))]
        for x in range(0, len(jsonData['layers']['cadastre'])):
          for y in range(0, len(jsonData['layers']['cadastre'][x])):
            if (jsonData['layers']['cadastre'][x][y]):
              C[x][y] = 1
            else:
              C[x][y] = 0
        print (C)
        exportDict.update({'cadastre' : str(vector_export(bottom_left, np.array(C)))})'''

      print ("EXPORT: Returning exports dictionary to frontend")
      return jsonify(exportDict)
    
    else:
      return jsonify("EXPORT: Error: Could not get coordinates")

    return jsonify("EXPORT: Error: Unknown")

    

@app.route("/<other>")
def hello_name(other):
    return "<h1>Hmm, the page '" + other + "' doesn't seem to exist :(</h1>"


if __name__ == "__main__":
    app.run(debug=False)
