# Backend/backendManager.py

# This is a replacement for BackendStarter.py,
# for use with the new frontend and backend model
# This is the starting point for all of our Backend functionality
# and serves as the link between the frontend and Backend.


# Takes 2 dictionaries:
# 1. POST dict: toggles, sliders, inputs, coordinates etc from the frontend
# 2. Form dict: already processed data we have previously sent to the backend.
#
# This app will then determine which things need to be added to the data dict

from Backend.trench_plan.equal_area_trenches import equal_area_trench_plan as trenches_step, equal_area_trench_plan
from Backend.trench_plan.evenly_spaced_trenches import trench_plan as trenches_even
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges

from Backend.paddock_plan.paddocks import paddocks

from Backend.queries.database_querying_interface import query_interpolated_one_point, query_interpolated_two_points
from Backend.queries.api_querying_interface import generate_dem_one_point_gp, RGB_aerial_one_point_gp, \
    generate_dem_two_points_gp, generate_cadastre_one_point, generate_cadastre_two_points, RGB_aerial_two_points_gp

import numpy as np
import sys
import os
import psycopg2

from os import environ

sys.path.insert(1, os.getcwd())
sys.path.append('.')

establish_database_connection = lambda: psycopg2.connect(
  user="me",
  password="me",
  host=environ.get('DB_HOST'),
  port="5432",
  database="nsw"
)

def backend_paddocks(ridges, gullies, trenches):
  """Handle datatype conversions between backend and frontend"""
  args = {'ridges': ridges, 'gullies':gullies, 'trenches':trenches}
  results = dict()
  results['paddocks'] = paddocks(args)
  return results

def backend_gullies(dem, gully_threshold, ridge_threshold):
  """Handle datatype conversions between backend and frontend"""
  gullies, ridges = gullies_and_ridges(
    dem,
    float(gully_threshold),
    float(ridge_threshold),
    healing=False
  )
  results = {'gullies': dict()}
  results['gullies']['array'] = gullies.tolist()
  return results

def backend_ridges(dem, gully_threshold, ridge_threshold):
  """Handle datatype conversions between backend and frontend"""
  gullies, ridges = gullies_and_ridges(dem, float(gully_threshold), float(ridge_threshold), healing=False)
  results = {'ridges': dict()}
  results['ridges']['array'] = ridges.tolist()
  return results

def backend_trenches_even(dem, num_trenches):
  """Handle datatype conversions between backend and frontend"""
  results = {'trenchesEven': dict()}
  results['trenchesEven']['array'] = trenches_even(dem, int(num_trenches)).tolist()
  return results

def backend_trenches_step(dem, num_trenches):
  """Handle datatype conversions between backend and frontend"""
  results = {'trenchesStep': dict()}
  results['trenchesStep']['array'] = trenches_step(dem, int(num_trenches)).tolist()
  return results

def backend_aerial(coord, shape):
  """ Request Aerial Imagery
  Example inputs:
    coord - one point : ['-34.737893', '150.530024']
    coord - two points : [[-34.737893, 150.530024],[-34.737893, 150.530024]]
    shape - [269, 147]
  Return:
    result - a dict containing relevant key value pairs
      'aerial' - a 2d array of rgb lists e.g.[255,255,255]
  """
  results = dict()
  coord = parse_coord(coord.replace("%3B", ";"))
  if type(coord[0]) == list:
    print(f"Getting aerial imagery from two points")
    results['aerial'] = RGB_aerial_two_points_gp(coord[0], coord[1], shape).tolist()
  else:
    print(f"Getting aerial imagery from one point")
    results['aerial'] = RGB_aerial_one_point_gp(coord[0], coord[1], shape).tolist()
  return results


def backend_cadastre(coord, shape):
  """
  Example inputs:
    coord - one point : ['-34.737893', '150.530024']
    coord - two points : [[-34.737893, 150.530024],[-34.737893, 150.530024]]
    shape - [269, 147]
  Return:
    result - a dict containing relevant key value pairs
      'cadastre' - a 2d array of rgb lists e.g.[255,255,255]
  """
  results = dict()
  coord = parse_coord(coord.replace("%3B", ";"))
  if type(coord[0]) == list:
    print(f"Getting cadastre from two points")
    results['cadastre'] = generate_cadastre_two_points(coord[0], coord[1], shape).tolist()
  else:
    print(f"Getting cadastre from one point")
    results['cadastre'] = generate_cadastre_one_point(coord[0], coord[1], shape).tolist()
  return results


def backend_dem(coord):
  """ Request a dem from a coordinate string
  Example inputs:
    Single coord : '-34.737893, 150.530024'
    Corner coords: '-34.737893, 150.530024; -34.737893, 150.530024'
  Return:
    result - a dict containing relevant key value pairs
      'statement' - a status message
      'coords' - parsed coords into a list (see 'parse_coord')
      'cell_size' - currently 10mx10m
      'DEM' - this is the important thing. A 2D array of height values
      'shape' - The dimensions (number of cells in x and y directions)
  """

  print("Started backend_main")
  print(f"Initial coord(s): {coord}")

  results = dict()
  results['statement'] = ""

  # 1. Verify that coords are syntactically correct
  # e.g. "-34.737893, 150.530024" or "-34.737893, 150.530024; -34.737893, 150.530024"
  coords = parse_coord(coord.replace("%3B", ";"))
  results["coords"] = coords
  if not coords:
    print("ERROR: Bad coords")
    results['statement'] = "Please make sure the input is syntactically correct (Eg: -35.28878, 149.01080)"
    results['DEM'] = None
    return results

  try:
    # 2: Establish database connection
    connection = establish_database_connection()

    results['statement'] = "DEM for " + coord

    # Step 3: Get DEM
    cell_size = 10
    results['cell_size'] = cell_size

    # Database queries
    if type(results["coords"][0]) == list:
      results['DEM'], bottom_left = query_interpolated_two_points(coords[0], coords[1], connection)
    else:
      results['DEM'], bottom_left = query_interpolated_one_point(coords[0], coords[1], connection)

    # API queries (Only run if the database query failed)
    if len(results['DEM']) == 0:
      if type(results["coords"][0]) == list:
        results['DEM'], bottom_left = generate_dem_two_points_gp(coords[0], coords[1], cell_size)
      else:
        results['DEM'], bottom_left = generate_dem_one_point_gp(coords[0], coords[1], cell_size)

    results['DEM'] = results['DEM'].tolist()
    results["shape"] = [len(results['DEM']), len(results['DEM'][0])]
    print(f"Got DEM of size: {len(results['DEM'])}x{len(results['DEM'][0])}")

  except Exception as e:
    print(f"Could not generate dem because {e}")
    results['statement'] = "Could not generate Digital Elevation Model for " + coord + ". Please try a different coordinate"
    results['DEM'] = None

  print("Generated DEM")
  return results


def parse_coord(coord):
  """
  Example inputs:
    Single coord : '-34.737893, 150.530024'
    Corner coords: '-34.737893, 150.530024; -34.737893, 150.530024'
  Example returns:
    Single coord : ['-34.737893', '150.530024']
    Corner coords: [[-34.737893, 150.530024],[-34.737893, 150.530024]]
  """
  print(f"Parsing coord: {coord}")
  if ';' in coord:
    print("Parsing corner coords")
    try:
      if ';' in coord:
        print("found ; in coord")
        coords = coord.split(';')
      print(f"Coord after split: {coords}")
      coords = [c.split(',') for c in coords]
      coords = [[float(c) for c in cs] for cs in coords]
    except:
      coords = None
  else:
    print("Parsing single coord")
    try:
      coords = coord.split(',')
      float(coords[0]),float(coords[1])
    except:
      coords = None

  print(f"Parsed coords: {coords}")
  return coords

def getbottomleft(coord):
  coords = parse_coord(coord.replace("%3B", ";"))
  connection = establish_database_connection()

  if type(["coords"][0]) == list:
    temp, bottom_left = query_interpolated_two_points(coords[0], coords[1], connection)
  else:
    temp, bottom_left = query_interpolated_one_point(coords[0], coords[1], connection)
  
  return bottom_left

test = lambda name: "Hello from the Backend, " + name + "! :D"
