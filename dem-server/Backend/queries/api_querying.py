import sys, os
sys.path.insert(1, os.getcwd())
from io import BytesIO
from shapely.geometry import Point
from shapely.affinity import affine_transform
import geopandas as gp
import numpy as np
from rasterio import features
from requests import Request, get
from pyproj import Transformer
from PIL import Image
from time import ctime
from skimage.transform import resize
from Backend.queries.interpolate import generate_grid


def generate_cadastre_lat_lon(lat, long, isACT):
    """
    Generates coordinate box in both NSW (3857) and ACT (28355) Spatial Reference
    - cad stands for cadastre which means property boundary

    :param lat: latitude
    :param long: longitude
    :param isACT: boolean indicating if its within ACT
    :return: bound - coordinate in spatial reference of its State (if isACt = true, ACT spatial reference),
            other - coordinate in spatial reference of the other state
            gs - polygon of the boundary
    """
    box_bound = generate_box_points_gp(lat, long, 0, isACT)
    return generate_cadastre(box_bound, isACT)

def generate_cadastre(box_bound, isACT):
    """Finds the cadastre outline from a bounding box"""
    if isACT:
        Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
        outputEPSG = 28355
    else:
        Cadastre_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry="
        outputEPSG = 3857
    gs = generate_shape_gp(box_bound, Cadastre_url)
    extent = gs.total_bounds
    bound = gp.GeoSeries([Point(extent[0], extent[1]), Point(extent[2], extent[3])])
    if isACT:
        inEPSG = 28355
        outEPSG = 3857
    else:
        inEPSG = 3857
        outEPSG = 28355
    x, y = transform_proj(extent[0], extent[1], inEPSG, outEPSG)
    p1 = Point(x,y)
    x, y = transform_proj(extent[2], extent[3], inEPSG, outEPSG)
    p2 = Point(x,y)
    other = gp.GeoSeries([p1, p2])
    # gs_line = gp.GeoSeries(LineString(gs.geometry[0].exterior))
    # gd_line = gp.GeoDataFrame(geometry=gs_line)
    return bound, other, gs

def check_if_ACT(lat, long):
    act_border = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/0/query?geometry="
    x, y = transform_proj(lat, long, 4326, 28355)
    bound = gp.GeoSeries([Point(x, y), Point(x,y)])
    ds = generate_shape_gp(bound, act_border)
    return len(ds) > 0

def generate_image_gp(bound, feature_url, q_x, q_y):
    """
    Generate an Image

    :param bound:
    :param feature_url:
    :return:
    """
    url = generate_image_gp_URL(bound, feature_url, q_x, q_y)
    ret = get(url)
    print(ret)
    img = Image.open(BytesIO(ret.content))
    return img

def generate_image_gp_URL(bound, feature_url, q_x, q_y):

    """
    Generate the full image query URL

    :param bound: the box coordinates
    :param feature_url: the feature_url of the map/image server
    :return: full url for query image
    """

    url = feature_url + str(bound[0].x)+ "," + str(bound[0].y) + "," + str(bound[1].x) + "," + str(bound[1].y)
    url = url + "&size=" + str(q_x) + "%2C" + str(q_y)
    url = url + "&format=png&transparent=true&f=image"
    return url

def generate_shape_gp(bound, feature_url):
    """
    Generate a ogr dataset from the query return

    :param bound: box coordinate
    :param feature_url: the feature_url of the map serevr
    :return: response dataset of the query
    """
    url = generate_shape_URL(bound, feature_url)
    # print(ctime(), "Sent Request:", url)
    data = Request('GET', url, headers={'Cache-Control': 'no-cache'}).prepare().url
    gd = gp.read_file(data)
    # print(ctime(), "Received points from API:", url)
    return gd

def generate_shape_URL(bound, feature_URL):
    """
    Generate the full shape query URL

    :param bound: box coordinate
    :param feature_URL: the feature_url of the map server
    :return: full url for query shape
    """
    url = feature_URL
    url = url + str(bound[0].x)+ "," + str(bound[0].y) + "," + str(bound[1].x) + "," + str(bound[1].y)
    url = url + "&geometryType=esriGeometryEnvelope&f=pjson"
    return url


def generate_rect_bounds_gp(c1, c2, isACT):
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    x,y = transform_proj(c1[0], c1[1], inputEPSG, outputEPSG)
    p1 = [x,y]
    x,y = transform_proj(c2[0], c2[1], inputEPSG, outputEPSG)
    p2 = [x,y]
    bound = gp.GeoSeries([Point(min(p1[0], p2[0]), min(p1[1],p2[1])), Point(max(p1[0], p2[0]), max(p1[1],p2[1]))])
    return bound

def transform_coords(c1, c2, inputEPSG, outputEPSG):
    x1,y1 = transform_proj(c1[0], c1[1], inputEPSG, outputEPSG)
    x2,y2 = transform_proj(c2[0], c2[1], inputEPSG, outputEPSG)
    return (x1, y1, x2, y2)

def transform_proj(x, y, inputEPSG, outputEPSG):
    in_str = 'epsg:' + str(inputEPSG)
    out_str = 'epsg:' + str(outputEPSG)
    tf = Transformer.from_crs(in_str, out_str)
    return tf.transform(x, y)

def generate_box_points_gp(lat, long, range, isACT):
    """Create a box where the two coordinates are the same (basically a point)"""
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    x, y = transform_proj(lat, long, inputEPSG, outputEPSG)
    bound = gp.GeoSeries([Point(x-range, y-range), Point(x+range,y+range)])
    return bound

def generate_DEM_gp(prim_bound, contour_url, cell_size):
    print(ctime(), "About to query for points")
    contour = generate_shape_gp(prim_bound, contour_url)
    extent = prim_bound.total_bounds
    points = np.empty((0, 3))
    print(ctime(), "Received all points along intersecting contours")
    for geom in contour.values:
        for pt in geom[1].coords:
            [x,y] = pt
            if ((x < extent[2]) & (x > extent[0]) & (y < extent[3]) & (y > extent[1])):
                points = np.append(points, np.array([[pt[0], pt[1], geom[0]]], dtype=float), axis=0)
    print(ctime(), "Extracted just the points within the boundary")

    # Need to make sure the EPSG is 28355 before interpolating for the dimensions to work out correctly
    minx, miny, maxx, maxy = extent
    extent_epsg28355 = transform_coords((minx, miny), (maxx, maxy), 3857, 28355)

    dem = single_interpolation(points, extent_epsg28355, cell_size)
    print(ctime(), "Interpolated points")

    bottom_left = (extent_epsg28355[0], extent_epsg28355[1])
    return dem, bottom_left

def single_interpolation(points, extent, cell_size):
    """interpolate all the points in one go"""
    dimensions = [(extent[2] - extent[0]), (extent[3] - extent[1])]
    dimensions = [int(round(e)) for e in dimensions]
    width = int(dimensions[0] / cell_size)
    height = int(dimensions[1] / cell_size)
    dem = generate_grid(points, width, height)
    return dem

def gen_plt_from_url(plt, data_bound, map_bound, url, color, pt_shp, size):
    ds = generate_shape_gp(data_bound, url)
    points_data = extract_points(ds, size[0], size[1], map_bound)
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color, rasterized=True)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15, rasterized=True)
    return plt, points_data

def gen_plt_from_points(points_data, plt, color, pt_shp):
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15)
    return plt

def extract_points(gs, bound, shape, cell_size=10):
    """Convert geopandas property coordinates to a numpy array
    params:
        gs - a geopandas geoseries containing the coordinates representing the boundaries of each property
        bound - the bounding box of the area selected
    return:
        property_outlines - a 2d boolean array containing all property boundaries
        properties - a 2d int array where -1 means no property, and 0 to n represent each property filled in
    """
    bound_width = bound.x[1] - bound.x[0]
    bound_height = bound.y[1] - bound.y[0]
    n_pixels_x = int(bound_width/cell_size)
    n_pixels_y = int(bound_height/cell_size)
    properties = np.full([n_pixels_y, n_pixels_x], -1)

    for i in range(len(gs.geometry)):

        # Transform the coordinates to match the indices of the property_boundaries np array
        geom = gs.geometry[i]
        ratio_x = (1/cell_size)
        ratio_y = (1/cell_size)
        translate_x = bound.x[0] * -ratio_x
        translate_y = n_pixels_y + bound.y[0] * ratio_y
        aff_transform = [ratio_x, 0, 0, -ratio_y, translate_x, translate_y]
        transformed = affine_transform(geom, aff_transform)

        # Convert the property geometry to a raster (a 2d array)
        raster = features.geometry_mask([transformed], out_shape=properties.shape, transform=[1.0, 0.0, 0.0, 0.0, 1.0, 0.0], invert=True)
        properties[np.where(raster)] = i

        # plt.imshow(properties)
        # plt.show()

    property_outlines = extract_perimeters(properties)
    # Instead of resizing, I should probably specify this out shape during the affine transformation
    if shape:
        property_outlines = resize(property_outlines, shape).astype(bool)
    return property_outlines, properties

def extract_perimeters(properties):
    """Extract just the perimeters from each property """
    property_outlines = np.zeros(properties.shape, dtype=bool)
    adjacencies = np.array([[-1, 0], [0, -1], [1, 0], [1, 1]])
    for i in range(properties.shape[0]):
        for j in range(properties.shape[1]):
            coord = [i,j]
            property = properties[coord[0], coord[1]]
            if property == -1:
                continue
            neighbours = adjacencies + coord
            non_negatives = np.array([n for n in neighbours if n[0]>=0 and n[1]>=0 and
                                      n[0] < properties.shape[0] and n[1] < properties.shape[1]])
            neighbour_properties = properties[non_negatives[:,0],non_negatives[:,1]]
            neighbours_all_same = np.all(neighbour_properties == property)
            if not neighbours_all_same:
                property_outlines[i,j] = True
    return property_outlines

# Potentially useful API links
NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox="
NSW_Cadastre_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry="
NSW_Road_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/29/query?geometry="
NSW_Contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
NSW_Bld_rural_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/22/query?geometry="
NSW_HydroArea_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/46/query?geometry="

ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
ACT_Contour1m_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/2/query?outFields=contour&geometry="
ACT_Contour5m_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
ACT_Contour10m_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/0/query?outFields=contour&geometry="
ACT_Contour10m_url2 = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/55/query?geometry="
ACT_Road_sealed = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/4/query?geometry="
ACT_Road_unsealed = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
ACT_soil_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/soil/MapServer/1/query?geometry="
ACT_water_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/84/query?geometry="

