import numpy as np
import psycopg2
from collections import Counter

def connect_to_database(user, password, database, host="localhost", port="5432"):
    connection = psycopg2.connect(user=user,
                                  password=password,
                                  database=database,
                                  host=host,
                                  port=port)
    return connection


def get_points(cursor, corners, table="uninterpolated", corners_epsg=28355, table_epsg=28355, sorted=False):
    """Fetch all the points in a given area"""
    # Note: Transform the points, not the polygon.
    polygon = f"""select ST_SetSRID(ST_Extent(ST_MakeLine(ARRAY[p1, p2, p3, p4, p1])), {table_epsg}) as polygon
                from
                (select ST_Transform(ST_SetSRID(ST_Point({corners["min_x"]}, {corners["min_y"]}), {corners_epsg}), {table_epsg}) as p1) as foo,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["max_x"]}, {corners["min_y"]}), {corners_epsg}), {table_epsg}) as p2) as bar,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["max_x"]}, {corners["max_y"]}), {corners_epsg}), {table_epsg}) as p3) as roar,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["min_x"]}, {corners["max_y"]}), {corners_epsg}), {table_epsg}) as p4) as nein
    """
    query = f"select st_x(point) as x, st_y(point) as y, z from {table}, ({polygon}) as bla where st_contains(polygon, point)"
    if sorted:
        query = f"{query} ORDER BY x, y ASC"
    cursor.execute(query)
    fetched = cursor.fetchall()
    # if not fetched:
    #     print(f"No points found for corners {corners}")
    points = np.asarray(fetched)
    return points


def tablify_points(points, cell_size=10):
    """Convert points from [(x,y,z), ... ] to a 2d array of z values. Assumes points are pre-interpolated."""
    shape = (len(set(points[:,0])), len(set(points[:,1])))

    # Simple case, no overlapping regions
    if shape[0] * shape[1] == len(points):
        print("------ Points fit nicely into a 2d array ---------")
        dem = points[:, 2].reshape(shape)
        return dem

    # Sometimes there are overlapping regions, so the grid doesn't match up.
    # Here we separate out these regions and use the region with the largest number of points.
    print("!!!!!!!! Overlapping points, Picking just one region !!!!!!!!")
    grid_decimals = Counter(p[0] % 10 for p in points)
    chosen_decimal = grid_decimals.most_common()[0][0]
    grid = np.array([p for p in points if p[0] % cell_size == chosen_decimal])
    shape = (len(set(grid[:, 0])), len(set(grid[:, 1])))
    if shape[0] * shape[1] == len(grid):
        dem = grid[:, 2].reshape(shape)
        return dem

    # Note: It might be worth trying to do something clever to combine the regions instead of just picking one region
    raise Exception("Could not generate specified area, probably because it lies on the boundary of different regions")

