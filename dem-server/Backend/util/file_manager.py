import os

def find_root():
    """return the absolute filepath to the project root (tl_2021)"""
    project_name = 'tl-2021'
    full_path = os.path.abspath(__file__)
    root_path = full_path[:full_path.find(project_name) + len(project_name)]
    return os.path.join(root_path)

def find_data():
    """return the absolute filepath to the Test/Data folder"""
    root_path = find_root()
    test_data_path = os.path.join(root_path, "Data")
    return os.path.join(test_data_path)
