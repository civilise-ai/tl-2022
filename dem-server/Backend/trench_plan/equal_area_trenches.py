import numpy as np
from Backend.trench_plan.Extract_Contours import extract_contours
from Backend.trench_plan.evenly_spaced_trenches import dict_to_grid
from skimage.measure import find_contours

def equal_area_trench_plan(Z, num_trenches):
    """Creates even area divisions of trenches, as contours"""
    total = len(Z)*len(Z[0])
    spacing = total/(num_trenches+1)
    Z = np.array(np.round(Z), dtype=int)
    all_heights = []
    for col in Z:
        for cell in col:
            all_heights.append(cell)
    all_heights.sort()
    contours = []
    for i in range(1, num_trenches+1, 1):
        contours.append(all_heights[int(i*spacing)])
    dict_Contours = extract_contours(Z)
    grid = dict_to_grid(Z, dict_Contours, contours)
    return grid

# trench cost reference: https://www.airtasker.com/landscaping/trench-digging/price/how-much-does-trench-cost/
def trench_costs(trenches, cost_per_meter=30, contour_res=10):
    trench_length = np.count_nonzero(trenches)
    if contour_res == 1:
        cost = trench_length * cost_per_meter
    elif contour_res == 2:
        cost = trench_length * cost_per_meter * 2
    else:
        cost = trench_length * cost_per_meter * 10
    return trench_length*contour_res, cost