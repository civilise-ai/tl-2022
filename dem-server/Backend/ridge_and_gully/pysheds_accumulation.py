import numpy as np
from affine import Affine
from pyproj import Proj
from pysheds.grid import Grid

from Backend.ridge_and_gully.endpoints import find_endpoints, extend_gullies
from Backend.ridge_and_gully.group_expansion import group_expansion

def gullies_and_ridges(dem, gully_threshold, ridge_threshold=None, healing=False):
    """Generate the gullies and ridges sequentially to ensure no overlapping"""
    dem = np.array(dem)

    if not ridge_threshold:
        ridge_threshold = gully_threshold

    # We can fill depressions for gullies because we can assume all water leaves the property (and heads to the ocean). But can't make this assumption for ridges.
    gullies, gully_acc = acc_cutoff(dem, gully_threshold, fill_depressions=True)
    ridges, ridge_acc = acc_cutoff(-dem, ridge_threshold, fill_depressions=False)

    # Ensure there is no overlap between the ridges and gullies
    ridges[np.where(gullies)] = False

    # Extend the gullies uphill and the ridges downhill using the accumulation
    gully_endpoints = find_endpoints(gullies, gully_acc)
    ridge_endpoints = find_endpoints(ridges, ridge_acc)

    gullies = extend_gullies(gullies, gully_acc, gully_endpoints, ridges)
    ridges = extend_gullies(ridges, ridge_acc, ridge_endpoints, gullies)

    if healing:
        # Extend the gullies uphill and ridges downhill using the elevation
        gully_endpoints = find_endpoints(gullies, gully_acc)
        ridge_endpoints = find_endpoints(ridges, ridge_acc)

        gullies = extend_gullies(gullies, dem, gully_endpoints, ridges, early_stopping=False)
        ridges = extend_gullies(ridges, -dem, ridge_endpoints, gullies, early_stopping=False)

        # Connect the ridges together using the group expansion algorithm
        ridges = group_expansion(gullies, ridges)

    return gullies, ridges

# Just used for debugging
def extract_cells(coords, shape):
    """Converts a list of cell dictionaries to a 2d array"""
    extracted = np.zeros(shape, dtype=int)
    xs = [coord[0] for coord in coords]
    ys = [coord[1] for coord in coords]
    extracted[xs, ys] = 1
    return extracted

def acc_cutoff(dem, threshold=98, fill_depressions=True):
    """Use the water accumulation with a cutoff threshold to determine the gullies"""
    acc = water_accumulation(dem, fill_depressions=fill_depressions)
    acc_threshold = np.percentile(acc, threshold)
    gullies = np.vectorize(lambda a: a > acc_threshold)(acc)
    return gullies, acc

def water_accumulation(Z, fill_depressions=True):
    """Use pysheds to find the gully network without specifying an indivual pour point"""
    # The pysheds documentation is here: https://mattbartos.com/pysheds
    grid = dem_to_grid(Z)

    if fill_depressions:
        # Conditioning terrain for water flow. Can only do this for gullies, not ridges.
        grid.fill_depressions(data='dem', out_name='flooded_dem')
        grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
        grid.flowdir(data='inflated_dem', out_name='dir')
    else:
        grid.flowdir(data='dem', out_name='dir')
    grid.accumulation(data='dir', out_name='acc')
    acc = grid.view('acc')
    return np.array(acc)

def dem_to_grid(Z):
    """Convert the DEM (2d array of height values) to a Grid (Data structure used by Pysheds)"""
    grid = Grid()
    # crs is the coordinate reference system: https://epsg.io/28355
    # Affine identity means no extra transformations: https://www.e-education.psu.edu/natureofgeoinfo/c2_p18.html
    grid.add_gridded_data(data=Z, data_name='dem', affine=Affine.identity(), shape=Z.shape,
                          crs=Proj('epsg:28355'))
    return grid