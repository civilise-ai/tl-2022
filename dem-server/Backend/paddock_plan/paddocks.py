import copy
import math
import functools
from re import A
import pickle
'''
create a border for the grid, notice -1 represent the line for ridge/gully/trench 
in the grid, it works similar over here.

 -1 -1 -1 -1 
 -1  0  0 -1   /--  0 0
 -1  0  0 -1   \--  0 0
 -1 -1 -1 -1 
'''
def pad_with_ridges(grid):
    new_grid = []
    dummy_row = []
    # Top and bottom rows
    for i in range(len(grid[0])+2):
        dummy_row.append(-1)
    new_grid.append(dummy_row)
    for row in grid:
        new_row = []
        new_row.append(-1)
        for cell in row:
            new_row.append(cell)
        new_row.append(-1)
        new_grid.append(new_row)
    new_grid.append(dummy_row)
    return new_grid
'''
 -1 -1 -1 -1 
 -1  0  0 -1  -- \  0 0
 -1  0  0 -1  -- /  0 0
 -1 -1 -1 -1 
'''
def remove_padding(grid):
    # grid[1:-1, 1:-1, 1:-1]
    new_grid = []
    for i in range(1, len(grid)-1):
        row = []
        for j in range(1, len(grid[i])-1):
            row.append(grid[i][j])
        new_grid.append(row)
    return new_grid

'''
Collect adjacent paddocks.
0  0 -1  => paddocks = {0,2,1}
0 -1  2  
0  1 -1
'''
def all_adjacent_paddocks(grid,i,j):

    paddocks = set()
    if i > 0:
        if grid[i-1][j] > -1:
            paddocks.add(grid[i-1][j])

    if i < len(grid)-1:
        if grid[i+1][j] > -1:
            paddocks.add(grid[i+1][j])

    if j > 0:
        if grid[i][j-1] > -1:
            paddocks.add(grid[i][j-1])

    if j < len(grid[i])-1:
        if grid[i][j+1] > -1:
            paddocks.add(grid[i][j+1])

    return paddocks
'''
num_paddocks: records the total number of paddocks
paddock_stats: records the size("area") and the border length ("perimeter") of each paddock
'''
def calculate_paddock_stats(grid):
    working_grid = pad_with_ridges(grid)
    paddock_stats = dict()
    num_paddocks = 0    # np.max(grid)
    for i in range(len(working_grid)):
        for j in range(len(working_grid[i])):
            # cell refers to the paddock_id
            cell = working_grid[i][j]
            if cell > num_paddocks:
                num_paddocks = cell
            if cell > -1:
                if cell in paddock_stats:
                    paddock_stats[cell]["area"] = paddock_stats[cell]["area"] + 1
                else:
                    paddock_stats[cell] = dict()
                    paddock_stats[cell]["area"] = 1
                    paddock_stats[cell]["perimeter"] = 0
            else:
                adjacent_paddocks = all_adjacent_paddocks(working_grid,i,j)
                if len(adjacent_paddocks) > 0:
                    for paddock in adjacent_paddocks:
                        if paddock in paddock_stats:
                            # Some paddock cells may be used for the perimeter multiple times, because we need to account for each edge of the cell that's on the perimeter
                            paddock_stats[paddock]["perimeter"] = paddock_stats[paddock]["perimeter"] + 1
                        else:
                            paddock_stats[paddock] = dict()
                            paddock_stats[paddock]["area"] = 0
                            paddock_stats[paddock]["perimeter"] = 1
    # to fit the index
    paddock_stats["num_paddocks"] = num_paddocks + 1
    return paddock_stats

'''
Count each paddock from 0 to n
'''
def identify_paddocks(grid):
    working_grid = []
    # np.where(grid == 0, -1000, grid)
    '''
    Turn the number that represent paddock into a different number(could be any but -1000 
    in this case) besides 0, because there is paddock 0 that need to be represent as all 0s.
    '''
    for i in range(len(grid)):
        row = []
        for j in range(len(grid[i])):
            row.append(int(grid[i][j]))
            if row[j] == 0:
                row[j] = -1000
        working_grid.append(row)
    # Add border
    working_grid = pad_with_ridges(working_grid)
    current_paddock = 0
    # Count each paddock
    for i in range(len(working_grid)):
        for j in range(len(working_grid[i])):
            if working_grid[i][j] == -1000:
                working_grid = fill_area(working_grid,i, j, current_paddock)
                current_paddock = current_paddock + 1
    # Remove border
    working_grid = remove_padding(working_grid)
    return working_grid

'''
Mark each paddock, from paddock 0 to paddock n.
Every cell in each paddock will turn into the paddock index.
'''
def fill_area(grid, x, y, paddock):
    new_grid = grid.copy()
    cells_visited = set()
    cells_to_visit = set()
    cells_to_visit.add((x,y))
    while len(cells_to_visit) > 0:
        cell = cells_to_visit.pop()
        i = cell[0]
        j = cell[1]
        if i > -1 and i < len(grid):
            if j > -1 and j < len(grid[i]):
                if new_grid[i][j] == -1000:
                    new_grid[i][j] = paddock
                    if (i-1,j) not in cells_visited:
                        cells_to_visit.add((i-1,j))
                    if (i+1,j) not in cells_visited:
                        cells_to_visit.add((i+1,j))
                    if (i,j-1) not in cells_visited:
                        cells_to_visit.add((i,j-1))
                    if (i,j+1) not in cells_visited:
                        cells_to_visit.add((i,j+1))
        cells_visited.add(cell)
    return new_grid

"""
Changing the grid into binary format, marking the ridges/gullies/trenches as -1, and 
the flat space is 0.
"""
def parse_paddocks_input(input):
    ridges = input["ridges"]
    gullies = input["gullies"]
    trenches = input["trenches"]
    overlay = []
    # ridges | gullies | trenches
    for i in range(len(ridges)):
        row = []
        for j in range(len(ridges[0])):
            if ridges[i][j] or gullies[i][j] or trenches[i][j]:
                row.append(-1)
            else:
                row.append(0)
        overlay.append(row)
    return overlay

'''
0  0 -1  2  2  2       0  0 -1  2  2  2 
0 -1 -1  2  2  2       0 -1  2  2  2  2
0 -1 -1  2  2  2  --\  0  0 -1  2  2  2
0 -1 -1 -1  2  2  --/  0 -1  3 -1  2  2 Ψ(￣∀￣)Ψ
0 -1  3 -1  2  2       0 -1  3 -1  2  2
0 -1  3 -1  2  2       0 -1  3 -1  2  2

Clear the redundant fence
'''
def clean_fence_lines(overlay):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j] == -1:
                adjacent = all_adjacent_paddocks(overlay,i,j)
                if len(adjacent) == 1:
                    overlay[i][j] = adjacent.pop()
    return overlay

def reset_paddocks(overlay):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j]>0:
                overlay[i][j] = 0
    return overlay

'''27/04/2022
'''
def initialized(overlay,is_update):
    output = {}
    if is_update:
        overlay = reset_paddocks(overlay)
    overlay = identify_paddocks(overlay)
    overlay = clean_fence_lines(overlay)
    output["overlay"] = overlay
    paddock_stats = calculate_paddock_stats(overlay)
    areas = []
    perimeters = []
    for i in range(0, paddock_stats["num_paddocks"]):
        areas.append(paddock_stats[i]["area"])
        perimeters.append(paddock_stats[i]["perimeter"])
    output["areas"] = areas
    output["perimeters"] = perimeters
    return output

"""
paddock division funciton
"""
def paddocks(frontend_input):
    #output = {}
    # print(len(frontend_input['ridges']))
    # print(len(frontend_input['gullies']))
    # print(len(frontend_input['trenches']))
    overlay = parse_paddocks_input(frontend_input)
    output = initialized(overlay,False)

    # For jiaan testing use 27/04/2022
    '''    
    with open("output", "wb") as fp:   #Pickling
        pickle.dump(output, fp)
    '''
    output = merge_undersized_paddocks(output)
    output = split_oversized_paddocks(output)
    
    return output

"""
num = 0 -> 90%
num = 1 -> 10%
(2022S1)
"""
def control_paddock_size(paddock, num):
    '''
    sort()
    percent * total number
    take the concurrent index
    '''
    areas_list = copy.deepcopy(paddock["areas"])
    areas_list.sort()
    output = -1
    if num == 0:
        output = areas_list[math.floor(len(areas_list)*0.9) - 1] #28/04/2022 modified
    else:
        output = areas_list[math.ceil(len(areas_list)*0.1)] #28/04/2022 modified
    return output


def all_neighbour_paddocks(paddocks, paddock_index):
    workingGrid = paddocks["overlay"]
    neighbours = set()
    for i in range(len(workingGrid)):
        for j in range(len(workingGrid[0])):#i
            # Only check the fences -- 2022（￣︶￣）↗　
            if workingGrid[i][j] == -1:
                currentNeighbours = all_adjacent_paddocks(workingGrid, i, j)
                if (len(currentNeighbours) > 1) and (paddock_index in currentNeighbours):
                    neighbours = neighbours | currentNeighbours #set union
    neighbours.remove(paddock_index)
    return neighbours



def overwrite_paddock(overlay, old_paddock, new_paddock):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j]== old_paddock:
                overlay[i][j] = new_paddock
    # Clean the fence between paddock -- 2022（￣︶￣）↗
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):#i
            if overlay[i][j] == -1:
                currentNeighbours = all_adjacent_paddocks(overlay, i, j)
                if (len(currentNeighbours) == 1) and (new_paddock in currentNeighbours):
                    overlay[i][j] = new_paddock
    return overlay

def find_largest_neighbour(paddocks, paddock_index):
    neighbours = all_neighbour_paddocks(paddocks, paddock_index)
    largestNeighbour = neighbours.pop()
    largestSize = paddocks["areas"][largestNeighbour]
    for paddock in neighbours:
        if paddocks["areas"][paddock] > largestSize:
            largestSize = paddocks["areas"][paddock]
            largestNeighbour = paddock  # 28/04/2022 add
    return [paddock_index, largestNeighbour]


###################
#                 #
# merge paddocks  #
#                 #
###################

def merge_undersized_paddocks(paddocks):
    min_size = control_paddock_size(paddocks, 1) #2022
    workingPaddocks = copy.deepcopy(paddocks)
    min_paddocks = []
    currentPaddock = 0
    exitSmallPaddock = 0

    while currentPaddock < len(workingPaddocks["areas"]):
        if workingPaddocks["areas"][currentPaddock] <= min_size:

            array = find_largest_neighbour(workingPaddocks,currentPaddock)
            min_paddocks.append(array)
        currentPaddock = currentPaddock + 1
    while exitSmallPaddock < len(min_paddocks):
        small_paddock = min_paddocks[exitSmallPaddock][0]
        largest_neighbour = min_paddocks[exitSmallPaddock][1]
        paddocks["overlay"] = overwrite_paddock(paddocks["overlay"],small_paddock,largest_neighbour)
        exitSmallPaddock += 1
    paddocks = initialized(paddocks["overlay"],True)
    return paddocks

'''
1. adding bourders(using function pad_with_ridges()) to the overlay
2. find two horizontal points that approximately cut the perimeters into half
   it's aim to prevent split undersized paddock(<10%)
3. change the points between them to -1
[-1, -1, -1, -1, -1, -1, -1, -1] 
[-1, -1,  0,  0,  0,  0,  0, -1] 
[-1,  1,[-1],  #, #,  #, #,[-1]]
[-1,  1,  1, -1, -1,  0,  0, -1] 
[[-1],*,  *,  *,  *,[-1], 0, -1] 
[-1,  1,  1,  1,  1, -1,  0, -1]
[-1,  1,  1,  1,  1, -1,  0, -1] 
[-1, -1, -1, -1, -1, -1, -1, -1]
4. remove the boarder(using function remove_padding() )
'''
def split_paddocks(paddocks, paddock_index):
    perimeter = paddocks["perimeters"][paddock_index]
    half_perimeter = math.floor(perimeter/2)
    count = 0
    point_one = [0,0] # 28/04/2022 modified
    point_two = [0,0] # 28/04/2022 modified
    output = pad_with_ridges(paddocks["overlay"])
    for i in range(len(output)):
        for j in range(len(output[0])):
            if output[i][j] == -1 and count < half_perimeter: 
                adjacent_paddocks = all_adjacent_paddocks(output,i,j)
                if (paddock_index in adjacent_paddocks):
                    count += 1
                    if count % 2 == 0:
                        point_one = [i,j]
                    else:
                        point_two = [i,j]
                    if count >= half_perimeter:
                        if point_one[0] > point_two[0]:
                            count -= 1
                        if abs(point_one[1]-point_two[1]) == 1:
                            count -= 1
                            
    # 26/04/2022 
    index = point_one[0]
    front = 0
    '''28/04/2022
    find the front index of the left point
    [-1,  1, -1,  #,  #,  #,  #, -l], 
              ^
          it's index
    '''
    if point_one[1] > point_two[1]:
        front = point_two[1]
    else:
        front = point_one[1]

    for i in range(abs(point_one[1] - point_two[1])):
        output[index][i+front] = -1   
    paddocks["overlay"] = remove_padding(output) 
    return paddocks

###################
#                 #
# split paddocks  #
#                 #
###################

"""
Split paddocks that is larger than max_size(90% in this case)
(2022S1)
"""
def split_oversized_paddocks(paddocks):
    max_size = control_paddock_size(paddocks, 0)
    output = copy.deepcopy(paddocks)
    # 27/04
    currentPaddock = 0
    for area in output["areas"]:
        if area >= max_size:
            output = split_paddocks(output, currentPaddock)
        currentPaddock+=1
    output = initialized(output["overlay"],True)
    return output
