# from paddock_plan.paddocks import paddocks, combine_min_paddocks
import paddocks as paddock # 2022
import unittest
# Before run tests, please run "cd /home/j/r/tl-2021/Backend" in terminal
'''
modified 28/04/2022
'''
class TestPaddock(unittest.TestCase):
    def test_small(self):
        print("Testing paddocks and stats function...")
        array = [False, False, False, False, False,False,False,False]

        empty = []
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)
        empty.append(array)

        array1 = [False, True, True, False, False, False, False, False]
        array2 = [False, False, True, False, False, False, True, True]
        array3 = [False, False, True, False, False, True, False, False]
        array4 = [False, False, True, True, True, False, True, True]
        array5 = [False, True, False, False, True,False,False,False]
        array6 = [False, True, True, False, True,True,True,True]
        array7 = [False, False, True, False, False,True,True,False]
        array8 = [False, False, True, False, False,True,True,False]
        array9 = [False, False, True, False, False,True,False,False]

        ridges = []
        ridges.append(array1)
        ridges.append(array2)
        ridges.append(array3)
        ridges.append(array4)
        ridges.append(array5)
        ridges.append(array6)
        ridges.append(array7)
        ridges.append(array8)
        ridges.append(array9)


        test_input = {}
        test_input["ridges"] = ridges
        test_input["gullies"] = empty
        test_input["trenches"] = empty

        output = paddock.paddocks(test_input) # 2022
        expected_output = { 'overlay': [[0, 0, -1, 1, 1, 1, 1, 1],
                                        [0, 0, -1, 1, 1, 1, 1, 1],
                                        [0, 0, -1, 1, 1, -1, 1, 1],
                                        [-1, 0, -1, -1, -1, 2, -1, -1],
                                        [3, -1, 4, 4, -1, 2, 2, 2],
                                        [3, 3, -1, 4, 4, -1, 2, -1],
                                        [3, 3, 3, -1, -1, 5, -1, 5],
                                        [3, 3, -1, 5, 5, 5, 5, 5],
                                        [3, 3, -1, 5, 5, 5, 5, 5]], 
                            'areas': [7, 14, 5, 10, 4, 12], 
                            'perimeters': [11, 16, 9, 13, 8, 15]}
        print(output)
        self.assertEqual(output, expected_output)

        '''
        if output == expected_output:
            print("Test Passed!")
            return True
        else:
            print("Test Failed\n")
            print("Expected Output:")
            print("Overlay")
            for i in range(len(expected_output["overlay"])):
                print(expected_output["overlay"][i])
            print("Areas:")
            print(expected_output["areas"])
            print("Perimeters:")
            print(expected_output["perimeters"])
            print("\nActual Output:")
            for i in range(len(output["overlay"])):
                print(output["overlay"][i])
            print("Areas:")
            print(output["areas"])
            print("Perimeters:")
            print(output["perimeters"])
            return False

        '''
if __name__ == '__main__':
    #uncomment line to test the paddocks function
    unittest.main()
