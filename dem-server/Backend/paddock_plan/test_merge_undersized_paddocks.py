import paddocks as paddock # 2022S1
import unittest
# Before run tests, please run "cd /home/j/r/tl-2022/Backend" in terminal

class TestMerge(unittest.TestCase):
    def test_small(self):
        array1 = [-1, 0, 0, 0, 0]
        array2 = [1, -1, 0, 0, 0]
        array3 = [1, -1, -1, -1, -1]
        array4 = [-1, -1, 3, 3, 3]
        array5 = [2, -1, 3, 3, 3]


        test_overlay = []
        test_overlay.append(array1)
        test_overlay.append(array2)
        test_overlay.append(array3)
        test_overlay.append(array4)
        test_overlay.append(array5)

        output = paddock.initialized(test_overlay,False)
        output = paddock.merge_undersized_paddocks(output)  #2022

        expected_output = {'overlay': [[0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0],
                                        [0, 0, -1, -1, -1],
                                        [-1, -1, 1, 1, 1],
                                        [1, 1, 1, 1, 1]], 
                            'areas': [12, 8], 
                            'perimeters': [15, 13]}
        self.assertEqual(output, expected_output)


    def test_large(self):
        output = {}
        with open("test_large_in_2.txt", "r") as file:
            overlay = [[int(x) for x in line.split()] for line in file]
        output["overlay"] = overlay
        output["areas"] = [2630, 12, 754, 10, 8, 435, 170, 803, 503, 1, 526, 2315, 5, 2, 84, 393, 387, 2, 12, 1, 356, 296, 71, 716, 444, 199, 283, 1, 152, 481, 1300, 668, 1, 373, 1343, 404, 36, 302, 1543, 961, 158, 271, 727, 1, 843, 252, 2, 13, 417, 128, 74, 554, 172, 100, 207, 1, 53, 484, 1520, 217, 386, 455, 2, 414, 159, 1, 38, 417, 23, 1284, 22, 1, 4, 1, 479, 1, 128, 1500, 59, 238, 71, 662, 72, 220, 4, 169, 171, 260, 139, 272, 12, 13, 188, 3, 3, 291, 1, 218, 193, 218, 5, 1876, 3, 1, 256, 1, 278, 48, 1, 1]
        output["perimeters"] = [425, 14, 142, 15, 13, 140, 50, 179, 122, 4, 132, 214, 10, 6, 36, 121, 96, 6, 16, 4, 80, 91, 55, 96, 98, 64, 73, 4, 48, 106, 182, 101, 4, 83, 166, 131, 23, 70, 169, 133, 57, 84, 161, 4, 124, 77, 6, 17, 97, 47, 38, 117, 51, 45, 55, 4, 32, 93, 164, 88, 97, 86, 6, 89, 80, 4, 29, 100, 22, 176, 22, 4, 8, 4, 123, 4, 47, 191, 33, 77, 31, 105, 29, 54, 8, 49, 52, 69, 47, 74, 13, 14, 66, 7, 7, 78, 4, 75, 51, 63, 10, 217, 7, 4, 63, 4, 97, 30, 4, 4]
        output = paddock.initialized(output["overlay"],False)
        paddocks = paddock.merge_undersized_paddocks(output)

        with open("test_large_out_2.txt", "r") as file:
            overlay2 = [[int(x) for x in line.split()] for line in file]

        #for i in range(len(paddocks["overlay"])):
            #print(paddocks["overlay"][i])
        
        self.assertEqual(paddocks["overlay"], overlay2)

        """
        if output == expected_output:
            print("Test Passed!")
            return True
        else:
            print("Test Failed\n")
            print("Expected Output:")
            print("Overlay")
            for i in range(len(expected_output["overlay"])):
                print(expected_output["overlay"][i])
            print("Areas:")
            print(expected_output["areas"])
            print("Perimeters:")
            print(expected_output["perimeters"])
            print("\nActual Output:")
            for i in range(len(output["overlay"])):
                print(output["overlay"][i])
            print("Areas:")
            print(output["areas"])
            print("Perimeters:")
            print(output["perimeters"])
            return False
            """

'''
2022
'''
if __name__ == '__main__':
    unittest.main()
