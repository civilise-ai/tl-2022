# 2022S1
# Before run tests, please run "cd /home/j/r/tl-2022/Backend" in terminal
import copy
import paddocks as paddock
import unittest
#from .paddocks import paddocks, split_oversized_paddocks

class TestSplit(unittest.TestCase):


    def test_large(self):
        #print("Testing paddock split function...")
        empty = []
        overlay = []

        array1 = [0]*200
        

        for i in range(200):
            empty.append(copy.deepcopy(array1))
        
        overlay = copy.deepcopy(empty)
        for i in range(200):
            overlay[i][i] = -1
            if i > 30:
                overlay[i][30] = -1
        
        paddocks = paddock.initialized(overlay,False)
        paddocks = paddock.split_oversized_paddocks(paddocks)
        expected_output = {}
        expected_output["areas"] = [8576, 5537, 8001, 11175, 6069]
        expected_output["perimeters"] = [448, 429, 379, 448, 378]
        

        with open("test_large_out.txt", "r") as file:
            expected_overlay = [[int(x) for x in line.split()] for line in file]
        expected_output["overlay"] = expected_overlay

        self.assertEqual(paddocks, expected_output)

        """
        
        if paddocks == expected_output:
            print("Testcase large Passed!")
            return True

        else:
            for i in range(len(paddocks["overlay"])):
                print(paddocks["overlay"][i])
            print("Areas:")
            print(paddocks["areas"])
            print("Perimeters:")
            print(paddocks["perimeters"])
            print("\nActual Output:")
        """

    def test_medium(self):
        empty = []
        overlay = []

        array1 = [0]*100
        

        for i in range(100):
            empty.append(copy.deepcopy(array1))
        
        overlay = copy.deepcopy(empty)
        for i in range(100):
            overlay[i][60] = -1
            overlay[32][i] = -1
        
        paddocks = paddock.initialized(overlay,False)
        paddocks = paddock.split_oversized_paddocks(paddocks)
        expected_output = {}
        expected_output["areas"] = [1920, 1248, 1920, 2614, 2040]
        expected_output["perimeters"] = [184, 142, 184, 212, 188]
        
        
        with open("test_medium_out.txt", "r") as file:
            expected_overlay = [[int(x) for x in line.split()] for line in file]
        expected_output["overlay"] = expected_overlay

        self.assertEqual(paddocks, expected_output)
        # """


    def test_small(self):
        array1 = [-1, 0, 0, 0, 0,0]
        array2 = [1, -1, 0, 0, 0,0]
        array3 = [1, 1, -1, -1, 0,0]
        array4 = [1, 1, 1, 1, -1,0]
        array5 = [1, 1, 1, 1, -1,0]
        array6 = [1, 1, 1, 1, -1,0]

        expected_overlay = []
        expected_overlay.append(array1)
        expected_overlay.append(array2)
        expected_overlay.append(array3)
        expected_overlay.append(array4)
        expected_overlay.append(array5)
        expected_overlay.append(array6)

        output = paddock.initialized(expected_overlay,False)
        output = paddock.split_oversized_paddocks(output)  #2022
        expected_output = {'overlay': [[-1, 0, 0, 0, 0, 0], 
                                    [1, -1, 0, 0, -1, -1], 
                                    [1, 1, -1, -1, 2, 2], 
                                    [-1, -1, 3, 3, -1, 2], 
                                    [3, 3, 3, 3, -1, 2], 
                                    [3, 3, 3, 3, -1, 2]], 
                    'areas': [7, 3, 5, 10], 
                    'perimeters': [12, 7, 11, 13]}

        self.assertEqual(output, expected_output)
        """
        
        if output == expected_output:
            print("Testcase small Passed!")
            return True
        else:
            print("Test Failed\n")
            print("Expected Output:")
            print("Overlay")
            for i in range(len(expected_output["overlay"])):
                print(expected_output["overlay"][i])
            print("Areas:")
            print(expected_output["areas"])
            print("Perimeters:")
            print(expected_output["perimeters"])
            print("\nActual Output:")
            for i in range(len(output["overlay"])):
                print(output["overlay"][i])
            print("Areas:")
            print(output["areas"])
            print("Perimeters:")
            print(output["perimeters"])
            return False
        """
        
if __name__ == '__main__':
    unittest.main()
