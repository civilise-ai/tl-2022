import { ChangeDetectionStrategy, Component, ElementRef, OnInit } from '@angular/core';
import { ColorSketchModule } from 'ngx-color/sketch';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResearchComponent implements OnInit {

  headings: Element[];

  constructor(
    private elementRef: ElementRef<HTMLElement>,
  ) { }

  ngOnInit(): void {
    this.setHeadings();
  }

  private setHeadings(): void {
    const headings: Element[] = [];
    this.elementRef.nativeElement
      .querySelectorAll('h2')
      .forEach(x => headings.push(x));
    this.headings = headings;
  }

  onLoad($event) {
    console.log("Load", $event)
  }
  onError($event) {
    console.log("Error", $event)
  }
}
