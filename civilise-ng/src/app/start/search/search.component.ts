import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LayerService, Layer } from 'src/app/services/layers.service';
import { ChartService, Chart } from 'src/app/services/chart.service';
import { TextureService, Texture } from 'src/app/services/texture.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  initFormGroup: FormGroup;
  openedLeftDrawer: boolean = true;
  coords: string;
  dem: Layer = {};
  chart: Chart = {}

  constructor(
    public formBuilder: FormBuilder,
    private layerService: LayerService,
    private chartService: ChartService,
    private textureService: TextureService,
  ) {
    this.initFormGroup = formBuilder.group({
      paramCoords: ['', Validators.requiredTrue]
    });
    // this.layerService.getDem.subscribe((dem: Layer) => {
    //   this.dem = dem;
    // });
  }

  onStart(coords?: string): void {
    this.coords = this.initFormGroup.value.paramCoords
      .trim()
      .split(' ')
      .join('');
    if (coords) this.coords = coords;
    if (this.coords) {
      this.layerService.setDem({
        value: this.coords,
        loading: true
      });
      this.chartService.setChart({
        value: this.coords,
        loading: true
      });
      this.textureService.setTexture({
        value: this.coords,
        loading: true
      })
    }
  }

  onClickExample(coords: string): void {
    this.initFormGroup.controls['paramCoords'].setValue(coords);
  }
}
