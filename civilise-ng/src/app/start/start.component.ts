import { AfterContentInit, Component, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';
import { Chart, ChartService } from '../services/chart.service';

import { LayerService, Layer } from '../services/layers.service';
import { Texture, TextureService } from '../services/texture.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements AfterContentInit {
  @ViewChild('tabs') tabGroup: MatTabGroup;

  dem: Layer;
  chart: Chart;
  texture: Texture;

  constructor(private layerService: LayerService, private chartService: ChartService, private textureService: TextureService) {
    this.layerService.getDem.subscribe((dem: Layer) => {
      this.dem = dem;
      if (!this.dem.loading) this.tabGroup.selectedIndex = 1;
    });

    this.chartService.getChart.subscribe((chart: Chart) => {
      this.chart = chart;
      // if (!this.chart.loading) this.tabGroup.selectedIndex = 2;
    })
    this.textureService.getTexture.subscribe((texture: Texture) => {
      this.texture = texture;
      // if (!this.chart.loading) this.tabGroup.selectedIndex = 2;
    })
  }

  ngAfterContentInit(): void {
    // load query params and update the apps starting state accordingly
    this.layerService.queryParamsGet();
    this.chartService.queryParamsGet();
    this.textureService.queryParamsGet();
  }
}
