import { Component, OnInit } from '@angular/core';
import { IData, Chart, ChartService } from 'src/app/services/chart.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TextureData, TextureService } from 'src/app/services/texture.service';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent {


  data: Map<string, IData[]>;
  textureData: TextureData;

  initFormGroup: FormGroup;
  openedLeftDrawer: boolean = true;

  carbonStored: IData[];
  carbonLoss: IData[];
  decomposingMicrobes: IData[];
  labileDetritus: IData[];
  mineral: IData[];
  soilLoss: IData[];
  soilMass: IData[];
  stableDetritus: IData[];

  soilTextureOn: boolean = true;
  carbonStoredOn: boolean = true;
  carbonLossOn: boolean = true;
  decomposingMicrobesOn: boolean = true;
  labileDetritusOn: boolean = true;
  mineralOn: boolean = true;
  soilLossOn: boolean = true;
  soilMassOn: boolean = true;
  stableDetritusOn: boolean = true;

  constructor(private chartService: ChartService,
    private textureService: TextureService) {

    this.textureService.getTexture.subscribe(texture => {
      this.textureData = texture.data;
    });

    this.chartService.getChart.subscribe(chart => {
      this.data = chart.data;
      this.carbonStored = this.data.get("CarbonStored");
      this.carbonLoss = this.data.get("CarbonLoss");
      this.decomposingMicrobes = this.data.get("DecomposingMicrobes");
      this.labileDetritus = this.data.get("LabileDetritus");
      this.mineral = this.data.get("Mineral");
      this.soilLoss = this.data.get("SoilLoss");
      this.soilMass = this.data.get("SoilMass");
      this.stableDetritus = this.data.get("StableDetritus");
    });


  }


  onStart(): void {

  }

  onClickExample(coords: string): void {

  }

  onChangeCarbonLossOn(): void {

  }

  onChangeCarbonStoredOn(): void {

  }

  onChangeDecomposingMicrobesOn(): void {

  }

  onChangeLabileDetritusOn(): void {

  }

  onChangeMineralOn(): void {

  }

  onChangeSoilLossOn(): void {

  }

  onChangedSoilMassOn(): void {

  }

  onChangeStableDetritusOn(): void {

  }

  onChangeSoilTextureOn(): void {

  }
}
