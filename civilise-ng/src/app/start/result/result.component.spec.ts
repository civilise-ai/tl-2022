import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { ResultComponent } from './result.component';
import {
  LayerService,
  Layer,
  PaddocksLayer
} from '../../services/layers.service';


describe('ResultComponent', () => {
  let component: ResultComponent;
  let fixture: ComponentFixture<ResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResultComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [LayerService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it(`should have unitGrassCostOn initialised to false`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitGrassCostOn).toBeFalse();
  });

  it(`should have unitTreeCostOn initialised to false`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitTreeCostOn).toBeFalse();
  });

  it(`should have unitTreeFencingCostOn initialised to false`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitTreeFencingCostOn).toBeFalse();
  });

  it(`should have unitFencingCostOn initialised to false`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitFencingCostOn).toBeFalse();
  });

  it(`should have unitGrassCost initialised to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitGrassCost).toEqual('10');
  });

  it(`should have unitTreeCost initialised to '100'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitTreeCost).toEqual('100');
  });

  it(`should have unitFencingCost initialised to '100'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitFencingCost).toEqual('100');
  });

  it(`should have unitTreeFencingCost initialised to '60'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    expect(component.unitTreeFencingCost).toEqual('60');
  });

  it(`test setGrassCost to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setGrassCost('10');
    expect(component.grassCost).toEqual('10');
  });

  it(`test setTreeCost to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setTreeCost('10');
    expect(component.treeCost).toEqual('10');
  });

  it(`test setFencingCost to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setFencingCost('10');
    expect(component.fencingCost).toEqual('10');
  });


  it(`test setTreeFencingCost to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setTreeFencingCost('10');
    expect(component.treefencingCost).toEqual('10');
  });


  it(`test setPaddockID to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setPaddockID('10');
    expect(component.paddockID).toEqual('10');
  });

  it(`test setPaddockArea to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setPaddockArea('10');
    expect(component.paddockArea).toEqual('10');
  });

  it(`test setPaddockPerimeter to '10'`, () => {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    component.setPaddockPerimeter('10');
    expect(component.paddockPerimeter).toEqual('10');
  });
});
