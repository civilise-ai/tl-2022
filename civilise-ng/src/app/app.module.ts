import { BrowserModule } from '@angular/platform-browser';
import { NgModule, SecurityContext } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AgmCoreModule } from '@agm/core';
import { ColorSketchModule } from 'ngx-color/sketch'; // <color-sketch></color-sketch>
import { ColorBlockModule } from 'ngx-color/block'; // <color-block></color-block>
import { MarkdownModule } from 'ngx-markdown';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AnalysisComponent } from './start/analysis/analysis.component';
import { FooterComponent } from './components/footer/footer.component';
import { LinechartComponent } from './components/linechart/linechart.component';
import { MapComponent } from './components/map/map.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PaddocksComponent } from './components/paddocks/paddocks.component';
import { PaletteComponent } from './components/palette/palette.component';
import { ResearchComponent } from './start/research/research.component';
import { ResultComponent } from './start/result/result.component';
import { SearchComponent } from './start/search/search.component';
import { StartComponent } from './start/start.component';
import { ThreeComponent } from './components/three/three.component';
import { ScrollspyNavLayoutModule } from './shared/scrollspy-nav-layout/scrollspy-nav-layout.module';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    StartComponent,
    ThreeComponent,
    PaddocksComponent,
    MapComponent,
    LinechartComponent,
    SearchComponent,
    ResearchComponent,
    ResultComponent,
    AnalysisComponent,
    FooterComponent,
    PaletteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatSliderModule,
    MatInputModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTabsModule,
    AgmCoreModule.forRoot({
      libraries: ['drawing'],
      apiKey: 'AIzaSyCbfLPwsSep65RtJKI7thADi2T1MEZLcWQ'
    }),
    MarkdownModule.forRoot({
      loader: HttpClient, sanitize: SecurityContext.NONE,
    }),
    ScrollspyNavLayoutModule,
    MatAutocompleteModule,
    MatTooltipModule,
    ColorSketchModule,
    ColorBlockModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
