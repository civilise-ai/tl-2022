import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from './message.service';

export interface Texture {
    on?: boolean;
    value?: string;
    data?: any;
    loading?: boolean;
    color?: any;
}

export interface TextureData {
    perSand: number,
    perSilt: number,
    perClay: number,
}

@Injectable({ providedIn: 'root' })
export class TextureService {


    public api = window.location.origin + '/soil/texture';
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(
        private http: HttpClient,
        private messageService: MessageService,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }


    /*
     *  DEM:   props, get, set
     **/
    private texture: Texture = {
        value: undefined, // coords
        data: undefined,
        loading: undefined // start off as null to indicate not loading nor loaded
        // can we record the shape and parsed_coords here too?
    };
    setTexture = (texture: Texture) => {
        // update the layer state
        if (texture.data) this.texture.data = texture.data;
        if (texture.loading) this.texture.loading = texture.loading;
        if (texture.value) this.texture.value = texture.value;

        // update the url query string
        this.queryParamsSet('coords', this.texture.value);
        // trigger loading/loaded state transitions used for searching to found result
        this.getTexture.next(this.texture);
        // call the api - this is a brand new search. Use all (but only) the parameters specifies in the url
        this.httpDefault(
            {
                coords: this.texture.value
            },
            {},
            {}
        ).subscribe(async (result) => {
            // DEM
            this.texture.loading = false;

            let textureData: TextureData = { perSilt: result.perSilt, perSand: result.perSand, perClay: result.perClay }
            this.texture.data = textureData;
            this.getTexture.next(this.texture);
            this.prevParams.coords = this.texture.value;
        });
        // Might need to reset all the other layers here too?
    };

    getTexture = new Subject<any>();

    queryParamsSet(k, v) {
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: { [k]: v },
            queryParamsHandling: 'merge'
        });
    }

    queryParamsGet = () => {
        this.route.queryParamMap.subscribe((queryParamMap) => {
            let coords = queryParamMap.get('coords');
            if (coords && !this.texture.value) {
                this.setTexture({ value: coords, loading: true });
            }
        });
    };

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    // used by flask to consider what layers require re-calculation
    prevParams = {
        coords: undefined,
    };
    httpDefault(params: any, layers: any, prevParams: any): Observable<any> {

        // This creates a url requesting just a single layer and provides the relevant params
        let url = `${this.api}?`;
        if (params.coords) {
            url += `coords=${params.coords}`;
        }
        return this.http
            .post<any>(
                url,
                { layers: layers, prevParams: prevParams },
                this.httpOptions
            )
            .pipe(
                // return this.http.post<any>(url, layers, this.httpOptions).pipe(
                tap((result: any) => {
                }),
                catchError(this.handleError<any>('httpDefault'))
            );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
}
