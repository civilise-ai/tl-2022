import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from './message.service';

export interface Chart {
    on?: boolean;
    value?: string;
    data?: any;
    loading?: boolean;
    color?: any;
}

export interface IData {
    label: number,
    value: number
}

@Injectable({ providedIn: 'root' })
export class ChartService {


    public api = window.location.origin + '/soil/analysis';
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(
        private http: HttpClient,
        private messageService: MessageService,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }


    /*
     *  DEM:   props, get, set
     **/
    private chart: Chart = {
        value: undefined, // coords
        data: undefined,
        loading: undefined // start off as null to indicate not loading nor loaded
        // can we record the shape and parsed_coords here too?
    };
    setChart = (chart: Chart) => {
        // update the layer state
        if (chart.data) this.chart.data = chart.data;
        if (chart.loading) this.chart.loading = chart.loading;
        if (chart.value) this.chart.value = chart.value;
        // update the url query string
        this.queryParamsSet('coords', this.chart.value);
        // trigger loading/loaded state transitions used for searching to found result
        this.getChart.next(this.chart);
        // call the api - this is a brand new search. Use all (but only) the parameters specifies in the url
        this.httpDefault(
            {
                coords: this.chart.value
            },
            {},
            {}
        ).subscribe(async (result) => {
            // DEM
            this.chart.loading = false;

            let charts = new Map<string, IData[]>();

            var CarbonLossData: IData[] = [];
            for (var i = 0; i < result.Carbon_Loss.length; i++) {
                CarbonLossData.push(<IData>{ label: i, value: result.Carbon_Loss[i] })
            }
            charts.set("CarbonLoss", CarbonLossData)

            var CarbonStoredData: IData[] = [];
            for (var i = 0; i < result.Carbon_Stored.length; i++) {
                CarbonStoredData.push(<IData>{ label: i, value: result.Carbon_Stored[i] })
            }
            charts.set("CarbonStored", CarbonStoredData)

            var DecomposingMicrobesData: IData[] = [];
            for (var i = 0; i < result.Decomposing_Microbes.length; i++) {
                DecomposingMicrobesData.push(<IData>{ label: i, value: result.Decomposing_Microbes[i] })
            }
            charts.set("DecomposingMicrobes", DecomposingMicrobesData)

            var LabileDetritusData: IData[] = [];
            for (var i = 0; i < result.Labile_Detritus.length; i++) {
                LabileDetritusData.push(<IData>{ label: i, value: result.Labile_Detritus[i] })
            }
            charts.set("LabileDetritus", LabileDetritusData)

            var MineralData: IData[] = [];
            for (var i = 0; i < result.Mineral.length; i++) {
                MineralData.push(<IData>{ label: i, value: result.Mineral[i] })
            }
            charts.set("Mineral", MineralData)

            var SoilLossData: IData[] = [];
            for (var i = 0; i < result.Soil_Loss.length; i++) {
                SoilLossData.push(<IData>{ label: i, value: result.Soil_Loss[i] })
            }
            charts.set("SoilLoss", SoilLossData)

            var SoilMassData: IData[] = [];
            for (var i = 0; i < result.Soil_Mass.length; i++) {
                SoilMassData.push(<IData>{ label: i, value: result.Soil_Mass[i] })
            }
            charts.set("SoilMass", SoilMassData)

            var StableDetritusData: IData[] = [];
            for (var i = 0; i < result.Stable_Detritus.length; i++) {
                StableDetritusData.push(<IData>{ label: i, value: result.Stable_Detritus[i] })
            }
            charts.set("StableDetritus", StableDetritusData)


            this.chart.data = charts;
            this.getChart.next(this.chart);
            this.prevParams.coords = this.chart.value;
        });
        // Might need to reset all the other layers here too?
    };

    getChart = new Subject<any>();

    queryParamsSet(k, v) {
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: { [k]: v },
            queryParamsHandling: 'merge'
        });
    }

    queryParamsGet = () => {
        this.route.queryParamMap.subscribe((queryParamMap) => {

            // checks to avoid double calling the api
            let coords = queryParamMap.get('coords');
            if (coords && !this.chart.value) {
                this.setChart({ value: coords, loading: true });
            }
        });
    };

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    // used by flask to consider what layers require re-calculation
    prevParams = {
        coords: undefined,
        trenchesEven: undefined,
        trenchesStep: undefined,
        gullies: undefined,
        ridges: undefined
    };
    httpDefault(params: any, layers: any, prevParams: any): Observable<any> {

        // This creates a url requesting just a single layer and provides the relevant params
        let url = `${this.api}?`;
        if (params.coords) {
            url += `coords=${params.coords}`;
        }
        return this.http
            .post<any>(
                url,
                { layers: layers, prevParams: prevParams },
                this.httpOptions
            )
            .pipe(
                // return this.http.post<any>(url, layers, this.httpOptions).pipe(
                tap((result: any) => {
                }),
                catchError(this.handleError<any>('httpDefault'))
            );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    private dataSubject = new BehaviorSubject<IData[]>(this.chart.data);
    $data = this.dataSubject.asObservable();
    addData(newData: IData) {
        this.chart.data.push(newData);
        this.dataSubject.next(this.chart.data);
    }


}
