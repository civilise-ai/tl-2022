import { Component, ElementRef, AfterViewInit, ViewChild, Input, OnChanges, OnInit } from '@angular/core';
import { IData, ChartService } from '../../services/chart.service';

import * as D3 from "d3";

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss']
})

export class LinechartComponent implements AfterViewInit, OnChanges {

  @ViewChild("containerLineChart") element: ElementRef;
  @Input() data: IData[];
  @Input() title: string;
  @Input() xlabel: string;
  @Input() ylabel: string;


  private host: D3.Selection<d3.BaseType, {}, d3.BaseType, any>;
  private svg: D3.Selection<SVGElement, {}, d3.BaseType, any>;
  private width: number;
  private height: number;
  private radius: number;
  private htmlElement: HTMLElement;
  private chartData: IData[];
  private xsize: string;
  private ysize: string;

  private isEnlarged: boolean;
  enlarge: string = "Enlarge";

  constructor(private chartService: ChartService) { }

  ngAfterViewInit(): void {

    this.htmlElement = this.element.nativeElement;
    this.host = D3.select(this.htmlElement);
    this.chartService.getChart.subscribe(chart => {
      this.chartData = chart.data;
    });
    this.setup();
    this.build();
  }

  ngOnChanges(): void {
    this.setup();
    this.build();
  }

  private setup(): void {
    let base = window.innerWidth * 0.6;
    if (this.isEnlarged) {
      this.width = base;
      this.height = base / 16 * 9;
      this.xsize = "20px";
      this.ysize = "20px";
    } else {
      this.width = base / 2;
      this.height = base / 2 / 16 * 9;
      this.xsize = "14px";
      this.ysize = "14px";
    }

  }

  toggle(): void {
    this.isEnlarged = !this.isEnlarged;
    this.enlarge = (this.isEnlarged) ? "Shrink" : "Enlarge";
    this.setup();
    this.build();
  }

  private build(): void {
    if (this.data !== undefined && this.htmlElement !== undefined) {
      this.buildLineChart(this.data, {
        x: (d: IData) => d.label,
        y: (d: IData) => d.value,
        xLabel: this.xlabel,
        yLabel: this.ylabel,
        xSize: this.xsize,
        ySize: this.ysize,
        height: this.height,
        width: this.width,
        color: "steelblue"

      });
    }
  }

  private buildLineChart(data, {
    x = undefined, // ([x]) => x, // given d in data, returns the (temporal) x-value
    y = undefined, // ([, y]) => y, // given d in data, returns the (quantitative) y-value
    defined = undefined, // for gaps in data
    curve = D3.curveLinear, // method of interpolation between points
    marginTop = 20, // top margin, in pixels
    marginRight = 30, // right margin, in pixels
    marginBottom = 30, // bottom margin, in pixels
    marginLeft = 40, // left margin, in pixels
    width = 640, // outer width, in pixels
    height = 400, // outer height, in pixels
    // xType = D3.scaleUtc, // the x-scale type
    xType = D3.scaleLinear,
    xDomain = undefined, // [xmin, xmax]
    xRange = [marginLeft, width - marginRight], // [left, right]
    xLabel = undefined,
    xSize = undefined,
    yType = D3.scaleLinear, // the y-scale type
    yDomain = undefined, // [ymin, ymax]
    yRange = [height - marginBottom, marginTop], // [bottom, top]
    yFormat = undefined, // a format specifier string for the y-axis
    yLabel = undefined, // a label for the y-axis
    ySize = undefined,
    color = "currentColor", // stroke color of line
    strokeLinecap = "round", // stroke line cap of the line
    strokeLinejoin = "round", // stroke line join of the line
    strokeWidth = 1.5, // stroke width of line, in pixels
    strokeOpacity = 1, // stroke opacity of line
  } = {}): void {
    // Compute values.
    const X: number[] = D3.map(data, x);
    const Y: number[] = D3.map(data, y);
    const I: number[] = D3.range(X.length);
    if (defined === undefined) defined = (d, i) => !isNaN(X[i]) && !isNaN(Y[i]);
    const D: number[] = D3.map(data, defined);

    // Compute default domains.
    if (xDomain === undefined) xDomain = D3.extent(X);
    if (yDomain === undefined) yDomain = [0, D3.max(Y)];

    // Construct scales and axes.
    const xScale = xType(xDomain, xRange);
    const yScale = yType(yDomain, yRange);
    const xAxis = D3.axisBottom(xScale).ticks(width / 80, yFormat);
    const yAxis = D3.axisLeft(yScale).ticks(height / 40, yFormat);

    // Construct a line generator.
    const line = (D3.line() as any)
      .defined(i => D[i])
      .curve(curve)
      .x(i => xScale(X[i]))
      .y(i => yScale(Y[i]));

    this.host.html("")
    this.svg = this.host.append("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [-width * 0.05, -height * 0.05, width * 1.08, height * 1.08])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;")

    this.svg.append("g")
      .style("font-size", xSize)
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(xAxis)
      .call(g => g.append("text")
        .attr("x", width - 35)
        .attr("y", 20)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .style("font-size", xSize)
        .text(xLabel));

    this.svg.append("g")
      .style("font-size", ySize)
      .attr("transform", `translate(${marginLeft},0)`)
      .call(yAxis)
      .call(g => g.select(".domain").remove())
      .call(g => g.selectAll(".tick line").clone()
        .attr("x2", width - marginLeft - marginRight)
        .attr("stroke-opacity", 0.1))
      .call(g => g.append("text")
        .attr("x", -marginLeft * 1.2)
        .attr("y", 0)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .style("font-size", ySize)
        .text(yLabel));

    this.svg.append("path")
      .attr("fill", "none")
      .attr("stroke", color)
      .attr("stroke-width", strokeWidth)
      .attr("stroke-linecap", strokeLinecap)
      .attr("stroke-linejoin", strokeLinejoin)
      .attr("stroke-opacity", strokeOpacity)
      .attr("d", line(I));

  }
}
