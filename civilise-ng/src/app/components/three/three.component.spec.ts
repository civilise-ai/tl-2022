import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { ThreeComponent } from './three.component';
import { LayerService, Layer } from 'src/app/services/layers.service';

describe('ThreeComponent', () => {
  let component: ThreeComponent;
  let fixture: ComponentFixture<ThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThreeComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [LayerService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it(`should have aspectRatio width/height`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.aspectRatio).toEqual(window.innerWidth / window.innerHeight);
  });

  it(`should have screenScale 1`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.screenScale).toEqual(1);
  });

  it(`should have landscapeHeightSize 1.1`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.landscapeHeightSize).toEqual(1.1);
  });

  it(`should have landscapeWidthSize 6.0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.landscapeWidthSize).toEqual(6.0);
  });

  it(`should have scene`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.scene).toBeTruthy();
  });

  it(`should have scene`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.scene).toBeTruthy();
  });

  it(`should have camera`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.camera).toBeTruthy();
  });

  it(`should have control`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.controls).toBeTruthy();
  });

  it(`should have renderer`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.renderer).toBeTruthy();
  });

  // Utility Variables
  it(`should have width initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.width).toEqual(0);
  });

  it(`should have length initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.length).toEqual(0);
  });

  it(`should have minimumHeight initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.minimumHeight).toEqual(0);
  });

  it(`should have maximumHeight initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.maximumHeight).toEqual(0);
  });

  it(`should have heightScalingFactor initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.heightScalingFactor).toEqual(0);
  }); 

  it(`should have widthScalingFactor initialised to 0`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.widthScalingFactor).toEqual(0);
  }); 

  // Mouseover, click detection stuff
  it(`should have mouse`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.mouse).toBeTruthy();
  }); 

  // Mouseover, click detection stuff
  it(`should have highlightedPaddock initialised to -1`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.highlightedPaddock).toEqual(-1);
  }); 

  it(`should have landscapeArrayCache initialised to undefined`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.landscapeArrayCache).toBeUndefined();
  }); 

  it(`should have baseImageCache initialsed to undefined`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    expect(component.baseImageCache).toBeUndefined();
  }); 

  it(`should have 5 undefined booleanMaps initialsed to undefined`, () => {
    fixture = TestBed.createComponent(ThreeComponent);
    component = fixture.componentInstance;
    // aerial, cadesture, trenches, gullies, ridges
    expect(component.booleanMaps).toEqual(new Array(5).fill(undefined));
  });

});
