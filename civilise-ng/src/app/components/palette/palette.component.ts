import { Component, ElementRef, AfterViewInit, ViewChild, Input, OnChanges, OnInit } from '@angular/core';
import { TextureData, TextureService } from '../../services/texture.service';

import * as D3 from "d3";
import { barycentric, ternaryPlot } from "d3-ternary"
@Component({
  selector: 'app-palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.scss']
})
export class PaletteComponent implements AfterViewInit, OnChanges {
  @ViewChild("containerPalette") element: ElementRef;

  private host: D3.Selection<d3.BaseType, {}, d3.BaseType, any>;
  private htmlElement: HTMLElement;

  private width: number;
  private height: number;

  @Input() textureData: TextureData

  perSilt: number;
  perSand: number;
  perClay: number;

  cx: number;
  cy: number;

  cr: number;
  cg: number;
  cb: number;

  rgb_r: number;
  rgb_g: number;
  rgb_b: number;

  private isEnlarged: boolean;
  enlarge: string = "Enlarge";

  constructor() { }

  ngOnChanges(): void {
    if (this.textureData != undefined) {
      // arrange soil texture values to 100
      this.perSilt = this.textureData.perSilt / 10;
      this.perSand = this.textureData.perSand / 10;
      this.perClay = this.textureData.perClay / 10;

      this.setup();
      this.build();
    }
  }

  ngAfterViewInit(): void {
    // Grab the DOM element we need for append SVG
    this.htmlElement = this.element.nativeElement;
    this.host = D3.select(this.htmlElement);

    if (this.textureData != undefined) {
      // arrange soil texture values to 100
      this.perSilt = this.textureData.perSilt / 10;
      this.perSand = this.textureData.perSand / 10;
      this.perClay = this.textureData.perClay / 10;

      this.setup();
      this.build();
    }
  }

  private setup(): void {
    // Enlarge and Shrink
    let base = window.innerWidth * 0.6;
    if (this.isEnlarged) {
      this.width = base;
      this.height = base / 16 * 9;
    } else {
      this.width = base / 1.5;
      this.height = base / 1.5 / 16 * 9;
    }
  }


  private build(): void {
    var textureTP = this._textureTernaryPlot(barycentric, ternaryPlot, this._radius(this.height));

    this.host.html("")

    // Draw Soil Texture Triangle
    const svg = this.host.append("svg").attr("width", this.width).attr("height", this.height)
    this.texturalTrianglePlot(
      svg,
      this.width,
      this.height,
      this._yOffset(this.height, textureTP),
      textureTP,
      this._gridLines(textureTP, this._line(D3)),
      this._ternaryDivisions(this._divisions(), D3, textureTP),
      this._closedLine(D3)
    );
  }

  // toggle the size
  toggle(): void {
    this.isEnlarged = !this.isEnlarged;
    this.enlarge = (this.isEnlarged) ? "Shrink" : "Enlarge";
    this.setup();
    this.build();
  }

  // Reference Link
  // https://observablehq.com/@julesblm/usda-soil-textural-triangle
  private texturalTrianglePlot(DOM, width, height, yOffset, textureTernaryPlot, gridLines, ternaryDivisions, closedLine) {
    // DOM.html("");
    const node = DOM.node();
    const svg = D3.select(node);
    const chart = svg
      .attr("id", "chart")
      .append("g")
      .attr("font-family", "sans-serif")
      .attr("transform", `translate(${width / 2} ${yOffset})`);

    const defs = svg.append("defs");

    defs
      .append("clipPath")
      .attr("id", "trianglePath")
      .append("path")
      .attr("d", textureTernaryPlot.triangle());

    defs
      .append("marker")
      .attr("id", "arrow")
      .attr("markerWidth", "10")
      .attr("markerHeight", "10")
      .attr("refX", "0")
      .attr("refY", "3")
      .attr("orient", "auto")
      .attr("markerUnits", "strokeWidth")
      .append("path")
      .attr("d", "M0,0 L0,6 L9,3 z");

    (chart as any)
      .append("g")
      .attr("class", "grid")
      .selectAll("g")
      .data(gridLines)
      .join("path")
      .attr("d", (d) => d)
      .attr("stroke", "black")
      .attr("stroke-width", 0.5);

    const tickGroup = (chart as any)
      .append("g")
      .attr("class", "ternary-ticks")
      .attr("font-size", 10)
      .selectAll("g")
      .data(textureTernaryPlot.ticks())
      .join("g")
      .attr("class", "axis-ticks")
      .selectAll("g")
      .data((d) => d)
      .join("g")
      .attr("class", "tick")
      .attr("transform", (d) => `translate(${d.position})`)
      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", (d) => d.textAnchor)
          .attr("transform", (d) => `rotate(${d.angle})`)
          .attr("dx", (d) => (-d.size - 5) * (d.textAnchor === "start" ? -1 : 1))
          .attr("dy", ".5em")
          .text((d) => d.tick)
      )
      .call((g) =>
        g
          .append("line")
          .attr("transform", (d) => `rotate(${d.angle + 90})`)
          .attr("y2", (d) => d.size * (d.textAnchor === "start" ? -1 : 1))
          .attr("stroke", "grey")
      );

    const axisLabelsGroup = chart
      .append("g")
      .attr("class", "axis-labels")
      .append("g")
      .attr("class", "labels")
      .attr("font-size", 16);

    const axisLabelsGroups = (axisLabelsGroup as any)
      .selectAll("text")
      .data(textureTernaryPlot.axisLabels({ center: true }), (d) => d.label)
      .join(
        (enter) => {
          const labelGroup = enter
            .append("g")
            .attr(
              "transform",
              (d, i) => `translate(${d.position})rotate(${d.angle})`
            );

          const axisArrow = labelGroup
            .append("line")
            .attr("stroke", "black")
            .attr("x1", (d, i) => (i === 2 ? 100 : -100))
            .attr("x2", (d, i) => (i === 2 ? -100 : 100))
            .attr("marker-end", "url(#arrow)");

          const axisLabelText = labelGroup
            .append("text")
            .attr("text-anchor", "middle")
            .attr("stroke", "ghostwhite")
            .attr("stroke-width", 7)
            .attr("paint-order", "stroke")
            .attr("alignment-baseline", "middle")
            .text((d) => d.label);
        },
        (update) =>
          update.attr(
            "transform",
            (d, i) => `translate(${d.position})rotate(${d.angle})`
          )
      );

    chart
      .append("path")
      .attr("d", textureTernaryPlot.triangle())
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .attr("stroke", "black")
      .attr("stroke-width", "1.2px");
    // .call(
    //   D3
    //     .drag()
    //     .on("drag", ({ x, y }) => dragged(x, y))
    //     .on("start", ({ x, y }) => dragged(x, y))
    // );

    chart
      .append("g")
      .attr("class", "data")
      .attr("clip-path", "url(#trianglePath)")
      .selectAll("path")
      .data(ternaryDivisions.map((d) => d.coords))
      .join("path")
      .attr("d", closedLine)
      .attr("fill", "none")
      .attr("stroke", "black")
      .attr("stroke-width", 2);

    (chart as any)
      .append("g")
      .selectAll("text")
      .data(ternaryDivisions)
      .join("text")
      .attr("x", (d) => d.centroid[0])
      .attr("y", (d) => d.centroid[1])
      .attr("text-anchor", "middle")
      .attr("stroke", "ghostwhite")
      .attr("stroke-width", 5)
      .attr("paint-order", "stroke")
      .attr("alignment-baseline", "middle")
      .attr("font-size", 11)
      .text((d) => d.texture);


    var coords = [{ clay: this.perClay, sand: this.perSand, silt: this.perSilt }].map(this._textureTernaryPlot(barycentric, ternaryPlot, this._radius(this.height)))
    this.cx = coords[0][0]
    this.cy = coords[0][1]

    // Draw RGB dot base on soil texture values
    this.rgb_r = Math.round(this.perSand / 100 * 255)
    this.rgb_g = Math.round(this.perSilt / 100 * 255)
    this.rgb_b = Math.round(this.perClay / 100 * 255)

    const handle = (chart as any)
      .append("circle")
      .attr("id", "drag-handle")
      .attr("fill", "rgba(" + this.rgb_r + ", " + this.rgb_g + ", " + this.rgb_b + ", 0.8)")
      .attr("cx", coords[0][0])
      .attr("cy", coords[0][1])
      .attr("r", 10);

    handle.append("title").text((d) => d);

    // make the dot draggable
    function dragged(x, y) {
      handle.attr("cx", x).attr("cy", y);

      node.dispatchEvent(new CustomEvent("input"), { bubbles: true });
      node.value = textureTernaryPlot.invert([x, y]);
    }

    node.value = textureTernaryPlot.invert([0, 0]);
  }

  private _textureTernaryPlot(barycentric, ternaryPlot, radius) {
    const ternarySoil = barycentric()
      .a((d) => d.silt)
      .b((d) => d.clay)
      .c((d) => d.sand);

    return ternaryPlot(ternarySoil)
      .radius(radius)
      .domains([
        [1, 0],
        [1, 0],
        [1, 0]
      ])
      .labels(["Silt Separate, %", "Clay Separate, %", "Sand Separate, %"])
      .labelAngles([60, -60, 0])
      .labelOffsets([100, 100, 100])
      .tickAngles([-60, 0, 60])
      .tickTextAnchors(["start", "end", "start"]);
  }

  private _cleanLabels(textureTernaryPlot) {
    return (
      textureTernaryPlot.labels().map(label => label.split(", ")[0])
    )
  }

  private _gridLines(textureTernaryPlot, line) {
    return (
      textureTernaryPlot
        .gridLines()
        .map(axisGrid => axisGrid.map(line).join(" "))
    )
  }

  private _format(d3) {
    return (
      d3.format(".2%")
    )
  }

  private _divisions() {
    return (
      [
        {
          texture: "Silt",
          values: [
            { clay: 0, sand: 20, silt: 80 },
            { clay: 12, sand: 8, silt: 80 },
            { clay: 12, sand: 0, silt: 88 },
            { clay: 0, sand: 0, silt: 100 },
            { clay: 0, sand: 20, silt: 80 },
          ],
        },

        {
          texture: "Silt loam",
          values: [
            { clay: 0, sand: 20, silt: 80 },
            { clay: 0, sand: 50, silt: 50 },
            { clay: 27, sand: 23, silt: 50 },
            { clay: 27, sand: 0, silt: 73 },
            { clay: 12, sand: 0, silt: 88 },
            { clay: 12, sand: 8, silt: 80 },
            { clay: 0, sand: 20, silt: 80 },
          ],
        },
        {
          texture: "Loam",
          values: [
            { clay: 7, sand: 43, silt: 50 },
            { clay: 7, sand: 52, silt: 41 },
            { clay: 20, sand: 52, silt: 28 },
            { clay: 27, sand: 45, silt: 28 },
            { clay: 27, sand: 23, silt: 50 },
            { clay: 7, sand: 43, silt: 50 },
          ],
        },
        {
          texture: "Sand",
          values: [
            { clay: 0, sand: 100, silt: 0 },
            { clay: 10, sand: 90, silt: 0 },
            { clay: 0, sand: 85, silt: 15 },
            { clay: 0, sand: 100, silt: 0 },
          ],
        },
        {
          texture: "Loamy sand",
          values: [
            { clay: 10, sand: 90, silt: 0 },
            { clay: 15, sand: 85, silt: 0 },
            { clay: 0, sand: 70, silt: 30 },
            { clay: 0, sand: 85, silt: 15 },
            { clay: 10, sand: 90, silt: 0 },
          ],
        },
        {
          texture: "Sandy loam",
          values: [
            { clay: 20, sand: 80, silt: 0 },
            { clay: 20, sand: 52, silt: 28 },
            { clay: 7, sand: 52, silt: 41 },
            { clay: 7, sand: 43, silt: 50 },
            { clay: 0, sand: 50, silt: 50 },
            { clay: 0, sand: 70, silt: 30 },
            { clay: 15, sand: 85, silt: 0 },
            { clay: 20, sand: 80, silt: 0 },
          ],
        },
        {
          texture: "Clay loam",
          values: [
            { clay: 40, sand: 45, silt: 15 },
            { clay: 40, sand: 20, silt: 40 },
            { clay: 27, sand: 20, silt: 53 },
            { clay: 27, sand: 45, silt: 28 },
            { clay: 40, sand: 45, silt: 15 }
          ]
        },
        {
          texture: "Sandy clay loam",
          values: [
            { clay: 35, sand: 65, silt: 0 },
            { clay: 35, sand: 45, silt: 20 },
            { clay: 27, sand: 45, silt: 28 },
            { clay: 20, sand: 52, silt: 28 },
            { clay: 20, sand: 80, silt: 0 },
            { clay: 35, sand: 65, silt: 0 },
          ],
        },
        {
          texture: "Sandy clay",
          values: [
            { clay: 55, sand: 45, silt: 0 },
            { clay: 35, sand: 45, silt: 20 },
            { clay: 35, sand: 65, silt: 0 },
            { clay: 55, sand: 45, silt: 0 },
          ],
        },
        {
          texture: "Clay",
          values: [
            { clay: 100, sand: 0, silt: 0 },
            { clay: 55, sand: 45, silt: 0 },
            { clay: 40, sand: 45, silt: 15 },
            { clay: 40, sand: 20, silt: 40 },
            { clay: 60, sand: 0, silt: 40 },
            { clay: 100, sand: 0, silt: 0 },
          ],
        },
        {
          texture: "Silty clay",
          values: [
            { clay: 60, sand: 0, silt: 40 },
            { clay: 40, sand: 20, silt: 40 },
            { clay: 40, sand: 0, silt: 60 },
            { clay: 60, sand: 0, silt: 40 },
          ],
        },
        {
          texture: "Silty clay loam",

          values: [
            { clay: 40, sand: 0, silt: 60 },
            { clay: 40, sand: 20, silt: 40 },
            { clay: 27, sand: 20, silt: 53 },
            { clay: 27, sand: 0, silt: 73 },
            { clay: 40, sand: 0, silt: 60 },
          ],
        },
      ]
    )
  }

  private _ternaryDivisions(divisions, d3, textureTernaryPlot) {
    return (
      divisions.map(d => {
        // add centroid for label position to each division
        const centroid = d3.polygonCentroid(d.values.map(textureTernaryPlot));
        const coords = d.values.map(textureTernaryPlot);

        return { ...d, coords, centroid };
      })
    )
  }

  private _line(d3) {
    return (
      d3.line()
    )
  }

  private _closedLine(d3) {
    return (
      d3.line().curve(d3.curveLinearClosed)
    )
  }

  private _d3(require) {
    return (
      require('d3@v6')
    )
  }

  private _height() {
    return (
      500
    )
  }

  private _radius(height) {
    return (
      height / 2
    )
  }

  private _yOffset(height, textureTernaryPlot) {
    return (
      height / 2 + textureTernaryPlot.radius() / 4
    )
  }

  // ========================================================
  private _drawLinearGradient(ramp, lerpFrames) {
    const gammaConstant = 2.2;
    // To linear 0..1 sRGB
    const toLinear = n => Math.pow(n / 255, 1 / gammaConstant);
    // To gamma 0..255 RGB
    const toGamma = n => Math.max(0, Math.min(255, Math.round(Math.pow(n, gammaConstant) * 255)));

    return function drawLinearGradient(colors, linear = false) {
      const inputs = linear ? colors.map(rgb => rgb.map(toLinear)) : colors;
      return ramp(u => {
        const output = lerpFrames(inputs, u)
        return linear ? output.map(toGamma) : output;
      });
    };
  }

  private _ramp(createContext) {
    return (
      (interpolator) => {
        const { context, width, height } = createContext(this.width, this.height);
        // Faster but doesn't allow per-pixel jittering
        // const imgCanvas = document.createElement('canvas');
        // imgCanvas.width = samples.length;
        // imgCanvas.height = 4;
        // const imgCtx = imgCanvas.getContext('2d');
        // for (let x = 0; x < samples.length; x++) {
        //   let Lab = samples[x];
        //   let [ R, G, B ] = LabConvert.lab2rgb(Lab);
        //   imgCtx.fillStyle = `rgb(${R}, ${G}, ${B})`;
        //   imgCtx.fillRect(x, 0, 1, imgCanvas.height);
        // }
        // context.drawImage(imgCanvas, 0, 0, width, height);
        const img = context.createImageData(context.canvas.width, context.canvas.height);

        for (let x = 0; x < img.width; x++) {
          for (let y = 0; y < img.height; y++) {
            const u = x / (img.width - 1);
            const [R, G, B] = interpolator(u, x, img.width);
            const i = x + y * img.width;
            if ((this.cx - 10) <= x && x <= (this.cx + 10) && (this.cy - 10) <= y && y <= (this.cy + 10)) {
              img.data[i * 4 + 0] = 100;
              img.data[i * 4 + 1] = 149;
              img.data[i * 4 + 2] = 237;
              img.data[i * 4 + 3] = 255;
              this.cr = R;
              this.cg = G;
              this.cb = B;
            } else {
              img.data[i * 4 + 0] = R;
              img.data[i * 4 + 1] = G;
              img.data[i * 4 + 2] = B;
              img.data[i * 4 + 3] = 255;
            }
          }
        }
        context.putImageData(img, 0, 0);
        return context.canvas;
      }
    )
  }


  private _lerpFrames(lerp) {
    function lerpArray(min, max, t, out) {
      out = out || [];
      if (min.length !== max.length) {
        throw new TypeError('min and max array are expected to have the same length');
      }
      for (var i = 0; i < min.length; i++) {
        out[i] = lerp(min[i], max[i], t);
      }
      return out;
    }

    return function lerpFrames(values, t, out) {
      var len = values.length - 1;
      var whole = t * len;
      var frame = Math.floor(whole);
      var fract = whole - frame;

      var nextFrame = Math.min(frame + 1, len);
      var a = values[frame % values.length];
      var b = values[nextFrame % values.length];
      if (typeof a === 'number' && typeof b === 'number') {
        return lerp(a, b, fract);
      } else if (Array.isArray(a) && Array.isArray(b)) {
        return lerpArray(a, b, fract, out);
      } else {
        throw new TypeError('Mismatch in value type of two array elements: ' + frame + ' and ' + nextFrame);
      }
    }
  }

  private _lerp() {
    return (
      function lerp(min, max, t) {
        return min * (1 - t) + max * t;
      }
    )
  }

  private _createContext(width, DOM) {
    return (
      (w = this.width, h = this.height) => {
        if (width < w) {
          let ratio = w / h;
          w = width;
          h = w / ratio;
        }
        const context = DOM.getContext("2d");
        return { context, width: w, height: h };
      }
    )
  }
}
