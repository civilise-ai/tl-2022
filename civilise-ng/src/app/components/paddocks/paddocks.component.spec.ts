import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { PaddocksComponent } from './paddocks.component';
import { LayerService } from '../../services/layers.service';


describe('PaddocksComponent', () => {
  let component: PaddocksComponent;
  let fixture: ComponentFixture<PaddocksComponent>;

  beforeEach(async() => {
    await TestBed.configureTestingModule({
      declarations: [PaddocksComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [LayerService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have transformedPaddocks initialised to []`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.transformedPaddocks).toEqual([]);
  });

  it(`should have displayedColumns initialised to ['_id', 'area', 'perimeter']`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.displayedColumns).toEqual(['_id', 'area', 'perimeter']);
  });

  it(`should have metricArea initialised to true`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.metricArea).toBeTrue();
  });

  it(`should have dataSource initialised`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.dataSource).toBeTruthy();
  });

  it(`test paddocks.getAreaAcres()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getAreaAcres(10)).toEqual(0.25);
  });

  it(`test paddocks.getAreaHectares()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getAreaHectares(10)).toEqual(0.1);
  });

  it(`test paddocks.getPerimeter()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getPerimeter(10)).toEqual(100);
  });

  it(`test paddocks.getGrassCost()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getGrassCost(10, 10)).toEqual(1);
  });

  it(`test paddocks.getTreeCost()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getTreeCost(10, 10)).toEqual(1);
  });

  it(`test paddocks.getFencingCost()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getFencingCost(10, 10)).toEqual(1000);
  });

  it(`test paddocks.getTreeFencingCost()`, () => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    expect(component.getTreeFencingCost(10, 10)).toEqual(1000);
  });

});
