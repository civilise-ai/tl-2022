## Setup Instructions

#

### Cloning the repo

- To clone the repo:  
   `git clone https://gitlab.com/civilise-ai/tl-2022.git`

#

### Production deployment
1. Ask John for login credentials for civilise.ai production machine
2. Navigate to the tl-2022 repo
   - `cd r/tl-2022`
3. Launch everything
   - `./deploy.prod.sh up`
#

### (Optional) Local environment

1. You can set up a local environment, but the drawback is the lack of the database. 
Currently, the database contains more than hundreds gigabyte of data,
we strongly suggest to use the production machine
where data is located at
   - `r/db` for DEM data
   - `r/soildb` for Soil data

### Architecture
- 0.0.0.0:4200->4200/tcp, :::4200->4200/tcp   tl-2022-prod-frontend-1
- 0.0.0.0:5000->8081/tcp, :::5000->8081/tcp   tl-2022-prod-gateway-1
- 0.0.0.0:5555->5555/tcp, :::5555->5555/tcp   tl-2022-prod-dembackend-1
- 0.0.0.0:5566->5566/tcp, :::5566->5566/tcp   tl-2022-prod-soilbackend-1
- 0.0.0.0:5434->5432/tcp, :::5434->5432/tcp   tl-2022-prod-soildatabase-1
- 0.0.0.0:5433->5432/tcp, :::5433->5432/tcp   tl-2022-prod-demdatabase-1


### Api Gateway
- "/api" CONTAINER_BE_DEM
- "/api/export" CONTAINER_BE_DEM
- "/soil/textureMap" CONTAINER_BE_SOIL
- "/soil/texture" CONTAINER_BE_SOIL
- "/soil/analysis" CONTAINER_BE_SOIL
- '*' CONTAINER_FE
