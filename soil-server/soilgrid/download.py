import matplotlib.pyplot as plt
from soilgrids import SoilGrids

# get data from SoilGrids
soil_grids = SoilGrids()
# soil_grids.get_coverage_info(service_id='sand', coverage_id='sand_0-5cm_mean')

# data = soil_grids.get_coverage_data(service_id='sand', coverage_id='sand_0-5cm_mean',
#                                     west=113.33, south=-43.63, east=153.569, north=-10.668,
#                                     crs='urn:ogc:def:crs:EPSG::4326', output='test.tif', width=16000, height=12000)

# Australia boundaries
West = 113.33
South = -43.63
East = 153.569
North = -10.668

WEDif = East - West
SNDif = North - South
index = 0

# Download data as 100 tif files
for i in range(10):
    curEast = West + (i + 1) * (WEDif / 10)
    curWest = West + i * (WEDif / 10)
    print(curWest)
    print(curEast)
    # clay, slit, sand
    for j in range(10):
        curSouth = North - (j + 1) * (SNDif / 10)
        curNorth = North - j * (SNDif / 10)
        print(curSouth)  # Debug
        print(curNorth)

        """
        service_id: sand, silt, clay
        coverage_id: https://www.isric.org/explore/soilgrids/faq-soilgrids
        EPSG4326 means GPS system, we need define width&height variables if use GPS system.
        width&height is pixel size of the tif file.
        """
        data = soil_grids.get_coverage_data(service_id='silt', coverage_id='silt_0-5cm_mean',
                                            west=curWest, south=curSouth, east=curEast, north=curNorth,
                                            crs='urn:ogc:def:crs:EPSG::4326',
                                            output='silt/silt' + str(index) + '.tif', width=4000,
                                            height=3000)
        index += 1

# show metadata
for key, value in soil_grids.metadata.items():
    print('{}: {}'.format(key, value))

# plot data
data.plot(figsize=(9, 5))
plt.title('Mean pH between 0 and 5 cm soil depth in Senegal')
