import unittest

from soilgrid import extract_data


class TestExtractData(unittest.TestCase):
    def test_error_case1(self):
        expected_output = -1
        output = extract_data.getFile(-1, -1)
        self.assertEqual(output, expected_output)

        output = extract_data.getFile(120, -1)
        self.assertEqual(output, expected_output)

    def test_error_case2(self):
        expected_output = -1
        output = extract_data.getData(-1, -1, "a")
        self.assertEqual(output, expected_output)

    def test_getFile(self):
        expected_output = 0
        output = extract_data.getFile(114.51, -12.455)
        self.assertEqual(output, expected_output)

        expected_output = 76
        output = extract_data.getFile(144.51, -32.455)
        self.assertEqual(output, expected_output)

    def test_getResult(self):
        expected_output = 1172, 1626
        output = extract_data.getResult(114.51, -12.455)
        self.assertEqual(output, expected_output)

        expected_output = 2914, 11
        output = extract_data.getResult(124.31, -30.458)
        self.assertEqual(output, expected_output)

