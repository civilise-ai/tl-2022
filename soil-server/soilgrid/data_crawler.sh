# remove and create new data folder
rm -rf ./data
mkdir data
cd data

# curl README.md
curl -O https://files.isric.org/soilgrids/latest/data/README.md

property=(bdod cec cfvo clay landmask nitrogen ocd ocs phh2o sand silt soc wrb)

# curl bdod
mkdir bdod
cd bdod
suffix=(dbf prj qix shp shx) #bdod clay soc
curl -O curl -O https://files.isric.org/soilgrids/latest/data/bdod/bdod_index.dbf

depth=(0-5cm 5-15cm 15-30cm 30-60cm 60-100cm 100-200cm)
stat=(mean Q0.05 Q0.5 Q0.95 uncertainty)

curl -O https://files.isric.org/soilgrids/latest/data/bdod/bdod_0-5cm_mean.vrt
curl -O https://files.isric.org/soilgrids/latest/data/bdod/bdod_0-5cm_mean.vrt.ovr

# extract data file
# e.g. bdod_0-5cm_mean/tileSG-005-062/tileSG-005-062_3-3.tif
grep "tile" bdod_0-5cm_mean.vrt | cut -d '/' -f 2,3 > bdod_0-5cm_mean_dirs
grep "tile" bdod_0-5cm_mean.vrt | cut -d '/' -f 2,3,4 | sed 's/.$//' > bdod_0-5cm_mean_data

# curl data file
mkdir bdod_0-5cm_mean/tileSG-005-062
cd bdod_0-5cm_mean/tileSG-005-062
curl -O https://files.isric.org/soilgrids/latest/data/bdod/bdod_0-5cm_mean/tileSG-005-062/tileSG-005-062-3-3.tif
