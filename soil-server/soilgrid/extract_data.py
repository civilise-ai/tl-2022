import pandas as pd

"""
Get single csv entry by GPS location and soil type
"""


def getData(longtitude, latitude, type):
    if type == "clay":
        if getFile(longtitude, latitude) != -1:
            filename = "clay/clay" + getFile(longtitude, latitude)
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    elif type == "slit":
        if getFile(longtitude, latitude) != -1:
            filename = "slit/slit" + getFile(longtitude, latitude)
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    elif type == "sand":
        if getFile(longtitude, latitude) != -1:
            filename = "sand/sand" + getFile(longtitude, latitude)
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    else:
        print("Wrong type, try again.")
        return -1


"""
Return -1 if we get error, else return index number of the csv file we should look at
"""


def getFile(longtitude, latitude):
    if 113.33 < longtitude < 153.569:
        row = (longtitude - 113.33) // 4.0239
    else:
        return -1

    if -43.63 < latitude < -10.668:
        col = (latitude * (-1) - 10.668) // 3.2962
    else:
        return -1

    if row == 0:
        return int(col)
    else:
        return int(row) * 10 + int(col)


"""
Return row and col for specific csv entry 
Input GPS location
"""


# get csv entry by row and col
def getResult(longtitude, latitude):
    row = ((longtitude - 113.33) % 4.0239) // 0.001005975
    col = ((latitude * (-1) - 10.668) % 3.2962) // 0.00109873
    return int(row), int(col)


"""
Get csv entry by filename, row number and column number
"""


def readData(filename, row, col):
    df = pd.read_csv(filename)
    return df.iloc[row, col]
