# Data Collection

This folder contains some file to help us download and manage data from [SoilGrids](https://www.isric.org/explore/soilgrids).
After [download](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soilgrid/download.py) high resolution data that present as 100 tif files, 
we can [transfer](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soilgrid/transfer.py) file to csv format. You can check this
[example](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soilgrid/sample%20data.png) that represents sand data for whole Australia.
We can [extract data](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soilgrid/extract_data.py) by GPS location and soil type, 
and we conducted [unit testing](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soilgrid/test_extract_data.py) and test on our local machine for this program.
