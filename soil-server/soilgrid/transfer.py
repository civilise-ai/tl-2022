import pandas as pd
from osgeo import gdal

"""
Transfer tif to array
"""
def raster2array(rasterfn):
    raster = gdal.Open(rasterfn)
    band = raster.GetRasterBand(1)
    return band.ReadAsArray()


# name of tif
df = pd.DataFrame(raster2array('test.tif'))
# name of csv
df.to_csv('out.csv')
