import time

import pandas as pd
import psycopg2
from psycopg2 import extras as ex
from multiprocessing import Pool
from os import environ

#Delete if it already contains a data table
def drop_table(file_num):
    db = psycopg2.connect(host=environ.get('DB_HOST'), port='5432', user='postgres', database='soil', password='me')
    db.autocommit = True
    cursor = db.cursor()
    cursor.execute(f"""DROP TABLE IF EXISTS texture_{file_num % 10 * 2}_{file_num // 10};""")
    cursor.execute(f"""DROP TABLE IF EXISTS texture_{file_num % 10 * 2 + 1}_{file_num // 10};""")

    cursor.close()

#Create data tables and split them to improve query efficiency
def create_table(file_num):
    db = psycopg2.connect(host=environ.get('DB_HOST'), port='5432', user='postgres', database='soil', password='me')
    db.autocommit = True
    cursor = db.cursor()
    for i in [0, 1]:
        cursor.execute(f"""
            CREATE TABLE IF NOT EXiSTS texture_{file_num % 10 * 2 + i}_{file_num // 10} (
                row       integer NOT NULL,
                col       integer NOT NULL,
                perSilt   integer,
                perSand   integer,
                perClay   integer, -- double precision,
                fileNum   integer,
                PRIMARY KEY (row, col)
            );
        """)
    cursor.close()

#Insert all the data in csv into the database
def insert_data(file_num):
    db = psycopg2.connect(host=environ.get('DB_HOST'), port='5432', user='postgres', database='soil', password='me')
    db.autocommit = True
    cursor = db.cursor()
    per_silt_df = pd.read_csv("/app/csvFiles/silt_csv/silt" + str(file_num) + ".csv", index_col=0, header=0)
    print(per_silt_df.shape)
    per_sand_df = pd.read_csv("/app/csvFiles/sand_csv/sand" + str(file_num) + ".csv", index_col=0, header=0)
    print(per_sand_df.shape)
    per_clay_df = pd.read_csv("/app/csvFiles/clay_csv/clay" + str(file_num) + ".csv", index_col=0, header=0)
    print(per_clay_df.shape)

    for j in range(1500):
        datalist = []
        # start_time = time.time()
        for k in range(4000):
            datalist.append(
                (j, k, int(per_silt_df.iloc[j, k]), int(per_sand_df.iloc[j, k]), int(per_clay_df.iloc[j, k]), file_num))
            # sql = 'INSERT INTO "modelData" ("row", "col", "type", "number", "value") VALUES (%s,%s,\'%s\',%s,%s)' % (j, k, "clay", 0, df.iloc[j,k])
            # cursor.execute(sql)
        sql = f"""INSERT INTO texture_{file_num % 10 * 2}_{file_num // 10} ("row", "col", "persilt", "persand", "perclay", "filenum") VALUES %s;"""
        ex.execute_values(cursor, sql, datalist, page_size=4000)
        # end_time = time.time()
        # print(file_num, j, end_time - start_time)

    for j in range(1500, 3000):
        datalist = []
        # start_time = time.time()
        for k in range(4000):
            datalist.append(
                (j-1500, k, int(per_silt_df.iloc[j, k]), int(per_sand_df.iloc[j, k]), int(per_clay_df.iloc[j, k]), file_num))
            # sql = 'INSERT INTO "modelData" ("row", "col", "type", "number", "value") VALUES (%s,%s,\'%s\',%s,%s)' % (j, k, "clay", 0, df.iloc[j,k])
            # cursor.execute(sql)
        sql = f"""INSERT INTO texture_{file_num % 10 * 2 + 1}_{file_num // 10} ("row", "col", "persilt", "persand", "perclay", "filenum") VALUES %s;"""
        ex.execute_values(cursor, sql, datalist, page_size=4000)
        # end_time = time.time()
        # print(file_num, j, end_time - start_time)

    cursor.close()

if __name__ == '__main__':
    nums = list(range(0, 100))
    with Pool(4) as p:
        p.map(drop_table, nums)
        p.map(create_table, nums)
        p.map(insert_data, nums)

