from equations import get_model_data
import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(('localhost', 5010)) # layers.services.ts line 33
serversocket.listen(5) # become a server socket, maximum 5 connections
while True:
    connection, address = serversocket.accept()
    buf = connection.recv(64)
    if len(buf) > 0:
        print(buf)
        l = buf.decode('UTF-8').split()
        l = l[1].split("=")
        cor = l[1].split(',')
        cor = [float(i) for i in cor] # longtitude, latitude
        dat = get_model_data(cor[1], cor[0])
        print(dat)
        break


