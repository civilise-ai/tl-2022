import pandas as pd
import time

import pandas as pd
import psycopg2
from psycopg2 import extras as ex
from multiprocessing import Pool

from os import environ


"""
Get single database entry by GPS location and soil type
"""
def queryData(longtitude, latitude):
    db = psycopg2.connect(host=environ.get('DB_HOST'), port='5432', user='postgres', database='soil', password='me')
    db.autocommit = True
    cursor = db.cursor()

    if -43.63 < latitude < -10.668:
        row = int((latitude * (-1) - 10.668) // (3.2962/2))

    if 113.33 < longtitude < 153.569:
        col = int((longtitude - 113.33) // 4.0239)

    sql = f"""SELECT persand, persilt, perclay FROM texture_{row}_{col} WHERE row = %s and col = %s"""
    query_row = ((latitude * (-1) - 10.668) % (3.2962/2)) // 0.00109873
    query_col = ((longtitude - 113.33) % 4.0239) // 0.001005975

    cursor.execute(sql, (query_row, query_col))
    (persand, persilt, perclay) = cursor.fetchone()
    cursor.close()
    return persand, persilt, perclay

"""
Get multiple database entry by GPS location and soil type
"""
def queryMultiData(longtitude, latitude, block_rows, block_cols):
    db = psycopg2.connect(host=environ.get('DB_HOST'), port='5432', user='postgres', database='soil', password='me')
    db.autocommit = True
    cursor = db.cursor()

    # e.g. 27*15 [-13, 13], [-7, 7]
    # e.g. 28*16 [-14, 13], [-8, 7]
    max_row = (block_rows - 1) // 2
    min_row = (-1) * (block_rows // 2)
    max_col = (block_cols - 1) // 2
    min_col = (-1) * (block_cols // 2)


    new_min_latitude = latitude + 0.00109873 * min_row
    new_max_latitude = latitude + 0.00109873 * max_row
    new_min_longtitude = longtitude + 0.001005975 * min_col
    new_max_longtitude = longtitude + 0.001005975 * max_col


    # print(new_min_latitude, new_max_latitude, new_min_longtitude, new_max_longtitude)

    sql = generateSql(new_min_longtitude, new_max_longtitude, new_min_latitude, new_max_latitude)
    cursor.execute(sql, ())

    # print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

    outputs = cursor.fetchall()

    tuple2d = []
    for i in range(block_rows):
        tuple2d.append([0] * block_cols)


    for i, output in enumerate(outputs):
        tuple2d[i // block_cols][i % block_cols] = output
    cursor.close()

    return tuple2d

# Concatenation query SQL
# Different cases for multi-database query
def generateSql(min_longtitude, max_longtitude, min_latitude, max_latitude):
    # determine 
    if 113.33 < min_longtitude < 153.569:
        min_col = int((min_longtitude - 113.33) // 4.0239)

    if 113.33 < max_longtitude < 153.569:
        max_col = int((max_longtitude - 113.33) // 4.0239)

    if -43.63 < min_latitude < -10.668:
        min_row = int((min_latitude * (-1) - 10.668) // (3.2962/2))

    if -43.63 < min_latitude < -10.668:
        max_row = int((max_latitude * (-1) - 10.668) // (3.2962/2))

    # print(min_row, max_row, min_col, max_col)

    # *(-1) will make min to max, and max to min
    query_max_row = ((min_latitude * (-1) - 10.668) % (3.2962 / 2)) // 0.00109873
    query_min_row = ((max_latitude * (-1) - 10.668) % (3.2962 / 2)) // 0.00109873

    query_min_col = ((min_longtitude - 113.33) % 4.0239) // 0.001005975
    query_max_col = ((max_longtitude - 113.33) % 4.0239) // 0.001005975

    # print(query_min_row, query_max_row, query_min_col, query_max_col)
    """
    -------------------------
    |table 1    |table 2    |
    |           |           |
    |           |           |
    |-----------------------|
    |           |           |
    |           |           |    
    | table 3   |table 4    |
    |-----------------------|

    Case 1:
    -------------------------
    |table 1    |table 2    |
    |   +++++   |           |
    |   +   +   |           |
    |   +++++   |           |
    |-----------------------|
    |           |           |
    |           |           |    
    | table 3   |table 4    |
    |-----------------------|

    Case 2:
    -------------------------
    |table 1    |table 2    |
    |           |           |
    |           |           |
    |  +++++    |           |
    |- +   + ---------------|
    |  +++++    |           |
    |           |           |    
    | table 3   |table 4    |
    |-----------------------|

    Case 3:
    -------------------------
    |table 1    |table 2    |
    |         +++++         |
    |         +   +         |
    |         +++++         |
    |-----------------------|
    |           |           |
    |           |           |    
    | table 3   |table 4    |
    |-----------------------|

    Case 4:
    -------------------------
    |table 1    |table 2    |
    |           |           |
    |           |           |
    |         +++++         |
    |---------+   +---------|
    |         +++++         |
    |           |           |    
    | table 3   |table 4    |
    |-----------------------|

    """

    # Case 1: only need to query data from one table
    if min_row == max_row and min_col == max_col:
        sql = f"""SELECT row, col, perSand, perSilt, perClay from texture_{min_row}_{min_col}
        where row >= {query_min_row} and row <= {query_max_row} and col >= {query_min_col} and col <= {query_max_col}
        order by row, col ASC; """

    # Case 2: need to query data from two tables
    elif min_row != max_row and min_col == max_col:
        sql = f"""
        SELECT row, col, perSand, perSilt, perClay from 
        (SELECT row, col, perSand, perSilt, perClay from texture_{min_row}_{min_col}
                where row >= {query_min_row} and col >= {query_min_col} and col <= {query_max_col}
        UNION
        SELECT row+1500, col, perSand, perSilt, perClay from texture_{max_row}_{min_col}
                where row <={query_max_row} and col >= {query_min_col} and col <= {query_max_col}) joint
        order by row, col ASC;
        """
    # Case 3: need to query data from two tables
    elif min_row == max_row and min_col != max_col:
        sql = f"""
        SELECT row, col, perSand, perSilt, perClay from 
        (SELECT row, col, perSand, perSilt, perClay from texture_{min_row}_{min_col}
                where row >= {query_min_row} and row <= {query_max_row} and col >= {query_min_col}
        UNION
        SELECT row, col+4000, perSand, perSilt, perClay from texture_{min_row}_{max_col}
                where row >= {query_min_row} and row <= {query_max_row} and col <= {query_max_col}) joint
        order by row, col ASC;
        """
    # Case 4: need to query data from four tables
    else:
        sql = f"""
        SELECT row, col, perSand, perSilt, perClay from 
        (SELECT row, col, perSand, perSilt, perClay from texture_{min_row}_{min_col}
                where row >= {query_min_row} and col >= {query_min_col}
        UNION
        SELECT row, col+4000, perSand, perSilt, perClay from texture_{min_row}_{max_col}
                where row >= {query_min_row} and col <= {query_max_col}
        UNION
        SELECT row+1500, col, perSand, perSilt, perClay from texture_{max_row}_{min_col}
                where row <= {query_max_row} and col >= {query_min_col}
        UNION
        SELECT row+1500, col+4000, perSand, perSilt, perClay from texture_{max_row}_{max_col}
                where row <= {query_max_row} and col <= {query_max_col}) joint
        order by row, col ASC;
        """
    return sql



def getData(longtitude, latitude, type):
    if type == "clay":
        if getFile(longtitude, latitude) != -1:
            filename = "/data/8715/clay_csv/clay" + str(getFile(longtitude, latitude)) + ".csv"
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    elif type == "silt":
        if getFile(longtitude, latitude) != -1:
            filename = "/data/8715/silt_csv/silt" + str(getFile(longtitude, latitude)) + ".csv"
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    elif type == "sand":
        if getFile(longtitude, latitude) != -1:
            filename = "/data/8715/sand_csv/sand" + str(getFile(longtitude, latitude)) + ".csv"
            row, col = getResult(longtitude, latitude)
            return readData(filename, row, col)
        else:
            print("Wrong input, try again.")
    else:
        print("Wrong type, try again.")
        return -1


"""
Return -1 if we get error, else return index number of the csv file we should look at
"""
def getFile(longtitude, latitude):
    if 113.33 < longtitude < 153.569:
        row = (longtitude - 113.33) // 4.0239
    else:
        return -1

    if -43.63 < latitude < -10.668:
        col = (latitude * (-1) - 10.668) // 3.2962
    else:
        return -1

    if row == 0:
        return int(col)
    else:
        return int(row) * 10 + int(col)


"""
Return row and col for specific csv entry 
Input GPS location
"""
# get csv entry by row and col
def getResult(longtitude, latitude):
    row = ((longtitude - 113.33) % 4.0239) // 0.001005975
    col = ((latitude * (-1) - 10.668) % 3.2962) // 0.00109873
    return int(row), int(col)


"""
Get csv entry by filename, row number and column number
"""
def readData(filename, row, col):
    df = pd.read_csv(filename)
    return df.iloc[col, row]


if __name__ == "__main__":
    latitude, longtitude = -35.28878, 149.01080

    print(getData(longtitude, latitude, "sand"))
    print(getData(longtitude, latitude, "silt"))
    print(getData(longtitude, latitude, "clay"))
    print(queryData(longtitude, latitude))
    print(queryMultiData(longtitude, latitude))
