from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin

from soil_equations import get_soil_model_outputs
from query_texture import get_texture_data, get_texture_multidata


# creating a Flask app
app = Flask(__name__)

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

#textureMap Page route
@app.route("/soil/textureMap", methods=["GET", "POST"])
def get_texture_map():
    # check if coords was provided as part of url
    print("request args", request.args)
    print("request json", request.get_json())
    if 'coords' in request.args:
        coords = request.args['coords']
        shape = request.get_json()['shape']

        if ";" in coords:
            coords_list = coords.split(';') # two gps location
            top_left  = coords_list[0].split(',')
            bottom_right = coords_list[1].split(',')
            coords = [(float(top_left[0]) + float(bottom_right[0])) / 2, (float(top_left[1]) + float(bottom_right[1])) / 2]
        else:
            coords = coords.split(',')
            coords = [float(i) for i in coords]

        print("=============================================")
        print(coords)
        print(shape)
        print("=============================================")
    else:
        return "Error: No coords field provided. Please specify coords."

    result = get_texture_multidata(coords[1], coords[0], shape[0], shape[1])
    return jsonify(result)


#texture Page route
@app.route("/soil/texture", methods=["GET", "POST"])
def get_texture():
    # check if coords was provided as part of url
    print("request args", request.args)
    if 'coords' in request.args:
        coords = request.args['coords']

        if ";" in coords:
            coords_list = coords.split(';') # two gps location
            top_left  = coords_list[0].split(',')
            bottom_right = coords_list[1].split(',')
            coords = [(float(top_left[0]) + float(bottom_right[0])) / 2, (float(top_left[1]) + float(bottom_right[1])) / 2]
        else:
            coords = coords.split(',')
            coords = [float(i) for i in coords]
        print("=============================================")
        print(coords)
        print("=============================================")
    else:
        return "Error: No coords field provided. Please specify coords."

    result = get_texture_data(coords[1], coords[0])
    return jsonify(result)


#analysis Page route
@app.route("/soil/analysis", methods=["GET", "POST"])
def api_coords():
    # check if coords was provided as part of url
    print("request args", request.args)
    if 'coords' in request.args:
        coords = request.args['coords']

        if ";" in coords:
            coords_list = coords.split(';') # two gps location
            top_left  = coords_list[0].split(',')
            bottom_right = coords_list[1].split(',')
            coords = [(float(top_left[0]) + float(bottom_right[0])) / 2, (float(top_left[1]) + float(bottom_right[1])) / 2]
        else:
            coords = coords.split(',')
            coords = [float(i) for i in coords]

        print("=============================================")
        print(coords)
        print("=============================================")
    else:
        return "Error: No coords field provided. Please specify coords."

    result = get_soil_model_outputs(coords[1], coords[0])
    return jsonify(result)

if __name__ == "__main__":
    app.run(debug=True)
