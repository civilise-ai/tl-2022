import math

import extract_data

#Query data and return as json
def get_texture_data(longtitude, latitude):
    persand, persilt, perclay = extract_data.queryData(longtitude, latitude)

    jsonout = {
        "perSand": persand,
        "perSilt": persilt,
        "perClay": perclay,
    }
    return jsonout


#Get soiltexture data based on latitude, longitude col and row values
def get_texture_multidata(longtitude, latitude, rows, cols):

    block_rows = math.ceil(rows / 10.0)
    block_cols = math.ceil(cols / 10.0)
    texture_map = extract_data.queryMultiData(longtitude, latitude, block_rows, block_cols)
    texture = []

    for i in range(rows):
        texture.append([])
        for j in range(cols):
            texture[i].append([0] * 4)

    block = 0
    for i in range(0, rows, 10):
        for j in range(0, cols, 10):

            for p in range(i, i+10 if i+10 < rows else rows):
                for q in range(j, j+10 if j+10 < cols else cols):
                    (row, col, perSand, perSilt, perClay) = texture_map[i // 10][j // 10]
                    texture[p][q] =[perSand, perSilt, perClay, block]

            block += 1


    jsonout = {
        "soil":
            {
                "texture": texture,
                "rows": block_rows,
                "cols": block_cols,
                "reso": 10,
            }
    }
    # print(jsonout)
    return jsonout