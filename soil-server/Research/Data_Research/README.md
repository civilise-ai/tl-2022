This folder includes the research projects regarding data compression and scalability. 

The perlin noise file creates randomly generated terrain to be used as test data for other code.

Fourier_series has code that generates a fourier series for multivariate funcitons.

Use This link to access the report:
https://docs.google.com/document/d/10saMFJd6C2i-HulZXELahNp1JE8qSPXXLU-gRx7AFOs/edit?usp=sharing

More comprehensive requirements document (tentative form):
https://docs.google.com/document/d/1TiQ7K4_98fGaSkzyvSWJ9bfBG6U2SbGaoZdtD5BgI3U/edit#heading=h.7c5ncajfthm
