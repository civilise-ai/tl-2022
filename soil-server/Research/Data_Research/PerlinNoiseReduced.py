from numpy.random import Generator, PCG64
import numpy as np
import imageio

class PerlinNoise:
    noise = 0
    edges = 0
    index = 0
    loc = 0
    
    def __init__(s, shape, seed = np.random.randint(2**31)):
        s.seed = seed
        s.rng = np.random.default_rng(seed)
        s.shape = shape
        s.n_edges()
        s.set_loc()
        
    def reset(s, shape = None, seed = None):
        if seed is None:
            s.seed = np.random.randint(2**31)
        else:
            s.seed = seed
        s.rng = np.random.default_rng(s.seed)
        if shape is not None and s.shape != shape:
            s.shape = shape
            s.n_edges()
            s.set_loc()
        s.noise = 0
        

    def n_edges(s):
        dim = s.shape.size
        if dim <=1:
            s.edges = np.array([-1,1])
        s.edges = np.arange(2**(dim-1))
        helper = np.zeros((2**(dim-1),dim), dtype = int)
        for i in range(dim-1):
            helper[:,i] = np.floor(s.edges/2**i)%2
        helper[:,:dim-1] = 2*helper[:,:dim-1]-1
        s.edges = np.zeros((dim*2**(dim-1), dim))
        for i in range(dim):
            s.edges[i*2**(dim-1):(i+1)*2**(dim-1)] = np.roll(helper,i)
            
    # To be able to produce n-dimensional noise, operations are carried out on an array of indices of the intended noise 
    # and then reshaped. nd refers to if an extra value is included to help convert it to vector rather than rastor format.
    def set_index(s, nd = False, dtype = np.int16):
        size = s.shape.prod()
        dim = s.shape.size
        dtype = np.float16 if nd else np.int32
        organiser = np.arange(size)
        s.index = np.zeros((size,dim+nd), dtype = dtype)
        helper = np.append(1, s.shape)
        for i in range(dim):
            s.index[:,i] = np.floor_divide(organiser[:size],helper[:i+1].prod())%helper[i+1]
        if nd:
            s.index[:,-1] = s.noise.flatten("F")
            
    # This creates a boolean array that allows the selection of grid corners/edges given the flattened n-dimensional array above
    def set_loc(s):
        size = s.shape.prod()
        dim = s.shape.size
        organiser = np.arange(2**(dim))
        s.loc = np.zeros((dim,2**(dim)), dtype = bool)
        helper = np.append(1, s.shape)
        for i in range(dim):
            s.loc[i,:]=np.floor_divide(organiser[:2**dim],2**i)%2

    # generate perlin noise "concurrently", meaning generate all output pixels simultaneously
    # This method is fast but requires around 3x as much memory as calculating each pixel individually
    # This implementation restricts the O(2**n) complexity of perlin noise to be in numpy (n is the number of dimensions)
    # negative grid_size values correspond to how many grid boxes are in an image, positive defines absolute grid sizes
    def generate_conc(s, grid_size, v_range = 1,spherical = False):
        shape = s.shape
        dim = shape.size
        size = shape.prod()
        dti = np.int16
        s.set_loc()
        grid_size = grid_size if grid_size > 0 else -np.max(shape)/grid_size
        # pixel to top left grid mapping
        grid_index = 0
        s.set_index()
        s.index = np.array(np.floor_divide(s.index,grid_size), dtype = dti)
        corners = np.zeros(size, dtype = dti)
        helper = np.array(np.append(1,np.floor_divide(shape, grid_size)+1), dtype = dti)
        for i in range(dim):
            corners += s.index[:,i]*helper[:i+1].prod()
        del(s.index)
        # find other corners
        corners = np.zeros((size,2**dim), dtype = int)+corners[:,np.newaxis]
        for i in range(dim):
            corners[:,s.loc[i]]= (corners[:,s.loc[i]]+helper[:i+1].prod())
        g_size = np.array(((np.ceil(shape/grid_size)+1).prod(), dim), dtype = int)
        grid = 0
        # Make corner vectors
        if type(s.edges) == int:
            grid = s.rng.random(g_size)
            #grid = grid#/np.sqrt((grid**2).sum(axis = -1))[:,np.newaxis]
        else:
            grid = s.rng.choice(s.edges, g_size[:-1])
        ######## For spherical/stitched implementation, contact surfaces (edges for 2d) will be replaced here ########
        ######## Spherical would simply replace the "upper" surface of each dimension with the grid values of the lower one
        ######## Stitched implementation would take a number of surfaces as input to replace (with offsets-> issue for non-Natural division of grid)
        # select corner vectors
        grid = grid[corners]
        del(corners)
        s.set_index()
        # corner/offset dot product
        offset = np.zeros((size,dim,2))
        offset[:,:,0] = -((s.index+0.5)%grid_size)
        offset[:,:,1] = grid_size-s.index%grid_size-0.5
        del(s.index)
        for i in range(dim):
            l = s.loc[i]
            grid[:,~l,0] += offset[:,i,0,np.newaxis]*grid[:,~l,i]
            grid[:,l,0] += offset[:,i,1,np.newaxis]*grid[:,l,i]
        del(offset)
        grid = grid[:,:,0]
        # interpolation
        temp = 0
        s.set_index()
        s.index = (s.index%grid_size)/grid_size
        for i in range(dim):
            limit = 2**(dim-i)
            x = s.index[:,-i-1,np.newaxis]
            l = s.loc[-i-1,:limit]
            grid = grid[:,~l]+(-2*x**3+3*x**2)*(grid[:,l]-grid[:,~l])
        grid=grid.reshape(shape)
        s.noise +=v_range*grid/np.max(np.absolute(grid))
    
    # generate "fractal" noise. Apply perlin noise to the same image with varying grid sizes and weights
    # where grid_sizes and weights are not provided, it generates "nice" noise of fixed resolution
    def generate_fractal(s, grid_size = None, weight = None, spherical = False):
        if grid_size is None:
            grid_size = np.arange(8, 3, -1)**2
        if weight is None:
            weight = np.array([1/(i+1) for i in range(grid_size.size)])
        if type(weight) == int:
            weight = weight*np.ones(np.array(grid_size).size)
        grid_size, weight = np.array(grid_size), np.array(weight)
        for i in range(grid_size.size):
            s.generate_conc(grid_size[i],weight[i], spherical)
        return s.noise
        
    def makeData(s, number, directory = "noise/",grid_size = None, weight = None):
        for img_id in range(number):
            s.generate_fractal(grid_size, weight)
            imageio.imwrite(f"{directory}{img_id}.png",s.noise)
            s.reset()
            
    def generator(s, grid_size = None, weight = None):
        while True:
            s.generate_fractal(grid_size, weight)
            yield s.noise
            s.reset()

            
# Code was developed from theory in these sources
# https://en.wikipedia.org/wiki/Perlin_noise#:~:text=Perlin%20noise%20is%20a%20procedural,details%20are%20the%20same%20size.
# https://adrianb.io/2014/08/09/perlinnoise.html

# At some point, I hope to implement "spherical" noise, which means that the surface can be "wrapped" around to form a continuous loop. 
# This extends to the application of generating "chunks" of noise for the generation of much larger images (the storage being infeasible for most computers)