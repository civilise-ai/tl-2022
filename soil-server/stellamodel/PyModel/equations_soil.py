from scipy.integrate import odeint
import numpy as np
import argparse
from matplotlib import pyplot as plt
import time as t

import json


def input_data(x, name):
    if 0 <= x <= 0.1:
        return 0.2
    elif 0.1 < x <= 0.2:
        return x + 0.1
    elif 0.2 < x <= 0.3:
        return 0.3
    elif 0.3 < x <= 0.5:
        return x
    else:
        return 0.5


stella_content = np.genfromtxt('stella2000.csv', delimiter=',', dtype=None)
stella_column_names = [name.decode('utf8') for name in stella_content[0, :]]


def get_stella_column(name):
    index = stella_column_names.index(name)
    return list(stella_content[:, index])


def get_stella_data(x, column):
    return column[int(x)].astype(float)


PlantGrowth_TempCoeff_Column = get_stella_column('PlantGrowth.TempCoeff')
Management_propResidues_Column = get_stella_column('Management.propResidues')
Nutrients_propManuDirect_Column = get_stella_column('Nutrients.propManuDirect')
Water_calPropUnsat_WatMoist_Column = get_stella_column('Water.calPropUnsat WatMoist')
Water_SurfWatOutflux_Column = get_stella_column('Water.SurfWatOutflux')
Livestock_Manure_Column = get_stella_column('Livestock.Manure')
Management_propCoverCrop_Column = get_stella_column('Management.propCoverCrop')
PlantGrowth_NPhBioHarvest_Column = get_stella_column('PlantGrowth.NPhBioHarvest')
Climate_DayJul_Column = get_stella_column('Climate.DayJul')
PlantGrowth_PhBioMort_Column = get_stella_column('PlantGrowth.PhBioMort')
PlantGrowth_NPhBioMort_Column = get_stella_column('PlantGrowth.NPhBioMort')
Management_propTillage_Column = get_stella_column('Management.propTillage')
PlantGrowth_PhBioHarvest_Column = get_stella_column('PlantGrowth.PhBioHarvest')

# ###############################################################################################
parameters = dict()



parameters['mineralDecompositionRate'] = 0.00028
parameters['stableLabile_DecompositionRate'] = 1e-05
parameters['perLabile'] = 0.05
parameters['stableMineral_DecompositionRate'] = 7e-05
parameters['OUTFLOW'] = 1.0
parameters['propCDeadBiomass'] = 0.58
parameters['microbeDeathRate'] = 0.11
parameters['slopeLength'] = 97.2
parameters['permeability'] = 1.8
parameters['iniStableOxi'] = 0.001
parameters['thresholdWater'] = 0.05
parameters['unitConversionBulkDensity'] = 10000.0
parameters['perMineral'] = 0.3
parameters['degSlope'] = 4.62
parameters['propHarvNPhLeft'] = 0.8
parameters['coeffStable'] = 9e-06
parameters['stableErodible'] = 0.005
parameters['iniSoilDepth'] = 0.09
parameters['propNPHLigninContent'] = 0.5
parameters['P'] = 1.0
parameters['S'] = 3.0
parameters['R'] = 1430.0
parameters['perStable'] = 0.65
parameters['empiricalPerCarbon'] = 0.03
parameters['empiricalC'] = 0.08
parameters['coeffLabile'] = 0.0002
parameters['propPHLigninContent'] = 0.025
parameters['empiricalK'] = 0.0277
parameters['perClay'] = 143.0
parameters['labilelDecompositionRate'] = 0.01
parameters['iniLabileOxi'] = 0.04
parameters['iniMicrobe'] = 0.03
parameters['propHarvPhLeft'] = 0.1
parameters['bulkDensity'] = 110.0
parameters['labileErodible'] = 0.165
parameters['empiricalCarbonStock'] = 46.0


def model(state: dict, time):
    Time = time

    # constant parameters
    # ##############################################################################################
    # start_time = t.process_time_ns()
    mineralDecompositionRate = parameters['mineralDecompositionRate']
    stableLabile_DecompositionRate = parameters['stableLabile_DecompositionRate']
    perSand = parameters['perSand']
    stableMineral_DecompositionRate = parameters['stableMineral_DecompositionRate']
    microbeDeathRate = parameters['microbeDeathRate']
    slopeLength = parameters['slopeLength']
    permeability = parameters['permeability']
    iniStableOxi = parameters['iniStableOxi']
    thresholdWater = parameters['thresholdWater']
    degSlope = parameters['degSlope']
    perSilt = parameters['perSilt']
    propHarvNPhLeft = parameters['propHarvNPhLeft']
    coeffStable = parameters['coeffStable']
    stableErodible = parameters['stableErodible']
    iniSoilDepth = parameters['iniSoilDepth']
    propNPHLigninContent = parameters['propNPHLigninContent']
    P = parameters['P']
    S = parameters['S']
    R = parameters['R']
    empiricalC = parameters['empiricalC']
    coeffLabile = parameters['coeffLabile']
    propPHLigninContent = parameters['propPHLigninContent']
    empiricalK = parameters['empiricalK']
    perClay = parameters['perClay']
    labilelDecompositionRate = parameters['labilelDecompositionRate']
    iniLabileOxi = parameters['iniLabileOxi']
    propHarvPhLeft = parameters['propHarvPhLeft']
    labileErodible = parameters['labileErodible']
    # ###############################################################################################
    # end_time = t.process_time_ns()
    # print("loading constants:", end_time - start_time)

    # data from csv files
    perSlope = np.tan(degSlope)
    MLS = input_data(perSlope, 'MLS')
    Climate_nYear = 3
    # ##############################################################################################
    # start_time = t.process_time_ns()
    PlantGrowth_TempCoeff = get_stella_data(Time, PlantGrowth_TempCoeff_Column)
    Management_propResidues = get_stella_data(Time, Management_propResidues_Column)
    Nutrients_propManuDirect = get_stella_data(Time, Nutrients_propManuDirect_Column)
    Water_calPropUnsat_WatMoist = get_stella_data(Time, Water_calPropUnsat_WatMoist_Column)
    Water_SurfWatOutflux = get_stella_data(Time, Water_SurfWatOutflux_Column)
    Livestock_Manure = get_stella_data(Time, Livestock_Manure_Column)
    Management_propCoverCrop = get_stella_data(Time, Management_propCoverCrop_Column)
    PlantGrowth_NPhBioHarvest = get_stella_data(Time, PlantGrowth_NPhBioHarvest_Column)
    Climate_DayJul = get_stella_data(Time, Climate_DayJul_Column)
    PlantGrowth_PhBioMort = get_stella_data(Time, PlantGrowth_PhBioMort_Column)
    PlantGrowth_NPhBioMort = get_stella_data(Time, PlantGrowth_NPhBioMort_Column)
    Management_propTillage = get_stella_data(Time, Management_propTillage_Column)
    PlantGrowth_PhBioHarvest = get_stella_data(Time, PlantGrowth_PhBioHarvest_Column)
    # ###############################################################################################
    # end_time = t.process_time_ns()
    # print("loading from csv:", end_time - start_time)

    ##############################################################################################
    start_time = t.process_time_ns()
    A = (perSlope * perSlope + 10000) ** 0.5
    M = (perSand / 1000 + perSilt / 1000) * (1 - perClay / 1000)
    cover = 1 * (0.8 * Management_propCoverCrop) + (1 - Management_propCoverCrop)
    propManuIndirect = 1 - Nutrients_propManuDirect
    labileOxi = iniLabileOxi * (1 + iniLabileOxi - (1 - Management_propTillage) / 10)
    LS = (65.41 * (perSlope * perSlope / A) + 4.56 * (perSlope / A) + 0.065) * (
            (4.53 * (10 ** -4)) * slopeLength) ** MLS
    propNPHLigninContentFarmMethod = (1.0000001 - Management_propTillage) * propNPHLigninContent
    tillage = Management_propTillage
    propHarvPhLeftFarmMethod = (1.0000001 - Management_propTillage) * propHarvPhLeft
    propHarvNPhLeftFarmMethod = (1.0000001 - Management_propTillage) * propHarvNPhLeft
    residues = 1 * (0.88 * Management_propResidues) + (1 - Management_propResidues)
    propPHLigninContentFarmMethod = (1.0000001 - Management_propTillage) * propPHLigninContent
    stabelOxi = iniStableOxi * (1 + iniStableOxi - (1 - Management_propTillage) / 10)

    ############################################################################################################
    ########### calPerOM needs to be greater than 100 before it makes calculK smaller than empiricalK ##########
    calTotalOM = state[7] + state[3] + state[4]
    calPerOM = (calTotalOM * 1000 / state[6]) * iniSoilDepth
    calculK = (2.1 * (10 ** -4) * (M ** 1.14) * (0.12 - calPerOM) + 3.25 * (S - 2) + 2.5 * (permeability - 3)) * 0.1317
    K = empiricalK if Climate_DayJul < 366 * Climate_nYear else min(empiricalK, calculK)
    ############################################################################################################

    calcuC = cover * residues * tillage * 1.7 / 10
    Cfactor = empiricalC if (Climate_DayJul < 366 * Climate_nYear) else max(empiricalC, calcuC)
    SDin = (propPHLigninContentFarmMethod * (
            PlantGrowth_PhBioMort + PlantGrowth_PhBioHarvest * propHarvPhLeftFarmMethod + Livestock_Manure * propManuIndirect) + (
                propNPHLigninContentFarmMethod) * (
                    PlantGrowth_NPhBioMort + PlantGrowth_NPhBioHarvest * propHarvNPhLeftFarmMethod))
    OxidationStable = state[7] * stabelOxi
    SDDecompMine = stableMineral_DecompositionRate * state[7]
    MineralDecomp = state[4] * PlantGrowth_TempCoeff * mineralDecompositionRate
    ErosionRate = R * state[6] * Water_SurfWatOutflux * K * LS * Cfactor * P / 365
    SDErosion = stableErodible * ErosionRate * state[7]
    SDDecompLD = state[7] * stableLabile_DecompositionRate * PlantGrowth_TempCoeff * state[2] if (
            Water_calPropUnsat_WatMoist > thresholdWater) else stableMineral_DecompositionRate
    MicUptakeSD = coeffStable * state[7]
    MicDeath = 0 if (state[2] < 0.01 * calPerOM) else microbeDeathRate * (
            Management_propTillage / 10) * state[2]
    LDDecomp = state[3] * labilelDecompositionRate * PlantGrowth_TempCoeff * state[
        2] if Water_calPropUnsat_WatMoist > thresholdWater else 0.01
    LDin = ((1 - propPHLigninContentFarmMethod) * (
            PlantGrowth_PhBioMort + PlantGrowth_PhBioHarvest * propHarvPhLeftFarmMethod + Livestock_Manure * propManuIndirect) + (
                    1 - propNPHLigninContentFarmMethod) * (
                    PlantGrowth_NPhBioMort + PlantGrowth_NPhBioHarvest * propHarvNPhLeftFarmMethod))
    LDErosion = ErosionRate * state[3] * labileErodible
    ResidueIn = LDin + SDin
    MicUptakeLD = state[3] * coeffLabile
    OxidationLabile = labileOxi * state[3]
    carbonOut = LDErosion + OxidationLabile + OxidationStable + SDErosion
    # ###############################################################################################
    # end_time = t.process_time_ns()
    # print("floating point calculation", end_time - start_time)

    dcarbonLoss = carbonOut
    dcarbonStored = ResidueIn
    dDecomposing_Microbes = MicUptakeLD + MicUptakeSD - MicDeath
    dLabile_Detritus = LDin + SDDecompLD - MicUptakeLD - OxidationLabile - LDDecomp - LDErosion
    dMineral = SDDecompMine - MineralDecomp
    dSoil_Loss = ErosionRate
    dSoil_Mass = - ErosionRate
    dStable_Detritus = SDin - SDDecompLD - SDDecompMine - MicUptakeSD - OxidationStable - SDErosion

    return [dcarbonLoss, dcarbonStored, dDecomposing_Microbes, dLabile_Detritus, dMineral, dSoil_Loss, dSoil_Mass,
            dStable_Detritus]

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--plot', dest='plot', type=int, help='Plot #, e.g. 0 = carbonLoss')
    parser.add_argument('--iter', dest='iter', type=int, help='# Iterations')
    parser.add_argument('--silt', dest='silt', type=int, help='perSilt')
    parser.add_argument('--sand', dest='sand', type=int, help='perSand')
    parser.add_argument('--clay', dest='clay', type=int, help='perClay')
    args = parser.parse_args()

    parameters['perSand'] = args.sand
    parameters['perSilt'] = args.silt
    parameters['perClay'] = args.clay

    state = []
    state.append(0)  # 0 ['carbonLoss']
    state.append(0)  # 1 ['carbonStored']
    state.append(parameters['iniMicrobe'])  # 2 ['Decomposing_Microbes']
    state.append(parameters['empiricalCarbonStock'] * 1.72 * 0.1 * parameters['perLabile'])  # 3 ['Labile_Detritus']
    state.append(parameters['empiricalCarbonStock'] * 1.72 * 0.1 * parameters['perMineral'])  # 4 ['Mineral']
    state.append(0)  # 5 ['Soil_Loss']
    state.append(parameters['empiricalCarbonStock'] * 1.72 * 0.1 * 1000 * parameters['iniSoilDepth'] / parameters[
        'empiricalPerCarbon'])  # 6 ['Soil_Mass']
    state.append(parameters['empiricalCarbonStock'] * 1.72 * 0.1 * parameters['perStable'])  # 7 ['Stable_Detritus']

    dt = 1
    time = np.arange(1, args.iter, dt)

    start_time = t.time_ns()
    out = odeint(model, state, time)
    end_time = t.time_ns()
    # print("total time:", (end_time - start_time), "ns")
    # print("total time:", (end_time - start_time) / 1000, "us")
    # print("total time:", (end_time - start_time) / 1000 / 1000, "ms")
    # print("total time:", (end_time - start_time) / 1000 / 1000 / 1000, "s")

    print("['carbonLoss']", out[:, 0][-1])
    print("['carbonStored']", out[:, 1][-1])
    print("['Decomposing_Microbes']", out[:, 2][-1])
    print("['Labile_Detritus']", out[:, 3][-1])
    print("['Mineral']", out[:, 4][-1])
    print("['Soil_Loss']", out[:, 5][-1])
    print("['Soil_Mass']", out[:, 6][-1])
    print("['Stable_Detritus']", out[:, 7][-1])

    if args.plot:
        plt.plot(time, out[:, args.plot])
        plt.show()

    with open("soilCurl.json", 'w') as file:
        json.dump({
            "Carbon_Loss": list(out[:, 0]),
            "Carbon_Stored": list(out[:, 1]),
            "Decomposing_Microbes": list(out[:, 2]),
            "Labile_Detritus": list(out[:, 3]),
            "Mineral": list(out[:, 4]),
            "Soil_Loss": list(out[:, 5]),
            "Soil_Mass": list(out[:, 6]),
            "Stable_Detritus": list(out[:, 7]),
        }, file);
