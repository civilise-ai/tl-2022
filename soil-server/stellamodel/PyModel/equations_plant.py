import math
import time as t
from matplotlib import pyplot as plt
from scipy.integrate import odeint
import numpy as np
import json
import argparse

stella_content = np.genfromtxt('stella2000.csv', delimiter=',', dtype=None)
stella_column_names = [name.decode('utf8') for name in stella_content[0, :]]


def get_stella_column(name):
    index = stella_column_names.index(name)
    return list(stella_content[:, index])


def get_stella_data(x, column):
    return column[int(x)].astype(float)


parameters = dict()
bioEnd = 80
BioRepro = 1800
bioStart = 20
calibrationMaxWater = 0.1
calibrationMinWater = 0.29
dayLengRequire = 13
estimateHeight = 120
halfSaturCoeffN = 0.02
halfSaturCoeffP = 0.01
harvestDay = 235
heightMaxBM = 1.2
iniNPhAboveBM = 0.04
iniRootDensity = 0.05
maxAboveBM = 0.6
maxDensity = 40
maxLAI = 3.25
maxPropPhAboveBM = 0.75
NPPCalibration = 0.45
optTemperature = 20
plantingDay = 30
plantWeight = 0.0009
propAboveBelowNPhBM = 0.85
propLAtoB = 15
propNPhMortality = 0.04
propNPhRoot = 0.002
propPhBEverGreen = 0.3
propPhBLeafRate = 0
propPhMortality = 0.015
propPhtoNPhMortality = 0
propPhtoNphReproduction = 0.005
saturatingLightIntensity = 600
sproutRate = 0.01

Climate_DayJul_Column = get_stella_column('Climate.DayJul')
Water_waterAvailable_Column = get_stella_column('Water.waterAvailable')
Management_propCoverCrop_Column = get_stella_column('Management.propCoverCrop')
Soil_calSoilDepth_Column = get_stella_column('Soil.calSoilDepth')
Climate_Elevation_Column = get_stella_column('Climate.Elevation')
Water_Surface_Water_Column = get_stella_column('Water.Surface Water')
Water_unsatWatDepth_Column = get_stella_column('Water.unsatWatDepth')
Climate_airTempC_Column = get_stella_column('Climate.airTempC')
Climate_solRadGrd_Column = get_stella_column('Climate.solRadGrd')
Nutrients_DINSurfAvail_Column = get_stella_column('Nutrients.DINSurfAvail')
Nutrients_PO4Available_Column = get_stella_column('Nutrients.PO4Available')
Climate_dayLength_Column = get_stella_column('Climate.dayLength')
Climate_nYear_Column = get_stella_column('Climate.nYear')
Management_frequPlanting_Column = get_stella_column('Management.frequPlanting')
Management_propPhHarvesting_Column = get_stella_column('Management.propPhHarvesting')
Management_propNPhHarvest_Column = get_stella_column('Management.propNPhHarvest')
PlantGrowth_DMP_Column = get_stella_column('PlantGrowth.DMP')

DT = 1


def model(state, time):
    global pulse
    Time = time

    Climate_DayJul = get_stella_data(Time, Climate_DayJul_Column)
    Water_waterAvailable = get_stella_data(Time, Water_waterAvailable_Column)
    Management_propCoverCrop = get_stella_data(Time, Management_propCoverCrop_Column)
    Soil_calSoilDepth = get_stella_data(Time, Soil_calSoilDepth_Column)
    Climate_Elevation = get_stella_data(Time, Climate_Elevation_Column)
    Water_Surface_Water = get_stella_data(Time, Water_Surface_Water_Column)
    Water_unsatWatDepth = get_stella_data(Time, Water_unsatWatDepth_Column)
    Climate_airTempC = get_stella_data(Time, Climate_airTempC_Column)
    Climate_solRadGrd = get_stella_data(Time, Climate_solRadGrd_Column)
    Nutrients_DINSurfAvail = get_stella_data(Time, Nutrients_DINSurfAvail_Column)
    Nutrients_PO4Available = get_stella_data(Time, Nutrients_PO4Available_Column)
    Climate_dayLength = get_stella_data(Time, Climate_dayLength_Column)
    Climate_nYear = get_stella_data(Time, Climate_nYear_Column)
    Management_frequPlanting = get_stella_data(Time, Management_frequPlanting_Column)
    Management_propPhHarvesting = get_stella_data(Time, Management_propPhHarvesting_Column)
    Management_propNPhHarvest = get_stella_data(Time, Management_propNPhHarvest_Column)

    Bio_time = state[0]
    Max_Photosynthetic_Biomass = state[1]
    Non_photosynthetic_Biomass = state[2]
    Photosynthetic_Biomass = state[3]
    Removed_Yield = state[4]
    Total_NPP = state[5]
    Total_Planting = state[6]
    Yield = state[7]

    maxPhAboveBM = maxAboveBM * maxPropPhAboveBM
    maxNPhBM = (maxAboveBM - maxPhAboveBM) * (1 + 1 / propAboveBelowNPhBM)
    maxBM = maxNPhBM + maxPhAboveBM
    TotalBM = Photosynthetic_Biomass + Non_photosynthetic_Biomass
    CalcuHeight = heightMaxBM * TotalBM / maxBM if maxBM > 0 else 0

    WatStressLow = max(0.0, math.sin(Water_waterAvailable * math.pi / 2) ** calibrationMinWater)
    RootBM = Non_photosynthetic_Biomass * (
        Management_propCoverCrop if Management_propCoverCrop > 0.5 else Management_propCoverCrop + 0.1) / (
                     propAboveBelowNPhBM + 1)
    rootDensity = max(iniRootDensity, Non_photosynthetic_Biomass * propNPhRoot * 1 / Soil_calSoilDepth)
    RootDepth = max((Climate_Elevation / 100) - 1, (RootBM / rootDensity)) * (
        Management_propCoverCrop if Management_propCoverCrop > 0.8 else 0.8)
    WatStressHigh = max(0.0, 1 / (1 + math.exp(
        Water_Surface_Water + calibrationMaxWater))) if calibrationMaxWater < 0 and Water_Surface_Water > -calibrationMaxWater else (
        min(1,
            Water_unsatWatDepth / RootDepth / calibrationMaxWater) if calibrationMaxWater > 0 and RootDepth > 0 else 1)
    WaterCoeff = min(WatStressHigh, WatStressLow)

    TempCoeff = math.exp(0.20 * (Climate_airTempC - optTemperature)) * abs(
        ((40 - Climate_airTempC) / (40 - optTemperature))) ** (0.2 * (40 - optTemperature))
    LightCoeff = Climate_solRadGrd * 10 / saturatingLightIntensity * math.exp(
        1 - Climate_solRadGrd / saturatingLightIntensity)
    NutrCoeff = min((Nutrients_DINSurfAvail / (Nutrients_DINSurfAvail + halfSaturCoeffN)),
                    (Nutrients_PO4Available / (Nutrients_PO4Available + halfSaturCoeffP)))
    NPPControlCoeff = min(LightCoeff, TempCoeff) * WaterCoeff * NutrCoeff
    calculatedPhBioNPP = NPPControlCoeff * NPPCalibration * Photosynthetic_Biomass * (
            1 - Photosynthetic_Biomass / maxPhAboveBM) if Photosynthetic_Biomass < maxPhAboveBM else 0

    DayJul = ((Time - DT) % 365) + 1
    dayLengthPrev = Climate_dayLength if Climate_dayLength <= 1 else Climate_dayLength - 1  # DELAY(Climate_dayLength, 1)
    DMP = get_stella_data(Climate_DayJul, PlantGrowth_DMP_Column)

    empiricalNPP = DMP * 0.45 * 0.1 / 10000

    if Climate_dayLength > dayLengRequire or Climate_dayLength >= dayLengthPrev:
        FallLitterCalc = 0
    elif Photosynthetic_Biomass < 0.01 * (1 - propPhBEverGreen):
        FallLitterCalc = (1 - propPhBEverGreen) * Photosynthetic_Biomass / DT
    else:
        FallLitterCalc = (1 - propPhBEverGreen) * (
                Max_Photosynthetic_Biomass * propPhBLeafRate / Photosynthetic_Biomass) ** 3

    Fall_litter = min(FallLitterCalc,
                      max(0, Photosynthetic_Biomass - propPhBEverGreen * Max_Photosynthetic_Biomass / (
                              1 + propPhBEverGreen)))

    HarvestTime = 1 if harvestDay < DayJul < harvestDay + 3 or harvestDay * 2 < DayJul < harvestDay * 2 + 3 or harvestDay * 3 < DayJul < harvestDay * 3 + 3 else 0

    # calLAI = min(Photosynthetic_Biomass * propLAtoB, maxLAI)
    # empiricalLAI = GRAPH(Climate_DayJul)
    # iniPhBM = propPhBEverGreen * iniNPhAboveBM * maxPropPhAboveBM / (1 - maxPropPhAboveBM)
    # LAI = empiricalLAI if Climate_DayJul < 366 * Climate_nYear else calLAI
    # maxNPhAboveBM = iniNPhAboveBM * (propAboveBelowNPhBM + 1) / propAboveBelowNPhBM
    NPhAboveBM = propAboveBelowNPhBM * RootBM

    #################################################################################################################
    ############################# NOT WORKING, since the value does not clear to 0 ##################################
    # PULSE(1, plantingDay, 365)
    PlantTime = 1 if int(Time) % 365 == plantingDay else 0
    if PlantTime == 1:
        print("PlantTime has reached")
    #################################################################################################################

    Planting = PlantTime * maxDensity * plantWeight
    propPhAboveBM = 0 if NPhAboveBM == 0 else Photosynthetic_Biomass / (Photosynthetic_Biomass + NPhAboveBM)
    PropPhMortDrought = 0.1 * max(0, (1 - WaterCoeff))
    RemovalTime = 1 if CalcuHeight * Management_propPhHarvesting > estimateHeight else 0
    Sprouting = 1 if propPhAboveBM < maxPropPhAboveBM and bioStart < Bio_time < bioEnd else 0
    TransdownRate = 1 - 1 / (Bio_time - BioRepro) ** 0.5 if Bio_time > BioRepro + 1 else (
        0 if propPhAboveBM < maxPropPhAboveBM else math.cos((maxPropPhAboveBM / propPhAboveBM) * math.pi / 2) ** 0.1)
    NPhBioHarvest = HarvestTime * Management_propNPhHarvest * Non_photosynthetic_Biomass / DT
    PhBioHarvest = (HarvestTime + RemovalTime) * Management_propPhHarvesting * Photosynthetic_Biomass / DT
    Harvest = PhBioHarvest + NPhBioHarvest

    #################################################################################################################
    ############################# NOT WORKING, since the value does not clear to 0 ##################################
    # PULSE(Yield / DT, 1, 365)
    removedYieldRate = Yield / DT if int(Time) % 365 == 1 else 0  #
    if removedYieldRate != 0:
        print("removedYieldRate has reached")
    #################################################################################################################

    yieldAssessment = Yield / DT * 0 if Yield > 0 else 0
    PhNPP = empiricalNPP if Climate_DayJul < 366 * Climate_nYear else (0 if HarvestTime > 0 else calculatedPhBioNPP)
    NPPInflow = PhNPP
    PhBioPlanting = Planting * Management_frequPlanting
    NPhBioPlanting = Planting * (1 - Management_frequPlanting)
    plantingIn = PhBioPlanting + NPhBioPlanting
    PhBioMort = Fall_litter * (1 - propPhtoNPhMortality) + Photosynthetic_Biomass * (
            PropPhMortDrought + propPhMortality)
    Transup = Sprouting * sproutRate * Non_photosynthetic_Biomass
    NPhBioMort = Non_photosynthetic_Biomass * propNPhMortality
    if PlantTime == 1:
        cf_Air_Temp = - Bio_time / DT
    else:
        cf_Air_Temp = (Climate_airTempC if Climate_airTempC > 5 else 0)

    PHinflow = Photosynthetic_Biomass / DT if Photosynthetic_Biomass > Max_Photosynthetic_Biomass else 0
    PHoutflow = Max_Photosynthetic_Biomass / DT if Photosynthetic_Biomass > Max_Photosynthetic_Biomass or PlantTime == 1 else 0
    Transdown = 0 if HarvestTime > 0 else TransdownRate * (PhNPP + propPhtoNphReproduction * Photosynthetic_Biomass) + (
            propPhtoNPhMortality * Fall_litter)

    dBio_time = cf_Air_Temp
    dMax_Photosynthetic_Biomass = PHinflow - PHoutflow
    dNon_photosynthetic_Biomass = NPhBioPlanting + Transdown - Transup - NPhBioHarvest - NPhBioMort
    dPhotosynthetic_Biomass = PhNPP + PhBioPlanting + Transup - PhBioHarvest - Transdown - PhBioMort
    dRemoved_Yield = removedYieldRate
    dTotal_NPP = NPPInflow
    dTotal_Planting = plantingIn
    dYield = Harvest - removedYieldRate - yieldAssessment

    return [dBio_time, dMax_Photosynthetic_Biomass, dNon_photosynthetic_Biomass, dPhotosynthetic_Biomass,
            dRemoved_Yield, dTotal_NPP, dTotal_Planting, dYield]


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--plot', dest='plot', type=int, help='Plot')
    parser.add_argument('--iter', dest='iter', type=int, help='Iteration')
    args = parser.parse_args()

    state = []
    state.append(0)  # 0 Bio_time
    state.append(0.6)  # 1 Max_Photosynthetic_Biomass
    state.append(iniNPhAboveBM * (propAboveBelowNPhBM + 1) / propAboveBelowNPhBM)  # 2 Non_photosynthetic_Biomass
    state.append(
        propPhBEverGreen * iniNPhAboveBM * maxPropPhAboveBM / (1 - maxPropPhAboveBM))  # 3 Photosynthetic_Biomass
    state.append(0)  # 4 Removed_Yield
    state.append(0)  # 5 Total_NPP
    state.append(0)  # 6 Total_Planting
    state.append(0)  # 7 Yield

    dt = 1
    time = np.arange(1, args.iter, dt)

    start_time = t.time_ns()
    out = odeint(model, state, time)
    end_time = t.time_ns()
    # print("total time:", (end_time - start_time), "ns")
    # print("total time:", (end_time - start_time) / 1000, "us")
    # print("total time:", (end_time - start_time) / 1000 / 1000, "ms")
    # print("total time:", (end_time - start_time) / 1000 / 1000 / 1000, "s")

    print("['Bio_time']", out[:, 0][-1])
    print("['Max_Photosynthetic_Biomass']", out[:, 1][-1])
    print("['Non_photosynthetic_Biomass']", out[:, 2][-1])
    print("['Photosynthetic_Biomass']", out[:, 3][-1])
    print("['Removed_Yield']", out[:, 4][-1])
    print("['Total_NPP']", out[:, 5][-1])
    print("['Total_Planting']", out[:, 6][-1])
    print("['Yield']", out[:, 7][-1])

    if args.plot:
        plt.plot(time, out[:, args.plot])
        plt.show()

    with open("plantCurl.json", 'w') as file:
        json.dump({
            "Bio_time": list(out[:, 0]),
            "Max_Photosynthetic_Biomass": list(out[:, 1]),
            "Non_photosynthetic_Biomass": list(out[:, 2]),
            "Photosynthetic_Biomass": list(out[:, 3]),
            "Removed_Yield": list(out[:, 4]),
            "Total_NPP": list(out[:, 5]),
            "Total_Planting": list(out[:, 6]),
            "Yield": list(out[:, 7]),
        }, file);
