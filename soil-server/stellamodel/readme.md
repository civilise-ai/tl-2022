# Steps to run the model

### 1. register an account at https://www.iseesystems.com/ and download latest version of stella architech trial

### 2. run stella architect

### 3. click File -> Open -> choose the model DAESim -COMSES

### 4. close the error prompt (false alert, does not affect any functionality)

### 5. play around the model

### 6. change the value of green cicles (aka inputs)

### 7. click on S-Run at the bottom left corner to see the results

