# basic linear regression model
import json

import numpy as np
from sklearn.linear_model import LinearRegression

# load data
with open("./samples.json", "r") as f:
    data = f.read()
samples = json.loads(data)

with open('./preprocess.json', "r") as f:
    data = f.read()
map = json.loads(data)

# prepare train and test set
train_dataset = samples["Inputs"][:8000]
test_dataset = samples["Inputs"][8000:]

# prepare inputs and outputs
X = np.zeros((8000,2))
y = np.zeros((8000,))
for i, sample in enumerate(train_dataset):
    X[i, :] = [sample[0], sample[1]]
    # ["Carbon_Loss": 0, "Carbon_Stored": 1, "Decomposing_Microbes": 2, "Labile_Detritus": 3,
    # "Mineral": 4, "Soil_Loss": 5, "Soil_Mass": 6, "Stable_Detritus": 7]
    #                                               index below | is 0 means selecting carbon_loss
    carbon_losses = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][0]
    y[i] = carbon_losses

# print(X)
# print(y)

# train
reg = LinearRegression().fit(X, y)
print("score ", reg.score(X, y))
# 0.8997966654202688
print("coef ", reg.coef_)
# [2.96660335e-08 2.95424144e-08]
print("intercept", reg.intercept_)
# 36.89317209325507

# test
loss = 0
X = np.zeros((2000,2))
y = np.zeros((2000,))
for i, sample in enumerate(test_dataset):
    X[i, :] = [sample[0], sample[1]]
    carbon_losses = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][0]
    y[i] = carbon_losses

Y = reg.predict(X)
MSE = np.mean(np.power(y-Y, 2))
print("MSE ", MSE)

# predict
print("predict ", reg.predict(np.array([[100, 200]])))
# [36.89318097]