import json
import numpy as np
import pandas as pd
import seaborn as sn
import time
import matplotlib.pyplot as plt
from matplotlib import cm
from sklearn.linear_model import LinearRegression
from sklearn.gaussian_process.kernels import ConstantKernel, RBF
from sklearn.linear_model import ARDRegression, LinearRegression, BayesianRidge
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.neighbors import KNeighborsRegressor


# load data
with open("./samples.json", "r") as f:
    data = f.read()
samples = json.loads(data)

with open('./preprocess.json', "r") as f:
    data = f.read()
map = json.loads(data)

# prepare train and test set
train_dataset = samples["Inputs"][:8000]
test_dataset = samples["Inputs"][8000:]

X = np.zeros((8000,2))
y_total = np.zeros((8000,8))
for i, sample in enumerate(train_dataset):
    X[i, :] = [sample[0], sample[1]]

    # ["Carbon_Loss": 0, "Carbon_Stored": 1, "Decomposing_Microbes": 2, "Labile_Detritus": 3,
    # "Mineral": 4, "Soil_Loss": 5, "Soil_Mass": 6, "Stable_Detritus": 7]
    #                                               index below | is 0 means selecting carbon_loss
    y_total[i, 0] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][0]
    y_total[i, 1] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][1]
    y_total[i, 2] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][2]
    y_total[i, 3] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][3]
    y_total[i, 4] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][4]
    y_total[i, 5] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][5]
    y_total[i, 6] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][6]
    y_total[i, 7] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][7]

y_total = pd.DataFrame(y_total, columns=["Carbon_Loss", "Carbon_Stored", "Decomposing_Microbes", "Labile_Detritus",
     "Mineral", "Soil_Loss", "Soil_Mass", "Stable_Detritus"])
X_dat = pd.DataFrame(X, columns = ['Sand', 'Silt'])


# test_data
X_test = np.zeros((2000,2))
y_test_t = np.zeros((2000,8))
for i, sample in enumerate(test_dataset):
    X_test[i, :] = [sample[0], sample[1]]
    y_test_t[i, 0] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][0]
    y_test_t[i, 1] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][1]
    y_test_t[i, 2] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][2]
    y_test_t[i, 3] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][3]
    y_test_t[i, 4] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][4]
    y_test_t[i, 5] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][5]
    y_test_t[i, 6] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][6]
    y_test_t[i, 7] = map[f'{sample[0]}_{sample[1]}_{sample[2]}'][7]

y_test_t = pd.DataFrame(y_test_t, columns=["Carbon_Loss", "Carbon_Stored", "Decomposing_Microbes", "Labile_Detritus",
     "Mineral", "Soil_Loss", "Soil_Mass", "Stable_Detritus"])
X_test = pd.DataFrame(X_test, columns = ['Sand', 'Silt'])


# load models

# carbon loss
cl_train = np.asarray(y_total.iloc[:,0]).reshape(-1,1)
cl_test =np.asarray(y_test_t.iloc[:,0]).reshape(-1,1)
brr_poly = make_pipeline(
    PolynomialFeatures(degree=2, include_bias=False),
    #StandardScaler(),
    BayesianRidge(),
).fit(X_dat, cl_train)

# decomposing microbes
dm_train = np.asarray(y_total.iloc[:,2]).reshape(-1,1)
dm_test = np.asarray(y_test_t.iloc[:,2]).reshape(-1,1)
reg_dm = LinearRegression().fit(cl_train, dm_train)

# soil loss
sl_train = np.asarray(y_total.iloc[:,5]).reshape(-1,1)
sl_test = np.asarray(y_test_t.iloc[:,5]).reshape(-1,1)
reg_sl = LinearRegression().fit(dm_train, sl_train)

# soil mass
sm_train = np.asarray(y_total.iloc[:,6]).reshape(-1,1)
sm_test = np.asarray(y_test_t.iloc[:,6]).reshape(-1,1)
reg_sm = LinearRegression().fit(dm_train, sm_train)

# stable Detritus
sd_train = y_total.iloc[:,7]
sd_test = y_test_t.iloc[:,7]
reg_sd = LinearRegression().fit(dm_train, sd_train)

# carbon stored
cs_train = y_total.iloc[:,1]
cs_test = y_test_t.iloc[:,1]
knn_cs = KNeighborsRegressor(n_neighbors=5).fit(cl_train, cs_train)

# mineral
mi_train = y_total.iloc[:,4]
mi_test = y_test_t.iloc[:,4]
reg_mi = LinearRegression().fit(dm_train, mi_train)

# labile Detritus
ld_train = y_total.iloc[:,3]
ld_test = y_test_t.iloc[:,3]
knn_ld= KNeighborsRegressor(n_neighbors=5).fit(cl_train, ld_train)

def predict_all(sand, silt):

    soil_texture = np.array([[sand, silt]])
    # carbon loss
    cl = brr_poly.predict(soil_texture).reshape(-1, 1)
    # decomposing microbes
    dm = reg_dm.predict(cl)
    # soil loss
    sl = reg_sl.predict(dm)
    # soil mass
    sm = reg_sm.predict(dm)
    # stable Detritus
    sd = reg_sd.predict(dm)
    # carbon stored
    cs = knn_cs.predict(cl)
    # mineral
    mi = reg_mi.predict(dm)
    # labile Detritus
    ld = knn_ld.predict(cl)



    return [cl, cs, dm, ld, mi, sl, sm, sd]


if __name__ == '__main__':
    print(predict_all(400, 300))