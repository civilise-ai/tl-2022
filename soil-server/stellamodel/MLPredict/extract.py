import json
from collections import defaultdict

if __name__ == '__main__':

    with open("/data/8715/json/samples.json", "r") as f:
        data = f.read()

    samples = json.loads(data)
    inputs = samples["Inputs"]

    map = defaultdict(list)
    for input in inputs:
        with open(f'/data/8715/json/{input[0]}_{input[1]}_{input[2]}.json', "r") as f:
            data = f.read()
        sample = json.loads(data)

        for target in ["Carbon_Loss", "Carbon_Stored", "Decomposing_Microbes", "Labile_Detritus", "Mineral",
                       "Soil_Loss", "Soil_Mass", "Stable_Detritus"]:
            map[f'{input[0]}_{input[1]}_{input[2]}'].append(sample[target][1998])
            
    with open(f'/data/8715/json/preprocess.json', "w") as f:
        f.write(json.dumps(map))
