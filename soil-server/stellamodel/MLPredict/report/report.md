# Predictive ML Model for Fast Simulation


### Introduction

Since the original model is relatively time-consuming to run, we plan to build predictive models to replace the costly simulation process. Given soil texture data, we need to predict carbon loss, decomposing microbes, soil loss, soil mass, stable detritus, mineral and labile detritus these variables are the same as the Stella model output. Since soil texture data have three variables sand, slit and clay, and the sum of these three variables will always equal 1000, hence we can drop clay as it is explained by the other two variables. We have 10,000 samples in total, split into 8,000 as training data and 2,000 as testing data, so we can evaluate our model on the testing set.

### Exploratory Data Analysis

![](heatmap.jpg)

                                                       Figure 1: Correlation heatmap

![](pairplot.png)

                                                                         Figure 2: Pairplot

According to the correlation heatmap (Figure 1), carbon loss has a significant positive correlation with sand and silt. So first step, we will train models to predict carbon loss by sand and slit data. For the second step, since carbon loss has a relatively high correlation with decomposing microbes, we can predict it using some linear model. For the third step, since decomposing microbes have a high correlation with soil loss, soil mass, stable detritus and minerals, we will separate models to predict these variables by decomposing microbes. Lastly, we will predict carbon stored and labile detritus, cause these variables are relatively difficult to predict, we will try different variables combinations and different models.

### Model Selection

Since all variables are numerics, hence all models are regression problems. We decide to use root mean square error (RMSE) and mean absolute percentage error (MAPE) as statistics to compare different models and combine them with some predictive plots. According to Hastie et al. (2009), RMSE measures the standard deviation of prediction error, and MAPE is similar to RMSE which is less influenced by single or a few extreme outliers. Besides time spent on the training and estimating variables that reflect the model's complex level is also considered, since our final goal is to increase speed to predict the model output. Then we will go through models to predict each of the eight variables one by one.

**Carbon Loss**

According to Figure 1, carbon loss has 0.49 and 0.47 correlation with sand and silt respectively, and there is no obvious linear relationship shown in Figure 2. Hence we use Simple Linear Regression as the benchmark model, tried a series of machine learning models and get the following statistics.

|                                      | RMSE                   | MAPE                   |
| ------------------------------------ | ---------------------- | ---------------------- |
| Simple Linear Regression (Benchmark) | 2.2931718624051906e-06 | 4.961338735262423e-08  |
| Polynomial regression                | 1.5574066606307394e-06 | 1.7228479064844492e-07 |
| KNN                                  | **1.6827877105626341e-06** | **3.6446990891481384e-08** |
| SVM Regression                       | 7.405864061727286e-06  | 1.7408101263324487e-07 |
| Bayesian  Ridge Regression           | 2.29317186240798e-06   | 4.9613387352913124e-08 |
| ARD Regression                       | 7.348181192866091e-06  | 1.7228479064844492e-07 |
| Bayesian Ridge Regression with polynomial features  | **1.5574066606549806e-06** | **3.34148142801666e-08**   |
| ARD Regression with polynomial features             | 7.348181192866091e-06  | 1.7228479064844492e-07 |
| Gaussian Mixture Model               | 35.532795320415474     | 0.9627031441517324     |
| XGBoosting                           | 12.692096242892971     | 0.3440227211420639     |

                                                                    Table 1

According to Table 1, *k*-nearest neighbors (KNN) and Bayesian Ridge Regression with polynomial features (BRR) have the best performance, and their predicted plot is shown below. 

![](knn_pred.png)

                                               Figure 3: Original Plot vs KNN Predicted Plot

![](brp_pred.png)

                                               Figure 4: Original Plot vs BRR Predicted Plot

Since BRR is a parametric model, it seems more smooths than KNN. Because both models are relatively simple (the highest degree in BRR is 2), hence two models have similar running times. So we decide to choose BRR as the final model as it has slightly better performance than KNN. The code to build BRR model is shown below.

```python
brr_poly = make_pipeline(
	# Degree for polynomial factors is 2. Without bias, since BayesianRidge() contains bias.
    PolynomialFeatures(degree=2, include_bias=False), 
	# original baysian ridge regression
    BayesianRidge(),
).fit(X_dat, y_train)   # fit model
y_brr = brr_poly.predict(X_test) # predict on test set
```

**Decomposing Microbes**

Since this variable has a -0.97 correlation with carbon loss, so we can use simple linear regression to fit the model. It get 5.436831032374386e-09 as RMSE and 2.8980952826711455e-07 as MAPE which can consider as good fit. Besides the fitted line plot shown below, even though there are a few outliers since the scale is small, the fitted line will cover most of the points. Since this is a simple model, it only takes little time to run the model, so we can use it as the final model for predicting decomposing microbes.

![dm_pred.png](dm_pred.png)

                                  Figure 5: Fitted line Plot (Carbon loss vs Decomposing microbes)

**Soil Loss**

Since this variable has a -1 correlation with decomposing microbes, so we can use simple linear regression to fit the model. It get 5.468547716116804e-05 as RMSE and 1.3791260502146423e-06 as MAPE which can consider as good fit. Besides the fitted line plot shown as below, the fitted line will cover most of the points. Since this is a simple model, it only takes little time to run the model, so we can use it as the final model for predicting soil loss.

![sl_pred.png](sl_pred.png)

                                  Figure 6: Fitted line Plot (Decomposing microbes vs Soil Loss)

**Soil Mass**

Soil mass has 1 correlation with decomposing microbes, so we can use simple linear regression to fit the model. It get 5.468547988408747e-05 as RMSE and 1.8282252559320306e-09 as MAPE which can consider as good fit. Besides the fitted line plot shown below, the fitted line will cover most of the points. Since this is a simple model, it only takes little time to run the model, so we can use it as the final model for predicting soil mass.

![sm_pred.png](sm_pred.png)

                                  Figure 7: Fitted line Plot (Decomposing microbes vs Soil Mass)

**Stable Detritus**

Similar to soil mass, stable detritus have 1 correlation with decomposing microbes, so we can use simple linear regression to fit the model. It get `3.131651055103524e-07` as RMSE and `6.855534058278439e-08` as MAPE which can consider as good fit. Besides fitted line plot shown as below,  the fitted line will cover most of points. Since this is a simple model, it only takes little time to run the model, so we can use it as final model for predict stable detritus.

![sb_pred.png](sb_pred.png)

                                  Figure 8: Fitted line Plot (Decomposing microbes vs Stable Detritus)

**Mineral**

Mineral has a 0.51 correlation with decomposing microbes, which is relatively significant. Since it doesn’t have a higher correlation with other variables that are not predicted by decomposing microbes, so we can use simple linear regression to fit the model. It get 8.504618446844696e-07 as RMSE and 3.2959559946980016e-07 as MAPE which can consider as good fit. Besides the fitted line plot shown below, the data looks relatively spread out, hence the fitted line doesn’t fit that well. However, RMSE and MAPE are still relatively small, because the variance is small, so it can consider as a good fit. Since this is a simple model, it only takes little time to run the model, so we can use it as the final model for predicting mineral.

![mi_pred.png](mi_pred.png)

                                  Figure 9: Fitted line Plot (Decomposing microbes vs Mineral)

**Carbon Stored**

Carbon stored is relatively difficult to predict, since its’ correlation with other variables is not significant according to the correlation heatmap and doesn’t have a clear non-linear relationship with other variables according to pairplots. Hence we tried a series model and get the following statistics.

|                                      | RMSE                   | MAPE                   |
| ------------------------------------ | ---------------------- | ---------------------- |
| Simple Linear Regression (Benchmark) | 1.715798031635494e-06  | 3.7133900202444515e-08 |
| Bayesian Ridge Regression with polynomial features  | 1.7158030707262484e-06 | 3.7083919111618905e-08 |
| KNN                                  | **3.0682436725788967e-07** | **1.8473981378729994e-09** |

                                                                 Table 2

According to Table 2, KNN has the best performance, and the predicted plot shown below looks like a good fit. Because KNN is relatively simple, hence we choose KNN as the final model for predicting carbon stored.

![cs_pred.png](cs_pred.png)

            Figure 10: Predicted Carbon Stored Plot (yellow for true value, black for predicted value)

**Labile Detritus**

Similar to carbon loss,  its’ correlation with other variables is not significant according to the correlation heatmap and doesn’t have a clear non-linear relationship with other variables according to pairplots. Hence we tried a series model and get the following statistics.

|                                      | RMSE                   | MAPE                   |
| ------------------------------------ | ---------------------- | ---------------------- |
| Simple Linear Regression (Benchmark) | 1.0699857973165067e-07 | 2.395234718215231e-07  |
| Bayesian Ridge Regression with polynomial features  | 1.0703690802921544e-07 | 2.3961049071304477e-07 |
| KNN                                  | **2.7754177591611487e-08** | **1.7744528810269695e-08** |

                                                                 Table 3

According to Table 2, KNN has the best performance, and the predicted plot shown below looks like a good fit. Because KNN is relatively simple, hence we choose KNN as the final model for predicting labile detritus.

![ld_pred.png](ld_pred.png)

             Figure 11: Predicted Labile Detritus Plot (yellow for true value, black for predicted value)

### Conclusion

Overall, the eight models have been selected to predict eight variables, and the original code to compare these models can be found on [model.ipynb](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soil-server/stellamodel/MLPredict/model.ipynb). Besides the integrated final eight models can be found on [all_models.py](https://gitlab.com/civilise-ai/tl-2022/-/blob/main/soil-server/stellamodel/MLPredict/all_models.py), which task total of 0.203s to run the whole Python file.

**Reference:**

Hastie, T., Tibshirani, R., Friedman, J. H., & Friedman, J. H. (2009). *The elements of statistical learning: data mining, inference, and prediction* (Vol. 2, pp. 1-758). New York: springer.